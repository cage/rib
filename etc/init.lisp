;;;; init file

;;;;;; mandatory forms ;;;
(defpackage :init
  (:use
   :cl
   :hooks))

(in-package :init)

;;;;;;;;;;;;;;;;;;;;;;;;;;

;; place your lisp here

;; example module
;;(modules:load-module "modules/glpi.lisp")
