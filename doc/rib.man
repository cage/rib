.TH "rib" "1" 

.SH "NAME"
.PP
Rib is a simple tool to run periodic task.

.SH "SYNOPSIS"
.PP
rib [-h|--help] [-v|--version] [-D|--debug-mode]
[-c|--configuration-file CONF-PATH] [-E|--encryption-utility-mode]
[-V|--IV] [-S|--skip-asking-master-password] [-T|--run-tasks
TASK-NAMES-REGEX] [-d|--task-directory DIRECTORY-NAME]

.SH "DESCRIPTION"
.PP
Rib is a simple tool to run periodic task, in its current form is oriented as a test tool for network services but could be used as a general scheduler as well.

.SH "OPTIONS"
.TP
\fB-h, --help\fP
print help and exit;
.TP
\fB-v, --version\fP
print program information and exit;
.TP
\fB-D, --debug-mode\fP
enter debug mode;
.TP
\fB-c, --configuration-file\fP
use the configuration file provided as CONF-PATH;
.TP
\fB-E, --encryption-utility-mode\fP
start utility to generate encrypt data suitable for the configuration file;
.TP
\fB-V, --IV\fP
print an initialization vector suitable for the configuration file and exit;
.TP
\fB-S, --skip-asking-master-password\fP
skip prompt for master password;
.TP
\fB-T, --run-tasks\fP
run tasks and print results on standard output (useful to debug the task), passing "\fBALL\fP" runs all the tasks;
.TP
\fB-d, --task-directory\fP
load task from an alternative directory.
.TP
\fB-N, --no-tui\fP
disable terminal user interface

.PP
Note: the command line switch override directive specified in the configuration \fIfile\fP (e.g. \fC\-\-task\-directory\fP).

.SH "Configuration"
.PP
To properly work the program need a configuration file, if not passed using command line parameter (the \fC\-c\fP switch), a system wide configuration file will be used.

.PP
The file format is simple and is called: https://toml.io/en/ \fBat\fP \fITOML\fP.

.PP
Please pay attention that all the sensitive configuration values (e.g. SMTP password) must be specified in encrypted form.

.PP
To encrypt a value a preparatory step must be performed to generate the \fIinitialization vector\fP (IV).

.RS
.nf
\fC
$ rib --IV

\fP
.fi
.RE

.PP
Save the value generated and paste in the configuration file using key \fIIV\fP

.RS
.nf
\fC
IV = "base64-encoded-value"

\fP
.fi
.RE

.PP
then to encrypt a value type:

.RS
.nf
\fC
$ rib --E

\fP
.fi
.RE

.PP
Follow the instructions and paste the encrypted value in the configuration file

.RS
.nf
\fC[smtp]

[…]
password      = "encrypted-base64-encoded-password"

\fP
.fi
.RE

.PP
A template file that can be customized to fits your needs is given below:

.RS
.nf
\fC# maximum size of the debug log (unit of measurement: octets)

max-debug-file-size = 100000000

# cryptography initialization  vector (I.V.); to generate  this value
# run: "rib --IV"

IV = ""

# if true send a notification when a task signal an error

notify-crash = true

# if true  continue notify  if a  task keeps returning error, if  false just
# notify once for a consecutive number of failures

send-consecutive-notifications = false

# after  this  amount  of  time,  send  notification  anyway  even  if
# consecutive the format is the same used for the task, see rib(1) for
# details

treshold-deny-consecutive-notifications = "20h"

## this path is relative to this configuration file

init-file = "init.lisp"

[http]

# specify a custom  user agent sent to the server  when connecting via
# HTTP protocol,  an empty  string here means  use default  user agent
# ("drakma")

# please note that  the string "/PROGRAM-VERSION" will  be appended to
# the string given below.

# Program  version is  a  number  that identify  an  iteration in  the
# program development (e.g. "0.1")

user-agent = ""

[telegram]

# this  value is  encrypted use:  "rib -E"  to encrypt  values with  a
# master password (this password must be provided by the user)

api-key = ""

[telegram.destinations]

# an arbitrary number of destination group  can be  specified below  but, for  each
# group a single chat id for each destination is allowed
admin = ""
developers = ""

[smtp]

host          = ""
username      = ""

## this value is encrypted use: "rib -E" to encrypt values with a
## master password (this password must be provided by the user)

## leave  the   password  empty  if   the  SMTP  server   requires  no
## authentication

password      = ""
port          = 25
sender        = ""

# Option are:
#  - empty string: no ssl is required
#  - "tls" use TLS
#  - any other value: use STARTTLS

use-ssl       = ""

[smtp.destinations]

# multiple mailboxes are  allowed using a, comma  separated, list of
# addresses (e.g. "a1, a2, a3···")

## an arbitrary number of destination group can be specified but admin
## group is mandatory


## admin group is mandatory

admin      = ""

[task]

# note that the  command line switch '-d' (if  provided) will override
# the value below

task-directory           = ""

# ignore task files that match with this regular expression

task-file-ignored-regexp = "ignored"

# This table specify an optional URL that (when dereferenced) returns
# a JSON  that contains HTTP URLs,  the body of the  HTTP replies from
# this links  must match  a regular expression

[task.http-json]

# the URLs containing the JSON with tasks data

url            = ""

# the frequency to run each test

frequency = "10m"

# the delay of each single matching test in a task (in milliseconds)

delay = 10000

# the number of single test to be performed for each task

count             =  10

# the test  fails if the  rate of  failed HTTP connections  is greater
# that this threshold and... (continues on the next comment below)

threshold         = 0.2

# also the  median of  the failed connection  is above  this threshold
# applied to all the tests

# e.g.

# population: 0 1 2 3 4 5 6 7 8 9 10

# test  failed at  8,  9 and  10: median  9,  assume median  threshold
# values: 0.5,  so the test fails  if: median > (0.5  * 10) → 9  > 5 →
# test fails

median-threshold  = 0.5

# the JSON key for the regular expression to be matched in a test

regexp-key = "keyword"

# the JSON key for the URL to be tested

target-url-key = "url"

# where to send notifications for these tasks

destination    = "admin"

[user-interface]

# units are character
histogram-height = 10

[threads]

# starting size of the testing thread  pool, usually a number equals to
# the number of CPUs/cores minus one

pool-starting-size             = 3

pool-max-size                  = 30

# if the  number of tests  waiting in the  queue is greater  than this
# number, expand the thread pool

pool-expanding-threshold       = 10

# frequency, in seconds, for checking if the pool needs to be expanded

pool-expanding-check-frequency = 60

# database

[database]

[database.history]

# the history  database is a  round robin database divided  in chunks;
# each chunk will contains the number of entries specified below

chunk-size = 10000

# this directive sets the number of chunks.
# so the total size of the database is
# total = size * chunk-size
#       = 10 * 10000
#       = 100000 entries

size = 10

[queue-sorting-parameters]

# recommended to leave these two values unchanged

max-difference-frequency-tolerance = "3600s"

tolerance-acceleration-ratio = "20000s"

[server]

hostname = ""

port = 9876

# path to  the server  certificate in pem format,  if that  path does  not
# exists the server is not enabled
# this path is relative to the data path

certificate-file = "tls/certificates/cert"

# path to  the client  certificate in pem format,  if that  path does  not
# exists the server is not enabled
# this path is relative to the data path

key-file         = "tls/certificates/key"

# path to  authorized client directory;  this path is relative  to the
# data path and  must point to a directory that  contains the client's
# certificates that are authorized to connect with this server

# Important:  if a  new certificate  is added  run "c_rehash"  on that
# directory

authorized-clients = "tls/authorized-clients-certs/"

[client]

# if the server name is null the client is not enabled

hostname = ""

port     = 9876

# path to  the server  certificate in pem format,  if that  path does  not
# exists the server is not enabled
# this path is relative to the data path

certificate-file = ""

# path to  the client  certificate in pem format,  if that  path does  not
# exists the server is not enabled
# this path is relative to the data path

key-file         = ""
\fP
.fi
.RE

.SH "Write your tasks"
.PP
This program load files from a system wide directory or a custom directory (using the \fC\-d\fP switch option).

.PP
A task can be written in Common Lisp or in any other language you prefer.

.SS "Lisp task"
.PP
If lisp is used, the task must bound the special variable \fC*loaded\-task\-function*\fP to a function accepting a potentially unlimited number of parameters (using \fC&rest\fP); this functions.

.IP \(em 4
if called with no arguments, returns \fCnil\fP if the task passed (i.e. no notification is needed), four or five values — instead — if the task needs to send a notification, in details the values returned are:

.IP 1.  4
the level of the notification (\fC:info\fP, \fC:error\fP, \fC:warning\fP, \fC:debug\fP);
.IP 2.  4
the text of the notification;
.IP 3.  4
the notification channel (just \fC:mail\fP, \fCtelegram\fP or \fC:all\fP, any other value means that no notification will be sent);
.IP 4.  4
a string that specify a regular expression (see regex(7)) matching the the destination group or groups (see \fIConfiguration\fP);
.IP 5.  4
a, possibly  empty, list that  specify \fBabsolute\fP file paths of additional data to be attached to notification.

.PP
If the function is called with argument: \fC"frequency"\fP, the frequency of the task is returned instead, see \fITask frequency format\fP.

.PP
If the function is called with argument: \fC"respects\-grace\-time"\fP a generalized boolean is returned that, if not nil, instructs the program to \fBnot\fP respects grace time (i.e. send consecutive notifications)

.PP
The function is called with argument: "\fCinit\fP" only once, just after have been loaded, this way the task's code can perform initialization duties.

.PP
Finally if the function is called with argument: \fC"dependencies"\fP, a list of dependencies, as file paths or URL, of the task is returned instead.

.SS "Task written in other languages"
.PP
A task can be written in any language if all the following conditions are met:

.IP \(em 4
if called with \fC"frequency"\fP as command line argument, the task must print a line representing the frequency, see \fITask frequency format\fP.
.IP \(em 4
if called with \fC"dependencies"\fP as command line argument the script must print a tab (i.e. 0x09, '') separated strings of task names (full URI or local file system paths) it depends on;

.IP \(em 4
if called with \fC"respects\-grace\-time"\fP as command line argument the script must returns an empty string to instructs the program to \fBnot\fP respects grace time (i.e. send consecutive notifications);

.IP \(em 4
if called with "\fCinit\fP" only as command line argument performs initialization duties, if needed;

.IP \(em 4
finally if called with no arguments on the command line the script must print at least four horizontal tab (i.e. 0x09, '') separated strings:

.IP 1.  4
the level of the notification (\fC"info"\fP, \fC"error"\fP, \fC"warn"\fP, \fC"debug"\fP);
.IP 2.  4
the text of the notification (cannot contains a tab character);
.IP 3.  4
the notification channel (just \fC"mail"\fP, \fC"telegram"\fP or \fC"all"\fP, any other value means that no notification will be sent);
.IP 4.  4
an fourth value that specify a regular expression (see regex(7)) matching the the destination group or groups (see \fIConfiguration\fP);
.IP 5.  4
any additonal fields, if presents, must be \fBabsolute\fP paths to files containing data to be attached in the notification.

.PP
Any output not following this format is interpreted as an instruction for the scheduler to \fBnot\fP send any notification.

.SS "Task frequency format"
.PP
Below the frequency grammar in a pseudo BNF format:

.RS
.nf
\fCFrequency format
FREQUENCY         := LITERAL-FREQUENCY | NUMBER SPACE* UNITS
LITERAL-FREQUENCY := ONCE
                     | "half-hour"
                     | "hourly"
                     | "daily"
                     | "weekly"
                     | "monthly"
                     | "yearly"
ONCE              := "once" | "boot"
NUMBER            := [0-9]+
UNITS             := 'h' 'm' 's' 'd' 'ms'
SPACE             := ASCII #x20
\fP
.fi
.RE

.SS "frequency example:"
.TS
 center,box;

l l .
"20ms"	twenty millisecond
_
"10s"	ten second
_
"2h"	two hours
_
"1d"	one day
_
"once"	run and discard the task
.TE
.TB ""

.SH "Interaction"
.PP
The program create two FIFO (see man fifo(7)) files in a system dependent directory (the TUI print the actual location).

.PP
One of the FIFO is used to send command to the program, the other is for reading command results.

.PP
Each reply is terminated with the strings "OK" or "ER" (the latter if an error occurred processing the command) followed by a carriage return (ASCII 13) and a linefeed (ASCII 10).

.PP
Available command are:

.TP
\fB"quit"\fP
clean up and close the program (may take some time to complete if there are tasks waiting to be ran);
.TP
\fB"force quit"\fP
force closing of the program, note that this could render the program's state and possibly corrupting your data and even prevent the program to start again;
.TP
\fB"waiting notifications"\fP
print CSV table with the notifications in the waiting queue.
.TP
\fB"reload"\fP
reload the tasks, the configuration file and restart the program routines.
.TP
\fB"collect garbage"\fP
perform a cleanup of the allocated, but no more used, RAM memory.
.TP
\fB"delete task TASK-ID-RE"\fP
delete all the tasks whom ID matches TASK-ID-RE (as a regular expression).

\fBimportant note:\fP the task that depends on the deleted ones will be recursively deleted as well.

Example below:

.RS
.nf
\fCA -> B
|
V
C-> D

E
\fP
.fi
.RE

after deleting  \fCD\fP, the task  \fCC\fP and  \fCA\fP will be  also deleted resulting in this graph:

.RS
.nf
\fCB

E
\fP
.fi
.RE

.TP
\fBquery system information\fP
A query can be specified using a trivial SQL-like query language, example given below:

.RS
.nf
\fCselect task-history where message like "error" and notification-level = "crit"
\fP
.fi
.RE

Logical operator supported:

.IP \(em 4
"\fCand\fP";
.IP \(em 4
"\fCor\fP";
.IP \(em 4
"\fCnot\fP". (with limitation, see below)

Comparison operator supported:

.IP \(em 4
"\fC=\fP";
.IP \(em 4
"\fClike\fP" \fBNote:\fP uses regular expressions match:
.IP \(em 4
"\fC!=\fP";
.IP \(em 4
"\fC<\fP";
.IP \(em 4
"\fC<=\fP";
.IP \(em 4
"\fC>\fP";
.IP \(em 4
"\fC>=\fP".

.RS
.nf
\fCselect task-history where message like "error" and not notification-level = "crit"
\fP
.fi
.RE

Parenthesis can be used around terms:

.RS
.nf
\fCselect a where not (b like "p" and b like "q" ) or b like "f"
\fP
.fi
.RE

The following tables are available so far:

.TP
\fBtable \fCtask\-history\fP\fP
this table contains the following columns

.TS
 center,box;

l l .
column name	description
_
triggered	has the tasks signalled a notification? (values: "true" or "false")
_
sent-to-destination	was the notification actually delivered? A notification is not delivered if the grace period is activ
_
task-name	univocal identifier
_
message	the message signalled from this task (values: the actual message or "NIL")
_
frequency	frequency of the tasks in minutes
_
notification-level	one value in (":info" ":warning" ":error" ":debug" ":crit")
_
notification-metadata	the metadata signalled from this task (values: the metadata or "NIL")
_
time-run	the time, in ISO-8601 format, when the task has run
.TE
.TB ""

.TP
\fBtable \fCmemory\-used\fP\fP
This table contains an histogram of RAM used by the program in the latest 1000 steps. The frequency of the step is equal to the value of \fCpool\-expanding\-check\-frequency\fP.

.TS
 center,box;

l l .
column name	description
_
Memory used	A string representation of memory used as: "VALUE SPACE UNITS"
.TE
.TB ""

.PP
The tables are are printed in CSV format.

.PP
Any other commands are ignored.

.SH "Notes"
.PP
Only SBCL compiler is supported.

.SH "BUGS"
.PP
Please file bug reports on \fIhttps://codeberg.org/cage/rib/\fP.


.SH "License"
.PP
This program is released under GNU General Public license version 3 or later (see COPYING file).

.SH "Contributing"
.PP
Any help is appreciated. If you intend to contribute please point your browser to \fIhttps://codeberg.org/cage/rib/issues\fP (issue tracker) or file a pull request on \fIhttps://codeberg.org/cage/rib/pulls\fP.

.SH "NO WARRANTY"
.PP
rib: a notification system

.PP
Copyright © Università degli Studi di Palermo, cage

.PP
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

.PP
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

.PP
You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/ \fBat\fP \fIhttp://www.gnu.org/licenses/\fP.
