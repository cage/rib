#+OPTIONS: html-postamble:nil html-preamble:nil
#+AUTHOR:
#+TITLE: rib

* NAME

  Rib is a simple tool to run periodic task.

* SYNOPSIS

  rib [-h|--help] [-v|--version] [-D|--debug-mode]
  [-c|--configuration-file CONF-PATH] [-E|--encryption-utility-mode]
  [-V|--IV] [-S|--skip-asking-master-password] [-T|--run-tasks
  TASK-NAMES-REGEX] [-d|--task-directory DIRECTORY-NAME]

* DESCRIPTION

  Rib is a simple tool to run periodic task, in its current form is oriented as a test tool for network services but could be used as a general scheduler as well.

* OPTIONS

  - -h, --help :: print help and exit;
  - -v, --version :: print program information and exit;
  - -D, --debug-mode :: enter debug mode;
  - -c, --configuration-file :: use the configuration file provided as CONF-PATH;
  - -E, --encryption-utility-mode :: start utility to generate encrypt data suitable for the configuration file;
  - -V, --IV :: print an initialization vector suitable for the configuration file and exit;
  - -S, --skip-asking-master-password :: skip prompt for master password;
  - -T, --run-tasks :: run tasks and print results on standard output (useful to debug the task), passing "*ALL*" runs all the tasks;
  - -d, --task-directory :: load task from an alternative directory.
  - -N, --no-tui :: disable terminal user interface

 Note: the command line switch override directive specified in the configuration /file/ (e.g. ~--task-directory~).

* Configuration

  To properly work the program need a configuration file, if not passed using command line parameter (the ~-c~ switch), a system wide configuration file will be used.

  The file format is simple and is called: [[https://toml.io/en/][TOML]].

  Please pay attention that all the sensitive configuration values (e.g. SMTP password) must be specified in encrypted form.

  To encrypt a value a preparatory step must be performed to generate the /initialization vector/ (IV).

  #+BEGIN_SRC shell

    $ rib --IV

  #+END_SRC

  Save the value generated and paste in the configuration file using key /IV/

  #+BEGIN_SRC text

    IV = "base64-encoded-value"

  #+END_SRC

  then to encrypt a value type:

  #+BEGIN_SRC shell

    $ rib --E

  #+END_SRC

  Follow the instructions and paste the encrypted value in the configuration file

  #+BEGIN_SRC shell
    [smtp]

    […]
    password      = "encrypted-base64-encoded-password"

  #+END_SRC

  A template file that can be customized to fits your needs is given below:

#+INCLUDE: "../etc/config.toml" src text

* Write your tasks

  This program load files from a system wide directory or a custom directory (using the ~-d~ switch option).

  A task can be written in Common Lisp or in any other language you prefer.

** Lisp task
   If lisp is used, the task must bound the special variable ~*loaded-task-function*~ to a function accepting a potentially unlimited number of parameters (using ~&rest~); this functions.

   - if called with no arguments, returns ~nil~ if the task passed (i.e. no notification is needed), four or five values — instead — if the task needs to send a notification, in details the values returned are:

     1. the level of the notification (~:info~, ~:error~, ~:warning~, ~:debug~);
     2. the text of the notification;
     3. the notification channel (just ~:mail~, ~telegram~ or ~:all~, any other value means that no notification will be sent);
     4. a string that specify a regular expression (see regex(7)) matching the the destination group or groups (see [[Configuration]]);
     5. a, possibly  empty, list that  specify *absolute* file paths of additional data to be attached to notification.

   If the function is called with argument: ~"frequency"~, the frequency of the task is returned instead, see [[Task frequency format]].

   If the function is called with argument: ~"respects-grace-time"~ a generalized boolean is returned that, if not nil, instructs the program to *not* respects grace time (i.e. send consecutive notifications)

   The function is called with argument: "~init~" only once, just after have been loaded, this way the task's code can perform initialization duties.

   Finally if the function is called with argument: ~"dependencies"~, a list of dependencies, as file paths or URL, of the task is returned instead.

** Task written in other languages

   A task can be written in any language if all the following conditions are met:

    - if called with ~"frequency"~ as command line argument, the task must print a line representing the frequency, see [[Task frequency format]].

   - if called with ~"dependencies"~ as command line argument the script must print a tab (i.e. 0x09, '\t') separated strings of task names (full URI or local file system paths) it depends on;

   - if called with ~"respects-grace-time"~ as command line argument the script must returns an empty string to instructs the program to *not* respects grace time (i.e. send consecutive notifications);

   - if called with "~init~" only as command line argument performs initialization duties, if needed;

   - finally if called with no arguments on the command line the script must print at least four horizontal tab (i.e. 0x09, '\t') separated strings:

     1. the level of the notification (~"info"~, ~"error"~, ~"warn"~, ~"debug"~);
     2. the text of the notification (cannot contains a tab character);
     3. the notification channel (just ~"mail"~, ~"telegram"~ or ~"all"~, any other value means that no notification will be sent);
     4. an fourth value that specify a regular expression (see regex(7)) matching the the destination group or groups (see [[Configuration]]);
     5. any additonal fields, if presents, must be *absolute* paths to files containing data to be attached in the notification.

   Any output not following this format is interpreted as an instruction for the scheduler to *not* send any notification.

** Task frequency format

   Below the frequency grammar in a pseudo BNF format:

   #+BEGIN_SRC text
    Frequency format
    FREQUENCY         := LITERAL-FREQUENCY | NUMBER SPACE* UNITS
    LITERAL-FREQUENCY := ONCE
                         | "half-hour"
                         | "hourly"
                         | "daily"
                         | "weekly"
                         | "monthly"
                         | "yearly"
    ONCE              := "once" | "boot"
    NUMBER            := [0-9]+
    UNITS             := 'h' 'm' 's' 'd' 'ms'
    SPACE             := ASCII #x20
   #+END_SRC

*** frequency example:

    | "20ms" | twenty millisecond       |
    |--------+--------------------------|
    | "10s"  | ten second               |
    |--------+--------------------------|
    | "2h"   | two hours                |
    |--------+--------------------------|
    | "1d"   | one day                  |
    |--------+--------------------------|
    | "once" | run and discard the task |

* Interaction

  The program create two FIFO (see man fifo(7)) files in a system dependent directory (the TUI print the actual location).

  One of the FIFO is used to send command to the program, the other is for reading command results.

  Each reply is terminated with the strings "OK" or "ER" (the latter if an error occurred processing the command) followed by a carriage return (ASCII 13) and a linefeed (ASCII 10).

  Available command are:

  - "quit" :: clean up and close the program (may take some time to complete if there are tasks waiting to be ran);
  - "force quit" :: force closing of the program, note that this could render the program's state and possibly corrupting your data and even prevent the program to start again;
  - "waiting notifications" :: print CSV table with the notifications in the waiting queue.
  - "reload" :: reload the tasks, the configuration file and restart the program routines.
  - "collect garbage" :: perform a cleanup of the allocated, but no more used, RAM memory.
  - "delete task TASK-ID-RE" :: delete all the tasks whom ID matches TASK-ID-RE (as a regular expression).

    *important note:* the task that depends on the deleted ones will be recursively deleted as well.

    Example below:

    #+BEGIN_SRC text
            A -> B
            |
            V
            C-> D

            E
    #+END_SRC

    after deleting  ~D~, the task  ~C~ and  ~A~ will be  also deleted resulting in this graph:

    #+BEGIN_SRC text
            B

            E
    #+END_SRC

  - query system information :: A query can be specified using a trivial SQL-like query language, example given below:

    #+BEGIN_SRC text
       select task-history where message like "error" and notification-level = "crit"
    #+END_SRC

    Logical operator supported:

    - "~and~";
    - "~or~";
    - "~not~". (with limitation, see below)

    Comparison operator supported:

    - "~=~";
    - "~like~" *Note:* uses regular expressions match:
    - "~!=~";
    - "~<~";
    - "~<=~";
    - "~>~";
    - "~>=~".

    #+BEGIN_SRC sql
      select task-history where message like "error" and not notification-level = "crit"
    #+END_SRC

    Parenthesis can be used around terms:

    #+BEGIN_SRC sql
      select a where not (b like "p" and b like "q" ) or b like "f"
    #+END_SRC

    The following tables are available so far:

  - table ~task-history~ :: this table contains the following columns

    #+ATTR_LATEX: :environment longtable :align |l|p{5cm}|
    | column name           | description                                                                                           |
    |-----------------------+-------------------------------------------------------------------------------------------------------|
    | triggered             | has the tasks signalled a notification? (values: "true" or "false")                                   |
    |-----------------------+-------------------------------------------------------------------------------------------------------|
    | sent-to-destination   | was the notification actually delivered? A notification is not delivered if the grace period is activ |
    |-----------------------+-------------------------------------------------------------------------------------------------------|
    | task-name             | univocal identifier                                                                                   |
    |-----------------------+-------------------------------------------------------------------------------------------------------|
    | message               | the message signalled from this task (values: the actual message or "NIL")                            |
    |-----------------------+-------------------------------------------------------------------------------------------------------|
    | frequency             | frequency of the tasks in minutes                                                                     |
    |-----------------------+-------------------------------------------------------------------------------------------------------|
    | notification-level    | one value in (":info" ":warning" ":error" ":debug" ":crit")                                           |
    |-----------------------+-------------------------------------------------------------------------------------------------------|
    | notification-metadata | the metadata signalled from this task (values: the metadata or "NIL")                                 |
    |-----------------------+-------------------------------------------------------------------------------------------------------|
    | time-run              | the time, in ISO-8601 format, when the task has run                                                   |

  - table ~memory-used~ :: This table contains an histogram of RAM used by the program in the latest 1000 steps. The frequency of the step is equal to the value of ~pool-expanding-check-frequency~.

    #+ATTR_LATEX: :environment longtable :align |l|p{5cm}|
    | column name | description                                                    |
    |-------------+----------------------------------------------------------------|
    | Memory used | A string representation of memory used as: "VALUE SPACE UNITS" |

  The tables are are printed in CSV format.

  Any other commands are ignored.

* Notes

  Only SBCL compiler is supported.

* BUGS

  Please file bug reports on [[https://codeberg.org/cage/rib/]].


* License

  This program is released under GNU General Public license version 3 or later (see COPYING file).

* Contributing

  Any help is appreciated. If you intend to contribute please point your browser to [[https://codeberg.org/cage/rib/issues]] (issue tracker) or file a pull request on [[https://codeberg.org/cage/rib/pulls]].

* NO WARRANTY

  rib: a notification system

  Copyright © Università degli Studi di Palermo, cage

  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this program. If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].
