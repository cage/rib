dnl rib: a general scheduler.
dnl Copyright © Università degli Studi di Palermo, cage

dnl This program is free software: you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation, either version 3 of the License, or
dnl (at your option) any later version.

dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.

dnl You should have received a copy of the GNU General Public License
dnl along with this program.  If not, see <http://www.gnu.org/licenses/>.

dnl derived from:

dnl tinmop © 2022 cage
dnl This program is free software: you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation, either version 3 of the License, or
dnl (at your option) any later version.

dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.

dnl You should have received a copy of the GNU General Public License
dnl along with this program.
dnl If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

AC_INIT([rib],[0.0.6],[https://codeberg.org/cage/rib/issues],[rib])

AM_INIT_AUTOMAKE([-Wall foreign])

AM_GNU_GETTEXT([external])

AC_PATH_PROG([MSGMERGE],[msgmerge],[no])

if test "$MSGMERGE" = "no" ; then
   AC_MSG_ERROR([Can not find msgmerge, from the gettext package.])
fi

AC_PATH_PROG([XGETTEXT],[xgettext],[no])

if test "$XGETTEXT" = "no" ; then
   AC_MSG_ERROR([Can not find xgettext, from the gettext package.])
fi

AC_PATH_PROG([MSGFMT],[msgfmt],[no])

if test "$MSGFMT" = "no" ; then
   AC_MSG_ERROR([Can not find msgfmt, from the gettext package.])
fi

AC_PATH_PROG([LISP_COMPILER],[sbcl],[no])

if test "$LISP_COMPILER" = "no" ; then
   AC_MSG_ERROR([Can not find SBCL, Common Lisp compiler.])
fi

AC_PATH_PROG([CURL],[curl],[no])

if test "$CURL" = "no" ; then
   AC_MSG_ERROR([Can not find curl.])
   exit 1;
fi

AC_PATH_PROG([GPG],[gpg],[no])

if test "$GPG" = "no" ; then
   AC_MSG_ERROR([Can not find gpg, crypto software.])
   exit 1;
fi

if test -z "${AWK}"; then
   AC_MSG_ERROR([Can not find AWK.])
   exit 1
fi

AC_PATH_PROGS([DIRNAME],[dirname],[no])

if test "$DIRNAME" = "no" ; then
   AC_MSG_ERROR([Can not find dirname executable.])
   exit 1;
fi

AC_PROG_MKDIR_P

dnl check sbcl version
SBCL_MIN_VERSION="1.4.8";
SBCL_VERSION=`${LISP_COMPILER} --version | ${AWK} -- '// {print $2}'`
SBCL_VERSION_OK=`echo "$SBCL_VERSION $SBCL_MIN_VERSION" | awk -f compare_version.awk`

if test "$SBCL_VERSION_OK" = "1" ; then
   AC_MSG_ERROR([Lisp compiler too old, $SBCL_MIN_VERSION is the oldest supported.])
   exit 1;
fi

AC_CHECK_HEADER([magic.h], [], AC_MSG_ERROR([Can not find libmagic header file.]), [])

AC_CHECK_LIB([ssl], [SSL_get_version], [], AC_MSG_ERROR([Can not find libssl.]))

AC_CONFIG_FILES([Makefile quick_quicklisp.sh po/Makefile.in src/config.lisp.in])

AC_OUTPUT
