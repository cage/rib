;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

;; derived from tinmop © 2022 cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

;; derived from

;; This software is Copyright ©  2021 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(in-package :graph-tests)

(defsuite graph-suite (all-suite))

(deftest test-matrix (graph-suite)
  (let ((graph (make-instance 'graph
                              :nodes '(a b c))))
    (add-arc graph 'a 'b)
    (add-arc graph 'b 'c)
    (let ((connection-matrix (graph::connection-matrix graph)))
      ;; a → a
      (assert-true (= (aref connection-matrix 0 0)
                      0))
      ;; a → b
      (assert-true (= (aref connection-matrix 0 1)
                      1))
      ;; a → c
      (assert-true (= (aref connection-matrix 0 2)
                      0))
      ;; b → a
      (assert-true (= (aref connection-matrix 1 0)
                      1))
      ;; b → b
      (assert-true (= (aref connection-matrix 1 1)
                      0))
      ;; b → c
      (assert-true (= (aref connection-matrix 1 2)
                      1))
      ;; c → a
      (assert-true (= (aref connection-matrix 2 0)
                      0))
      ;; c → b
      (assert-true (= (aref connection-matrix 2 1)
                      1))
      ;; c → c
      (assert-true (= (aref connection-matrix 2 2)
                      0))
      (delete-arc graph 'a 'b)
      ;; a → a
      (assert-true (= (aref connection-matrix 0 0)
                      0))
      ;; a → b
      (assert-true (= (aref connection-matrix 0 1)
                      0))
      ;; a → c
      (assert-true (= (aref connection-matrix 0 2)
                      0))
      ;; b → a
      (assert-true (= (aref connection-matrix 1 0)
                      0))
      ;; b → b
      (assert-true (= (aref connection-matrix 1 1)
                      0))
      ;; b → c
      (assert-true (= (aref connection-matrix 1 2)
                      1))
      ;; c → a
      (assert-true (= (aref connection-matrix 2 0)
                      0))
      ;; c → b
      (assert-true (= (aref connection-matrix 2 1)
                      1))
      ;; c → c
      (assert-true (= (aref connection-matrix 2 2)
                      0))
      (delete-arc graph 'a 'b)
      (assert-condition error (add-arc graph 'a 'x)))))

(deftest test-arc (graph-suite)
  (let ((graph (make-instance 'graph
                              :nodes '(a b c))))
    (add-arc graph 'a 'b)
    (add-arc graph 'b 'c)
    (assert-equalp '(0 2) (all-arcs graph 'b)))
  (let ((directed-graph (make-instance 'graph
                                      :nodes '(a b c d))))
    (add-directed-arc directed-graph 'a 'b)
    (add-directed-arc directed-graph 'b 'c)
    (add-directed-arc directed-graph 'd 'c)
    (assert-equalp '(2) (all-arcs directed-graph 'd))
    (assert-false  (all-arcs-pointing-to directed-graph 'd))
    (assert-equalp '(1 3) (all-arcs-pointing-to directed-graph 'c))))

(defun simple-dfs ()
  (let ((graph (make-instance 'graph
                              :nodes '(a b c d x y)))
        (path  '()))
    (add-arc graph 'a 'b)
    (add-arc graph 'b 'c)
    (add-arc graph 'c 'd)
    (add-arc graph 'd 'a)
    (add-arc graph 'x 'y)
    (dfs graph (lambda (a) (push a path)))
    (mapcar #'value (reverse path))))

(deftest test-dfs (graph-suite)
  (assert-equalp '(a b c d x y)
      (simple-dfs)))

(defun simple-directed ()
  (let ((graph (make-instance 'graph
                              :nodes '(a b c d x y))))
    (add-arc graph 'a 'b :weight-to->from 0)
    (add-arc graph 'b 'c :weight-to->from 0)
    (add-arc graph 'c 'd :weight-to->from 0)
    (add-arc graph 'd 'a :weight-to->from 0)
    (add-arc graph 'x 'y :weight-to->from 0)
    graph))

(defun simple-undirected ()
  (let ((graph (make-instance 'graph
                              :nodes '(a b c d x y w))))
    (add-arc graph 'a 'b)
    (add-arc graph 'b 'c)
    (add-arc graph 'c 'd)
    (add-arc graph 'd 'a)
    (add-arc graph 'x 'y)
    (add-arc graph 'x 'w)
    (add-arc graph 'w 'y)
    graph))

(defun simple-dag ()
  (let ((graph (make-instance 'graph
                              :nodes '(a b c d))))
    (add-arc graph 'a 'b :weight-to->from 0)
    (add-arc graph 'a 'c :weight-to->from 0)
    (add-arc graph 'c 'd :weight-to->from 0)
    graph))

(defun directed ()
  (let ((graph (make-instance 'graph
                              :nodes '(a b c d e))))
    (add-arc graph 'a 'd :weight-to->from 0)
    (add-arc graph 'a 'e :weight-to->from 0)
    (add-arc graph 'd 'c :weight-to->from 0)
    (add-arc graph 'c 'a :weight-to->from 0)
    graph))

(defun directed-2 ()
  (let ((graph (make-instance 'graph
                              :nodes '(a b c d e))))
    (add-arc graph 'a 'd :weight-to->from 0)
    (add-arc graph 'a 'e :weight-to->from 0)
    (add-arc graph 'd 'c :weight-to->from 0)
    (add-arc graph 'c 'a :weight-to->from 0)
    (add-arc graph 'e 'd :weight-to->from 0)
    graph))

(deftest test-circle (graph-suite)
  (assert-equalp '(d a b c d)
      (mapcar #'graph:value (first (cyclep (simple-directed)))))
  (assert-equalp '(d a b c d)
      (mapcar #'graph:value (first (cyclep (simple-undirected)))))
  (assert-false (cyclep (simple-dag)))
  (assert-true (first (cyclep (directed)))))
