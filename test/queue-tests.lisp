;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :queue-tests)

(defsuite queue-test-suite (all-suite))

(defparameter *queue* (make-instance 'q:queue :container '(dummy)))

(defun producer-threads-poll ()
  (loop repeat 20 do
    (bt:make-thread (lambda ()
                      (sleep (random 10))
                      (q:push-value *queue* 'value)))))

(defun consumer-thread-poll ()
  (bt:make-thread (lambda ()
                    (loop for i from 0 below 99 do
                      (sleep (random 2))
                      (q:pop-value *queue*)))))

(defun start-producer-consumer-poll ()
  (producer-threads-poll)
  (bt:join-thread (consumer-thread-poll))
  *queue*)

(deftest queue-polling-test (queue-test-suite)
  (assert-true (q:emptyp (start-producer-consumer-poll))))

(defun producer-threads ()
  (loop repeat 20 collect
                  (bt:make-thread (lambda ()
                                    (sleep (random 5))
                                    (q:push-unblock *queue* 'value)))))


(defun consumer-thread ()
  (bt:make-thread (lambda ()
                    (loop with stop = nil while (not stop) do
                      (let ((value (q:pop-block *queue*)))
                        (when (eq value 'stopper)
                          (setf stop t)))))))

(defun start-producer-consumer ()
  (let ((consumer  (consumer-thread))
        (producers (producer-threads)))
    (loop for producer in producers do (bt:join-thread producer))
    (bt:make-thread (lambda ()
                      (q:push-unblock *queue* 'stopper)))
    (bt:join-thread consumer)
    *queue*))

(deftest queue-block-unblock-test ((queue-test-suite) ())
  (assert-true (q:emptyp (start-producer-consumer))))
