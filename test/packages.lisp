;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

;; derived from tinmop © 2022 cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

;; derived from

;; This software is Copyright ©  2021 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(defpackage :all-tests
  (:use :cl
        :clunit
        :cl+ssl)
  (:local-nicknames (:fs   :filesystem-utils)
                    (:misc :misc-utils))
  (:export
   :all-suite
   :run-all-tests))

(defpackage :tls-tests
  (:use :cl
        :clunit
        :text-utils
        :all-tests)
  (:local-nicknames (:fs   :filesystem-utils)
                    (:misc :misc-utils))
  (:export))

(defpackage :core-test-tests
  (:use
   :cl
   :clunit
   :text-utils
   :all-tests
   :core-tests)
  (:local-nicknames (:fs   :filesystem-utils)
                    (:misc :misc-utils))
  (:export))

(defpackage :queue-tests
  (:use :cl
        :clunit
        :text-utils
        :all-tests)
  (:local-nicknames (:fs   :filesystem-utils)
                    (:misc :misc-utils)
                    (:q    :synchronized-queue))
  (:export))

(defpackage :graph-tests
  (:use :cl
        :clunit
        :text-utils
        :all-tests
        :graph)
  (:local-nicknames (:fs   :filesystem-utils)
                    (:misc :misc-utils)
                    (:a    :alexandria)
                    (:q    :synchronized-queue))
  (:export))

(defpackage :fifo-communication-tests
  (:use :cl
        :clunit
        :text-utils
        :all-tests)
  (:local-nicknames (:fs   :filesystem-utils)
                    (:misc :misc-utils)
                    (:a    :alexandria)
                    (:p    :esrap)
                    (:fifo :fifo-communication)
                    (:q    :synchronized-queue))
  (:export))

(defpackage :misc-tests
  (:use :cl
        :clunit
        :text-utils
        :misc-utils
        :all-tests)
  (:local-nicknames (:a :alexandria))
  (:export))
