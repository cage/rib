;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :core-test-tests)

(defsuite core-test-suite (all-suite tls-tests::tls-suite))

(alexandria:define-constant +test-host+ "www.autistici.org"         :test #'string=)

(alexandria:define-constant +test-uri+ "https://www.autistici.org/interzona/" :test #'string=)

(deftest test-certificates (core-test-suite)
  (assert-true  (core-tests:certificate-near-to-expiration-p +test-host+ 500))
  (assert-false (core-tests:certificate-expired-p +test-host+)))

(deftest test-http-code (core-test-suite)
  (assert-true (core-tests:http-ok-code-p +test-uri+)))

(alexandria:define-constant +test-regex-uri+
  (list "https://www.autistici.org/interzona/"
        "Welcome to my  website!")
  :test #'equalp)

(alexandria:define-constant +test-regex-uri-2+
  (list  "https://www.autistici.org/interzona/fulci.html"
         "237")
  :test #'equalp)

(deftest test-http-regex (core-test-suite)
  (assert-true (core-tests:http-check-regex (first  +test-regex-uri+)
                                            (second +test-regex-uri+))))

(deftest test-average (core-test-suite)
  (assert-equality #'=
      7/2
      (average '(1 2 3 4 5 6))))

(deftest test-median (core-test-suite)
  (assert-equality #'=
      2
      (median '(0 0 0 4 6 7))))

(deftest test-histogram-failures-count (core-test-suite)
  (assert-true (test-histogram-failures-count '(nil nil nil t t))))

(defun analyze-histogram (histogram)
  (core-tests:test-histogram-failures histogram
                                      :count-threshold (* (length histogram) 0.51)
                                      :median-window-threshold 0.65))

(defun histogram-analysys-results= (analysis expected-results)
  (let ((list (list (eq (core-tests::fails-count-p  analysis)       (elt expected-results 0))
                    (eq (core-tests::fails-window-p analysis)       (elt expected-results 1))
                    (eq (core-tests::last-histogram-value analysis) (elt expected-results 2))
                    (eq (core-tests::total-failure-p analysis)      (elt expected-results 3)))))
    (every (lambda (a) (not (null a))) list)))

(deftest heuristic-histogram-tests (core-test-suite)
  (assert-true (histogram-analysys-results=
                (analyze-histogram '(t t t t t t t t t t t t t t))
                '(t t t t)))
  (assert-true (histogram-analysys-results=
                (analyze-histogram '(t t t t t t t t t t t t nil t))
                '(t nil t nil)))
  (assert-true (histogram-analysys-results=
                 (analyze-histogram '(t t t t t t t t t t t t nil nil))
                 '(t nil nil nil)))
  (assert-true (histogram-analysys-results=
                (analyze-histogram '(t t t nil nil nil t t t t t t t t))
                '(t t t nil)))
  (assert-true (histogram-analysys-results=
                (analyze-histogram '(nil nil nil nil t t t t t t t t t t))
                '(t t t nil)))
  (assert-true (histogram-analysys-results=
                (analyze-histogram '(nil nil nil nil nil nil nil nil nil nil nil nil nil t))
                '(nil t t nil)))
  (assert-true (histogram-analysys-results=
                (analyze-histogram '(nil nil nil nil nil nil nil nil nil nil t t t t))
                '(nil t t nil))))

(deftest parse-frequency-tests (core-test-suite)
  (assert-equality #'string=
      :one-shot
      (core-tests::parse-test-frequency "once"))
  (assert-equality #'string=
      :one-shot
      (core-tests::parse-test-frequency "boot"))
  (assert-equality #'string=
      :one-shot
      (core-tests::parse-test-frequency "  once  "))
  (assert-equality #'string=
      :one-shot
      (core-tests::parse-test-frequency " boot  "))
  (assert-false (core-tests::parse-test-frequency "onc"))
  (assert-equality #'=
      (misc:seconds->milliseconds 86400)
      (core-tests::parse-test-frequency "1d"))
  (assert-equality #'=
      (core-tests::parse-test-frequency "daily")
      (core-tests::parse-test-frequency "1d"))
  (assert-equality #'=
      (misc:seconds->milliseconds 172800)
      (core-tests::parse-test-frequency "2d"))
  (assert-equality #'=
      (misc:seconds->milliseconds 10)
      (core-tests::parse-test-frequency "10s"))
  (assert-equality #'=
      (misc:seconds->milliseconds 10800)
      (core-tests::parse-test-frequency "3h"))
  (assert-equality #'=
      (misc:seconds->milliseconds 3600)
      (core-tests::parse-test-frequency "1h"))
  (assert-equality #'=
      (core-tests::parse-test-frequency "hourly")
      (core-tests::parse-test-frequency "1h"))
  (assert-equality #'=
      (misc:seconds->milliseconds 604800)
      (core-tests::parse-test-frequency "7d"))
  (assert-equality #'=
      (core-tests::parse-test-frequency "weekly")
      (core-tests::parse-test-frequency "7d"))
  (assert-equality #'=
      (misc:seconds->milliseconds 2592000)
      (core-tests::parse-test-frequency "30d"))
  (assert-equality #'=
      (core-tests::parse-test-frequency "monthly")
      (core-tests::parse-test-frequency "30d"))
  (assert-equality #'=
      (misc:seconds->milliseconds 1800)
      (core-tests::parse-test-frequency "30m"))
  (assert-equality #'=
      (core-tests::parse-test-frequency "half-hour")
      (core-tests::parse-test-frequency "30m"))
  (assert-equality #'=
      (misc:seconds->milliseconds 31536000)
      (core-tests::parse-test-frequency "365d"))
  (assert-equality #'=
      (core-tests::parse-test-frequency "yearly")
      (core-tests::parse-test-frequency "365d"))
  (assert-false (core-tests::parse-test-frequency "1y"))
  (assert-false (core-tests::parse-test-frequency "ad")))
