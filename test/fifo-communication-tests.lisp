;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :fifo-communication-tests)

(defsuite fifo-communication-suite (all-suite))

(deftest test-parser (fifo-communication-suite)
  (let ((parsed (p:parse 'fifo::command "select task-history")))
    (assert-equalp '(:query "task-history" NIL)
        parsed))
  (let ((parsed (p:parse 'fifo::command "select task-history where a like \"b\"")))
    (assert-equalp '(:query "task-history")
        (subseq parsed 0 2))
    (assert-true (functionp (third parsed))))
  (assert-true
      (let ((q (p:parse 'fifo::command
                        "select task-history where not a like \"p\" or not a like \"q\"")))
        (funcall (third q) '(:a "jpj"))))
  (assert-false
      (let ((q (p:parse 'fifo::command
                        "select task-history where not a like \"p\" or not a like \"q\"")))
        (funcall (third q) '(:a "jqpj"))))
  (assert-true
      (let ((q (p:parse 'fifo::command
                        "select task-history where not a like \"p\" and not a like \"q\"")))
        (funcall (third q) '(:a "jj"))))
  (assert-false
      (let ((q (p:parse 'fifo::command
                        "select task-history where not a like \"p\" and not a like \"q\"")))
        (funcall (third q) '(:a "jpj"))))
  (assert-false
      (let ((q (p:parse 'fifo::command
                        "select task-history where not a like \"p\" and not a like \"q\"")))
        (funcall (third q) '(:a "jqj"))))
  (assert-false
      (let ((q (p:parse 'fifo::command
                        "select task-history where not a like \"p\" and not a like \"q\"")))
        (funcall (third q) '(:a "jqpj"))))
  (assert-true
      (let ((q (p:parse 'fifo::command
                        "select a where ( b like \"p\" and b like \"q\" ) or b like \"f\"")))
        (funcall (third q) '(:b "amgf"))))
  (assert-false
      (let ((q (p:parse 'fifo::command
                        "select a where  b like \"p\" and b like \"q\"  or b like \"f\"")))
        (funcall (third q) '(:b "aqgf"))))
  (assert-true
      (let ((q (p:parse 'fifo::command
                        "select a where not (b like \"p\" or b like \"f\" ) or b like \"q\"")))
        (funcall (third q) '(:b "m"))))
  (assert-true
      (let ((q (p:parse 'fifo::command
                        "select a where not ( b like \"p\" or b like \"f\") or b like \"q\"")))
        (funcall (third q) '(:b "pq"))))
  (assert-false
      (let ((q (p:parse 'fifo::command
                        "select a where not (  b like \"p\" or b like \"f\"  ) or b like \"q\"")))
        (funcall (third q) '(:b "pg"))))
  (assert-true
      (let ((q (p:parse 'fifo::command
                        ;; short circuit test: 'a' column does not exists and test
                        ;; 'a = "q"' is not performed
                     "select a where not (b like \"p\" or b like \"f\") or a = \"q\"")))
     (funcall (third q) '(:b "q"))))
  (assert-condition conditions::column-not-found
      (let ((q (p:parse 'fifo::command
                        ;; short circuit test: 'b' column does not exists
                     "select a where not (b like \"p\" or b like \"f\" ) or a = \"q\"")))
     (funcall (third q) '(:a "q")))))
