;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli studi di Palermo

;; derived from tinmop © 2022 cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

;; derived from

;; This software is Copyright ©  2021 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(in-package :misc-tests)

(defsuite misc-suite (all-suite))

(deftest conversion-time-tests (misc-suite)
  (assert-equality #'= 1000 (seconds->milliseconds 1)))

(deftest parse-frequency-tests ((misc-suite) (conversion-time-tests))
  (assert-equality #'=
      (seconds->milliseconds 86400)
      (parse-frequency "1d"))
  (assert-equality #'=
      (parse-frequency "daily")
      (parse-frequency "1d"))
  (assert-equality #'=
      (seconds->milliseconds 172800)
      (parse-frequency "2d"))
  (assert-equality #'=
      (seconds->milliseconds 10)
      (parse-frequency "10s"))
  (assert-equality #'=
      (seconds->milliseconds 10800)
      (parse-frequency "3h"))
  (assert-equality #'=
      (seconds->milliseconds 3600)
      (parse-frequency "1h"))
  (assert-equality #'=
      (parse-frequency "hourly")
      (parse-frequency "1h"))
  (assert-equality #'=
      (seconds->milliseconds 604800)
      (parse-frequency "7d"))
  (assert-equality #'=
      (parse-frequency "weekly")
      (parse-frequency "7d"))
  (assert-equality #'=
      (seconds->milliseconds 2592000)
      (parse-frequency "30d"))
  (assert-equality #'=
      (parse-frequency "monthly")
      (parse-frequency "30d"))
  (assert-equality #'=
      (seconds->milliseconds 1800)
      (parse-frequency "30m"))
  (assert-equality #'=
      (parse-frequency "half-hour")
      (parse-frequency "30m"))
  (assert-equality #'=
      (seconds->milliseconds 31536000)
      (parse-frequency "365d"))
  (assert-equality #'=
      (parse-frequency "yearly")
      (parse-frequency "365d"))
  (assert-false (parse-frequency "1y"))
  (assert-false (parse-frequency "once"))
  (assert-false (parse-frequency "ad")))
