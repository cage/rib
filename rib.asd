(defsystem :rib
  :author      "cage"
  :license     "GPLv3"
  :version     "0.0.6"
  :pathname    "src"
  :serial      t
  :bug-tracker "https://codeberg.org/cage/rib/issues"
  :depends-on (:alexandria
               :cl+ssl
               :trivial-ssh
               :drakma
               :cl-ppcre-unicode
               :alexandria
               :esrap
               :crypto-shortcuts
               :croatoan
               :access
               :marshal
               :bordeaux-threads
               :local-time
               :cl-colors2
               :cl-i18n
               :ieee-floats
               :parse-number
               :unix-opts
               :drakma
               :usocket
               :cffi
               :babel
               :percent-encoding
               :cl-smtp
               :clop
               :magicffi
               :croatoan
               :yason
               :vom)
  :components ((:file "packages")
               (:file "config")
               (:file "constants")
               (:file "conditions")
               (:file "hooks")
               (:file "os-utils")
               (:file "misc-utils")
               (:file "text-utils")
               (:file "graph")
               (:file "modules")
               (:file "filesystem-utils")
               (:file "resources-utils")
               (:file "synchronized-queue")
               (:file "ssl-utils")
               (:file "http-utils")
               (:file "tui-utils")
               (:file "software-configuration")
               (:file "threads-management")
               (:file "core-tests")
               (:file "notification")
               (:file "notification-channels")
               (:file "statistics")
               (:file "program-management")
               (:file "fifo-communication")
               (:file "server")
               (:file "client")
               (:file "drawing")
               (:file "command-line")
               (:file "main")))

(defsystem :rib/test
  :description "Test suite for rib."
  :author      "cage"
  :license     "LLGPL"
  :version     "0.0.1"
  :serial      t
  :pathname    "test"
  :depends-on (:alexandria
               :cl-ppcre
               :cl+ssl
               :clunit2
               :usocket
               :babel
               :uiop
               :rib)
  :components ((:file "packages")
               (:file "all-tests")
               (:file "tls-tests")
               (:file "core-test-tests")
               (:file "queue-tests")
               (:file "graph-tests")
               (:file "fifo-communication-tests")
               (:file "misc-tests")))

;;(push :mock-email *features*)

;;(push :mock-telegram *features*)
