;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

;; derived from tinmop © 2022 cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :core-tests)

(defun milliseconds-now ()
  "Returns the number of milliseconds elapsed from the unix epoch."
  (milliseconds-from-unix-epoch))

(defclass task ()
  ((id
    :initform ""
    :initarg  :id
    :reader   id
    :type string
    :documentation "Unique identifier for the task")
   (dependencies
    :initform '()
    :initarg  :dependencies
    :accessor dependencies
    :type list
    :documentation "A  list of tasks  that need to be  performed before
    this instance")
   (one-shot
    :initform nil
    :initarg  :one-shot
    :reader   one-shot-p
    :writer   (setf one-shot)
    :documentation "Non  nil if the  task mus be performed  just once,
    when the program starts")
   (frequency
    :initform 5
    :initarg  :frequency
    :accessor frequency
    :documentation "frequency to run the test in milliseconds.")
   (last-time-run-at
    :initform (milliseconds-now)
    :initarg  :last-time-run-at
    :accessor last-time-run-at
    :documentation "The last time this task was performed encoded as milliseconds from the
start of the process")
   (last-time-triggered-p
    :initform nil
    :initarg  :last-time-triggered-p
    :accessor last-time-triggered-p
    :documentation "Non  nil if the  last time the task  was performed
    also returned a non nil value")
   (last-time-notified-at
    :initform (milliseconds-now)
    :initarg  :last-time-notified-at
    :accessor last-time-notified-at
    :documentation "The  last time  this task's results  was notified,
    encoded as milliseconds from the start of the process")
   (respects-grace-time
    :initform t
    :initarg  :respects-grace-time
    :writer   (setf respects-grace-time)
    :reader   respects-grace-time-p
    :documentation "Non nil if two consecutive failed tasks (i.e. that
    returned a  non nil value)  must not  be notified until  the grace
    time passed")
   (trigger-function-lock
    :initform (bt:make-lock)
    :initarg  :trigger-function-lock
    :accessor trigger-function-lock)
   (trigger-function
    :initform (if (swconf:debug-mode-p)
                  (lambda (&rest args)
                    (declare (ignore args))
                    (values :debug "dummy notification" :null))
                  (lambda (&rest args)
                    (declare (ignore args))
                    nil))
    :initarg  :trigger-function
    :accessor trigger-function
    :documentation "A function that run the task.
    Must returns nil to  indicate that no notification is needed
    four, or five values:


    - if `ARGS' is null, returns  `nil' if the task passed
      (i.e. no notification is needed), three or four values -instead- if
      the task needs to send a notification, in details:
    - the level of the notification (~:info~, ~:error~, ~:warning~,
      ~:debug~);
    - the text of the notification;
    - the notification channel (just ~:mail~, ~telegram~ or
      ~:all~, any other value means that no notification will be sent);
    - a string that specify a regular expression (see regex(7))
      matching the the destination group or groups;
    - a, possibly  empty, list that  specify absolute file paths  of additional
      data to be attached to notification.

    If the function is called with argument: \"frequency\", the frequency of the
    task is returned instead.

    If the function is  called with argument: \"respects-grace-time\",
    a generalized  boolean is  returned that  , if  not nil,  instructs the
    program  to  *not*  respects   grace  time  (i.e.   send  consecutive
    notifications)

    The function is  called with argument: \"init\" only  once, just after
    have been loaded, this way  the task's code can perform initialization
    duties.

    Finally if the function is  called with argument: \"dependencies\", a
    list of  dependencies, as file paths  or URL, of the  task is returned
    instead.


    Frequency format
    FREQUENCY         := LITERAL-FREQUENCY | NUMBER SPACE* UNITS
    LITERAL-FREQUENCY := \"once\" | \"half-hour\" | \"hourly\" | \"daily\" | \"weekly\" | \"monthly\" | \"yearly\"
    ONCE              := \"once\" | \"boot\"
    NUMBER            := [0-9]+
    UNITS             := 'h' 'm' 's' 'd' 'ms'
    SPACE             := ASCII #x20

    frequency example:

    \"1000ms\" → 1000 milliseconds (1 second)
    \"10s\"    → ten second
    \"2h\"     → two hours
    \"1d\"     → one day
    \"once\"   → run and discard the task")))

(defmethod print-object ((object task) stream)
  (print-unreadable-object (object stream :type t :identity nil)
    (format stream "id: ~a" (id object))))

(defmethod ms:class-persistent-slots ((object task))
  '(id
    one-shot
    frequency
    last-time-run-at))

(defmethod clone ((object task))
  (with-simple-clone (object 'task)))

(defmethod clone-into ((from task) (to task))
  (setf (slot-value to 'id)   (id from)
        (one-shot   to)       (one-shot-p from)
        (frequency  to)       (frequency from)
        (last-time-run-at to) (last-time-run-at from))
  to)

(defun frequency< (a b)
  "Predicate to sort in ascending order the task based on frequency of
the task."
  (< (frequency a) (frequency b)))

(defun last-time> (a b)
  "Predicate to  sort in descending order  the task based on  the time
the task was performed."
  (> (last-time-run-at a) (last-time-run-at b)))

(defun last-time< (a b)
  "Predicate to  sort in ascending order  the task based on  the time
the task was performed."
  (< (last-time-run-at a) (last-time-run-at b)))

(defun frequency-sorting-tolerance (frequency-difference)
  "Returns the tolerance when comparing two tasks frequency.

Uses a Michaelis Menten function to reduce tolerance as the difference
in frequency of the tasks increases

The argument `FREQUENCY-DIFFERENCE' is  the difference in frequency of
the task.

https://en.wikipedia.org/wiki/Michaelis%E2%80%93Menten_kinetics"
  (multiple-value-bind (max k)
      (swconf:queue-sorting-parameters)
    (misc:enzyme-kinetics max k frequency-difference)))

(defun queue-sort-predicate (a b)
  "Predicate to sort the task's queue.

If  the  two tasks  `A'  and  `B' have  the  same  tolerance within  a
tolerance, assign higher priority the the task that has lower frequency.

if the frequency  differs assign higher priority to the  task that was
performed earlier."
  (let* ((frequency-a     (frequency a))
         (frequency-b     (frequency b))
         (delta-frequency (- frequency-b frequency-a))
         (frequency-diff  (abs delta-frequency)))
    (with-epsilon ((frequency-sorting-tolerance frequency-diff))
      (if (epsilon= (last-time-run-at a)
                    (last-time-run-at b))
          (frequency< a b)
          (last-time< a b)))))

(a:define-constant +task-db-dump-filename+ "task-db.sexp" :test #'string=)

(defparameter *all-tasks*        '())

(defparameter *all-tasks-lock*    (bt:make-lock))

(defun %db-find-task (db id)
  (find-if (lambda (a) (string= id (id a))) db))

(defun-w-lock db-task-find (id)
    *all-tasks-lock*
  (%db-find-task *all-tasks* id))

(defun-w-lock db-task-delete (id)
    *all-tasks-lock*
  (let ((filtered (loop for task in *all-tasks*
                        when (not (string= id (id task)))
                          collect task)))
    (setf *all-tasks* filtered)
    *all-tasks*))

(defun-w-lock db-save-task (task)
    *all-tasks-lock*
  (pushnew task *all-tasks* :test (lambda (a b) (string= (id a) (id b)))))

(defun-w-lock db-task-update-time-run (id new-time)
    *all-tasks-lock*
  (a:when-let ((task (%db-find-task *all-tasks* id)))
    (setf (last-time-run-at task) new-time)
    task))

(defun dump-db-task ()
  (let ((filename (os-utils:cached-file-path +task-db-dump-filename+)))
    (log-message :info
                 (_ "Dumping tasks database in ~a")
                 filename)
    (with-open-file (stream
                     filename
                     :direction          :output
                     :if-exists          :supersede
                     :if-does-not-exist  :create)
      (misc:serialize-to-stream *all-tasks* stream))))

(defun revive-db-task ()
  (let* ((filename (os-utils:cached-file-path +task-db-dump-filename+)))
    (when (fs:file-exists-p filename)
      (let ((tasks (misc:deserialize (fs:namestring->pathname filename))))
        (log-message :info
                     (n_ "Restored ~a task from disk"
                         "Restored ~a task from disk"
                         (length tasks))
                     (length tasks))
        tasks))))

(defparameter *waiting-queue*    (make-instance 'q:queue
                                                :sorting-predicate #'queue-sort-predicate))

(defparameter *fulfilled-queue*  (make-instance 'q:queue
                                                :sorting-predicate #'queue-sort-predicate))

(defparameter *processing-queue* (make-instance 'q:queue
                                                :sorting-predicate #'queue-sort-predicate))

(defun milliseconds-from-last-trigger (task)
  "Milliseconds before the task expires."
  (with-accessors ((last-time-run-at last-time-run-at)) task
    (let ((now (milliseconds-now)))
      (- now last-time-run-at))))

(defun test-expired-p (task)
  (with-accessors ((frequency frequency)) task
    (>= (milliseconds-from-last-trigger task)
        frequency)))

(defun run-notification-hooks (task channel level payload metadata destination generate-and-send)
  (when hooks:*channel-notification-hook*
    (hooks:run-hook 'hooks:*channel-notification-hook*
                    task
                    channel
                    level
                    payload
                    metadata
                    destination
                    generate-and-send)))

(defun make-notification (channel level payload metadata destination
                          &key
                            (generate-and-send t)
                            (task nil)
                            (attachments nil))
  (flet ((send-mail ()
           (let ((notification (make-instance 'notification:notification-mail
                                              :attachments attachments
                                              :metadata metadata
                                              :payload  payload
                                              :level    level
                                              :destination destination
                                              :send-to-destination generate-and-send)))
             (when generate-and-send
               (notification:enqueue notification))
             notification))
         (send-telegram ()
           (let ((notification (make-instance 'notification:notification-telegram
                                              :metadata metadata
                                              :payload payload
                                              :level   level
                                              :destination destination
                                              :send-to-destination generate-and-send)))
             (when generate-and-send
               (notification:enqueue notification))
             notification))
         (send-noop ()
           (notification:enqueue (make-instance 'notification:notification-noop
                                                :metadata metadata
                                                :payload payload
                                                :level   level
                                                :destination destination
                                                :send-to-destination generate-and-send))))
    (when (null generate-and-send)
      (misc:log-message :debug "the notification will NOT be sent"))
    (run-notification-hooks task
                            channel
                            level
                            payload
                            metadata
                            destination
                            generate-and-send)
    (cond
      ((eq channel :mail)
       (send-mail))
      ((eq channel :telegram)
       (send-telegram))
      ((eq channel :all)
       (send-mail)
       (send-telegram))
      (t
       (send-noop)))))

(defun debug-queue->string (queue)
  (let ((items (mapcar (lambda (a) (format nil "~a:~a"
                                           (frequency a)
                                           (last-time-run-at a)))
                       (q:container queue))))
    (format nil "~{~a ~}" items)))

(defun add-to-history (task notification)
  (let* ((history-item (make-instance 'statistics:test-results
                                      :task         (clone task)
                                      :notification notification)))
    (statistics:add-history-item history-item)
    (client:send-test-results history-item)))

(defun fire-trigger-function (task &optional (seen '()))
  (with-accessors ((dependencies dependencies)
                   (trigger-function-lock trigger-function-lock)) task
    (loop for dependency in dependencies do
      (a:when-let ((dependency-task (db-task-find dependency)))
        (when (not (member dependency-task seen))
          (push dependency-task seen)
          (multiple-value-bind (level
                                notification-text
                                notification-channel
                                destination)
              (fire-trigger-function dependency-task seen)
            (when level
              (return-from fire-trigger-function
                (values level
                        (format nil
                                "~a ~a~2%~a"
                                (_ "Failed dependency: ")
                                dependency-task
                                notification-text)
                        notification-channel
                        destination)))))))
    (multiple-value-bind (level
                          notification-text
                          notification-channel
                          destination
                          attachments)
        (with-lock (trigger-function-lock)
          (funcall (trigger-function task)))
      (values level
              notification-text
              notification-channel
              destination
              attachments))))

(defun delay-task (task &key (delay-step 60))
  (let* ((sleep-milliseconds (max 1
                                  (- (frequency task)
                                     (milliseconds-from-last-trigger task))))
         (actual-delay-step  (min sleep-milliseconds delay-step))
         (now                (milliseconds-now)))
    (log-message :debug "delay from ~a to ~a~%" now (+ now sleep-milliseconds))
    (loop for i from now below (+ now sleep-milliseconds) by actual-delay-step
          while (not (tsm:stop-all-threads-p))
          do
             (sleep-milliseconds actual-delay-step))))

(defun send-consecutive-notification-anyway-p (task)
  (or (not (respects-grace-time-p task))
      (grace-period-passed-p (last-time-notified-at task))))

(defun grace-period-passed-p (last-time-notified)
  (a:when-let ((now       (milliseconds-now))
               (threshold (swconf:treshold-deny-consecutive-notifications)))
    (> (- now last-time-notified)
       threshold)))

(defun make-testing-thread (count)
  (lambda ()
    (log-message :info (_ "Starting testing thread #~a.") count)
    (loop while (not (tsm:stop-all-threads-p)) do
      (log-message :debug "~a popping ~a." count
                   (debug-queue->string *waiting-queue*))
      (let ((task (q:pop-block *waiting-queue*)))
        (log-message :debug "~a popped ~a once? ~a last time: ~a expired? ~a."
                     count
                     (text-utils:to-s task)
                     (one-shot-p task)
                     (last-time-run-at task)
                     (test-expired-p task))
        (if (or (one-shot-p task)
                (test-expired-p task))
            (handler-bind
                ((error
                   (lambda (e)
                     (let ((message (format nil
                                            (_ "Thread crashed running test ~s, error: ~a~%backtrace ~a~%")
                                            (id task)
                                            e
                                            (misc:backtrace e))))
                       (when (swconf:notify-crash-p)
                         (let* ((notification (make-notification :all
                                                                 :crit
                                                                 message
                                                                 (id task)
                                                                 swconf:+admin-smtp-destination-group+)))
                           (add-to-history task notification)))
                       (log-message :crit message)
                       (invoke-restart 'use-value t)))))
              (restart-case
                  (multiple-value-bind (level
                                        notification-text
                                        notification-channel
                                        destination
                                        attachments)
                      (fire-trigger-function task)
                    (log-message :debug
                                 "~a fired, trigger activated? ~a"
                                 (text-utils:to-s task)
                                 level)
                    (setf (last-time-run-at task) (milliseconds-now))
                    (db-task-update-time-run (id task) (last-time-run-at task))
                    (if level
                        (let* ((send-notification-p (or (swconf:send-consecutive-notifications-p)
                                                        (not (last-time-triggered-p task))
                                                        (send-consecutive-notification-anyway-p task)))
                               (notification        (make-notification notification-channel
                                                                       level
                                                                       notification-text
                                                                       (id task)
                                                                       destination
                                                                       :generate-and-send
                                                                       send-notification-p
                                                                       :task task
                                                                       :attachments
                                                                       attachments)))
                          (setf (last-time-triggered-p task) t)
                          (if send-notification-p
                              (setf (last-time-notified-at task) (milliseconds-now))
                              (log-message :debug
                                           "~a fired, trigger activated but previously fired, skipping notification"
                                           (text-utils:to-s task)))
                          (add-to-history task notification))
                        (progn
                          (add-to-history task nil)
                          (if (last-time-triggered-p task)
                              (log-message :debug
                                           "~a not fired, preparing for a new notification"
                                           (text-utils:to-s task))
                              (log-message :debug
                                           "~a not fired, and last time also has not fired"
                                           (text-utils:to-s task)))
                          (setf (last-time-triggered-p task) nil))))
                (use-value (e) e)))
            (let ((sleep-milliseconds (max 1             ; here runs the branch when task has not expired
                                           (- (frequency task)
                                              (milliseconds-from-last-trigger task)))))
              (log-message :debug
                           "task ~a not expired, sleeping for ~a milliseconds. l ~a texp ~a freq ~a"
                           task
                           sleep-milliseconds
                           (last-time-run-at task)
                           (milliseconds-from-last-trigger task)
                           (frequency task))
              (delay-task task)))
        (log-message :debug "~a pushing for recycle" count)
        (q:push-unblock *fulfilled-queue* task)
        (log-message :debug "pushed for recycle")))
    (log-message :info "Stopping testing thread #~a." count)))

(defun stop-testing-threads ()
  (loop repeat (length tsm:*testing-pool*) do
    (q:push-unblock core-tests:*waiting-queue* (make-instance 'core-tests:task))))

(defun destroy-testing-threads ()
  (loop for thread in tsm:*testing-pool* do
    (bt:destroy-thread thread)))

(defun recycling-thread ()
  (log-message :info (_ "Starting recycling thread."))
  (loop while (not (tsm:stop-all-threads-p)) do
    (handler-bind
        ((error
           (lambda (e)
             (let ((message (format nil
                                    (_ "Recycling thread crashed error: ~a~%backtrace ~a~%")
                                    e
                                    (misc:backtrace e))))
               (log-message :crit message)))))
      (log-message :debug
                   "rec waiting popping from fulfilled")
      (let ((task (q:pop-block *fulfilled-queue*)))
        (log-message :debug
                     "recycle popped ~s"
                     (text-utils:to-s task))
        (if (one-shot-p task)
            (log-message :debug
                         "this test meant to run just once")
            (let ((id-task (id task)))
              (when hooks:*before-recycling-hook*
                (setf task
                      (hooks:run-hook-compose 'hooks:*before-recycling-hook*
                                              task)))
              (if (null task) ;; because the hooks can return nil
                  (log-message :info (_ "deleting task ~s") id-task)
                  (progn
                    (q:push-unblock *waiting-queue* task)
                    (log-message :debug "pushed for process"))))))))
  (log-message :info (_ "Stopping recycling thread.")))

(defun make-recycling-thread ()
  (let ((thread (bt:make-thread #'recycling-thread
                                :name "recycling thread")))
    (setf tsm:*recycling-thread* thread)))

(a:define-constant +one-shot-frequency-task-field-values+ (list "once"
                                                                "boot")
  :test #'equalp)

(defun parse-test-frequency (value)
  (let ((trimmed-value   (text-utils:trim-blanks value))
        (frequency-value (misc:parse-frequency value)))
    (cond
      (frequency-value
       frequency-value)
      ((member trimmed-value +one-shot-frequency-task-field-values+ :test #'string-equal)
       :one-shot)
      (t
       nil))))

(a:define-constant +output-fields-separator+ (string (code-char 9)) :test #'string=)

(defgeneric parse-task-dependencies (object))

(defgeneric remove-task-dependencies (object id-task-to-remove))

(defmethod parse-task-dependencies ((object string))
  (remove-if #'text-utils:string-empty-p
             (mapcar #'text-utils:trim-blanks
                     (cl-ppcre:split +output-fields-separator+ object))))

(defmethod parse-task-dependencies ((object list))
  (assert (every #'stringp object))
  object)

(defmethod remove-task-dependencies ((object task) (id-task-to-remove string))
  (setf (dependencies object)
        (remove id-task-to-remove (dependencies object) :test #'string=)))

(defun split-external-program-output (output args)
  (a:when-let* ((lines (remove-if #'text-utils:string-empty-p
                                  (mapcar #'text-utils:trim-blanks
                                          (cl-ppcre:split +output-fields-separator+ output)))))
    (cond
      ((arguments-asks-dependencies-p args)
       lines)
      ((arguments-asks-frequency-p args)
       (first lines))
      ((arguments-asks-respects-grace-time-p args)
       (string-not-empty-p (first lines)))
      ((arguments-init-p args)
       lines)
      (t
       (cond
         ((< (length lines) 4)
          nil)
         (t
          (let ((triggeredp (not (string-empty-p (first lines)))))
            (values (and triggeredp
                         (a:make-keyword (string-upcase (first lines)))) ; results
                    (second lines)                                       ; level
                    (a:make-keyword (string-upcase (third lines)))       ; channel
                    (fourth lines)                                       ; destination
                    (subseq lines 4)))))))))                             ; optional attachments

(defparameter *loaded-task-function* nil)

(defun task-file->function (file)
  (cond
    ((or (file-shell-script-p file)
         (file-elf-p file))
     (lambda (&rest args)
       (let ((output (os-utils:slurp-external-program file args)))
         (split-external-program-output output args))))
    (t
     (let ((*loaded-task-function* nil))
       (load file :verbose nil :print nil)
       (let ((task-function *loaded-task-function*))
         (lambda (&rest args)
           (multiple-value-bind (level text channel destination attachments)
               (apply task-function args)
             (values level text channel destination attachments))))))))

(defun file->task (file)
  (make-instance 'task
                 :id               (fs:path-last-element  file)
                 :trigger-function (task-file->function file)))

(defparameter *task-directory* nil)

(defun make-fetched-task (url re frequency delay count destination threshold median-threshold)
  (lambda (&rest args)
    (cond
      ((arguments-asks-frequency-p args)
       frequency)
      ((arguments-asks-dependencies-p args)
       '())
      ((arguments-asks-respects-grace-time-p args)
       t)
      ((arguments-init-p args)
       t)
      (t
       (let ((last-time-notified nil))
         (labels ((loop-fn (&optional (error-count 10))
                    (handler-case
                        (progn
                          (a:when-let ((results (or (http-trigger-website-errors url
                                                                                 count
                                                                                 delay
                                                                                 :count-threshold
                                                                                 (* count
                                                                                    threshold)
                                                                                 :median-window-threshold
                                                                                 median-threshold)
                                                    (http-trigger-website-body url
                                                                               count
                                                                               delay
                                                                               re
                                                                               :count-threshold
                                                                               (* count
                                                                                  threshold)
                                                                               :median-window-threshold
                                                                               median-threshold))))
                            (values :warning
                                    results
                                    :all
                                    destination
                                    nil)))
                      (error (e)
                        (if (< error-count 0)
                            (when (or (null last-time-notified)
                                      (grace-period-passed-p last-time-notified))
                              (setf last-time-notified (milliseconds-now))
                              (values :warning
                                      (format nil (_ "Something wrong happened: ~a") e)
                                      :all
                                      swconf:+admin-smtp-destination-group+
                                      nil))
                            (progn
                              (sleep-milliseconds (seconds->milliseconds 12))
                              (loop-fn (1- error-count))))))))
           (loop-fn)))))))

(defun fetch-http-task ()
  (multiple-value-bind (url frequency delay
                        count regexp-key target-url-key
                        destination
                        threshold
                        median-threshold)
      (swconf:task-json-parameters)
    (when (string-not-empty-p url)
      (let* ((json        (to-s (http-utils:get-url-content-body url)))
             (tasks-hash (yason:parse json))
             (all-tasks (loop for task in tasks-hash
                              collect
                              (let* ((target-url (gethash target-url-key task))
                                     (regexp     (gethash regexp-key task))
                                     (fn         (make-fetched-task target-url
                                                                    regexp
                                                                    frequency
                                                                    delay
                                                                    count
                                                                    destination
                                                                    threshold
                                                                    median-threshold)))
                                (log-message :debug (format nil
                                                            (_ "fetching ~a")
                                                            target-url))
                                (when (and (string-not-empty-p target-url)
                                           (string-not-empty-p regexp)
                                           (ignore-errors
                                            (cl-ppcre:create-scanner regexp)))
                                  (make-instance 'task
                                                 :id               target-url
                                                 :trigger-function fn))))))
        (remove-if #'null all-tasks)))))

(a:define-constant +frequency-argument-trigger+     "frequency"           :test #'string=)

(a:define-constant +dependencies-argument-trigger+  "dependencies"        :test #'string=)

(a:define-constant +no-grace-time-argument-trigger+ "respects-grace-time" :test #'string=)

(a:define-constant +init-argument-trigger+          "init"                :test #'string=)

(defun arguments-asks-frequency-p (args)
  (string= (first args) +frequency-argument-trigger+))

(defun arguments-asks-dependencies-p (args)
  (string= (first args) +dependencies-argument-trigger+))

(defun arguments-asks-respects-grace-time-p (args)
  (string= (first args) +no-grace-time-argument-trigger+))

(defun arguments-init-p (args)
  (string= (first args) +init-argument-trigger+))

(defun load-task (task old-tasks-dump &key (initialize-task t))
  (let ((same-id-exists (q:find-value-if *waiting-queue*
                                         (lambda (a) (string= (id task) (id a))))))
    (if same-id-exists
        (make-notification :all
                           :warn
                           (format nil
                                   (_ "Loading of the task ~a failed because another with the same id was already loaded before")
                                   (id task))
                           ""
                           swconf:+admin-smtp-destination-group+)
        (let ((frequency    (parse-test-frequency (funcall (trigger-function task)
                                                           +frequency-argument-trigger+)))
              (dependencies (parse-task-dependencies (funcall (trigger-function task)
                                                              +dependencies-argument-trigger+)))
              (respects-grace-time-p (funcall (trigger-function task)
                                              +no-grace-time-argument-trigger+)))
          (if frequency
              (progn
                (when initialize-task
                  (funcall (trigger-function task) +init-argument-trigger+))
                (if (eq :one-shot frequency)
                    (setf (one-shot task) t)
                    (setf (frequency task) frequency))
                (setf (dependencies task) dependencies)
                (setf (respects-grace-time task) respects-grace-time-p)
                (a:when-let ((old-task-dump (%db-find-task old-tasks-dump (id task))))
                  (log-message :debug
                               (format nil
                                       "setting last-time-run at ~a for task ~a"
                                       (milliseconds-unix-epoch->timestamp (last-time-run-at old-task-dump))
                                       (id task)))
                  (setf (last-time-run-at task)
                        (last-time-run-at old-task-dump)))
                (log-message :debug (format nil "loading test ~s" (id task)))
                (db-save-task task)
                (q:push-unblock *waiting-queue* task))
              (make-notification :all
                                 :crit
                                 (format nil
                                         (_ "Could not parse frequency for test ~a")
                                         (id task))
                                 ""
                                 swconf:+admin-smtp-destination-group+))))))

(defun load-all-task-files (old-tasks-dump &key (initialize-task t))
  (let ((all-task-files (remove-if-not #'regular-file-p
                                       (fs:collect-children *task-directory*)))
        (ignored-scanner  (swconf:task-file-ignored-regexp)))
    (loop for task-file in all-task-files
          when (or (null ignored-scanner)
                   (not (cl-ppcre:scan ignored-scanner task-file)))
            do
               (let ((task (file->task task-file)))
                 (load-task task old-tasks-dump :initialize-task initialize-task)))))

(defun load-all-tasks (&key (initialize-task t))
  (setf *all-tasks* '())
  (let ((old-tasks-dump (revive-db-task)))
    (load-all-task-files old-tasks-dump :initialize-task initialize-task)
    (let ((fetched-tasks (fetch-http-task)))
      (loop for task in fetched-tasks do
        (load-task task old-tasks-dump :initialize-task initialize-task))))
  (check-circular-dependencies *all-tasks*))

(defun tasks->graph (tasks)
  (a:when-let* ((ids   (mapcar #'id tasks))
                (graph (make-instance 'graph:graph :nodes ids :equals-node-predicate #'string=)))
    (loop for task in tasks do
      (a:when-let ((dependencies (dependencies task)))
        (loop for dependency in dependencies do
          (graph:add-directed-arc graph (id task) dependency))))
    graph))

(defun all-tasks-graph ()
  (tasks->graph *all-tasks*))

(defun check-circular-dependencies (tasks)
  (a:when-let* ((graph (tasks->graph tasks))
                (cycles (graph:cyclep graph))
                (errors (loop for cycle in cycles
                              when cycle
                                collect
                                (format nil
                                        (_ "Circular dependency detected: ~{~a → ~} …~%")
                                        (mapcar #'graph:value cycle)))))
    (error (text-utils:strcat* errors))))

(defun certificate-expiration-days (host &key (port 443))
  (let* ((expiration-date (ssl-utils:fetch-certificate-expiry-date host port))
         (now             (local-time:now))
         (diff-seconds    (local-time:timestamp-difference expiration-date
                                                           now))
         (diff-days       (seconds->days diff-seconds)))
    (truncate diff-days)))

(defun certificate-near-to-expiration-p (host maximum-days &key (port 443))
  (<= (certificate-expiration-days host :port port)
      maximum-days))

(defun certificate-expired-p (host &optional (port 443))
  (let* ((expiration-date (ssl-utils:fetch-certificate-expiry-date host port))
         (now             (local-time:now)))
    (local-time:timestamp<= expiration-date now)))

(defun http-expected-code-p (uri expected-code &key (redirect 5))
  (let ((code (http-utils:get-url-code uri :redirect redirect)))
    (=  code expected-code)))

(defun http-ok-code-p (uri &key (redirect 5))
  (http-expected-code-p uri +http-code-ok+ :redirect redirect))

(defun http-check-body (uri fn)
  (funcall fn (http-utils:get-url-content-body uri)))

(defun http-check-header (uri header-name fn &key (redirect 5))
  (a:when-let ((header (http-utils:get-url-header uri header-name :redirect redirect)))
    (funcall fn header)))

(defun http-check-regex (uri regex)
  (let ((scanner (cl-ppcre:create-scanner regex)))
    (labels ((scan-line (line)
               (cl-ppcre:scan-to-strings scanner line))
             (scan-body (data)
               (let ((lines (text-utils:split-lines (to-s data))))
                 (loop for line in lines do
                   (let ((matched (scan-line line)))
                     (when matched
                       (return-from scan-body matched))))
                 nil)))
      (http-check-body uri #'scan-body))))

(defun http-test-no-timeout (uri seconds)
  (assert (integerp seconds))
  (assert (> seconds 0))
  (handler-case
      (misc:with-timeout (seconds)
        (http-utils:get-url-content-body uri :timeout nil))
    (sb-sys:deadline-timeout () :timeout)
    (error () nil)))

(defun http-test-no-errors (uri)
  (ignore-errors
   (http-utils:get-url-content-body uri)))

(defun test-many-times (count delay test-function &rest args)
  (flet ((call-test ()
           (apply test-function args)))
    (loop repeat count do
      (if (not (call-test))
          (return-from test-many-times nil)
          (sleep-milliseconds delay)))
    t))

(defun collect-tests-results (count delay test-function &rest args)
  (flet ((call-test ()
           (apply test-function args)))
    (loop repeat count
          collect
          (progn
            (sleep-milliseconds delay)
            (call-test)))))

(defun average (values)
  (/ (reduce #'+ values)
     (length values)))

(defun median (values)
  (let* ((length        (length values))
         (middle        (floor (/ length 2)))
         (sorted-values (sort values #'<)))
    (if (oddp length)
        (elt sorted-values middle)
        (let ((left  (elt sorted-values (1- middle)))
              (right (elt sorted-values middle)))
          (average (list left right))))))

(defstruct weighted-element
  (value)
  (weight))

(defun sort-weighted-population (population)
  (sort population (lambda (a b) (< (weighted-element-value a)
                                    (weighted-element-value b)))))

(defun weighted-median (population weights)
  (assert (a:length= population weights))
  (let* ((max-weight          (/ (reduce #'+ weights) 2))
         (weighted-population (loop for value in population
                                    for weight in weights
                                    collect
                                    (make-weighted-element :value  value
                                                           :weight weight)))
         (accumulated-weights 0))
    (loop for element in weighted-population do
      (incf accumulated-weights (weighted-element-weight element))
      (when (> accumulated-weights max-weight)
        (return-from weighted-median (weighted-element-value element))))))

(defun test-histogram-failures-count (histogram &optional (threshold (* (length histogram)
                                                                        0.2)))
  (> (count-if #'identity histogram)
     threshold))

(defun smoothstep (max value slope shift)
  (let ((mapping (if (< max 12)
                     (/ max 12)
                     (/ 12 max))))
    (/ 1
       (+ 1
          (exp (* (- slope)
                  (- (* value mapping)
                     (+ 6 shift))))))))

(defclass histogram-test-results ()
  ((fails-count
    :initform nil
    :initarg  :fails-count
    :reader fails-count-p)
   (fails-window
    :initform nil
    :initarg  :fails-window
    :reader fails-window-p)
   (last-histogram-value
    :initform nil
    :initarg  :last-histogram-value
    :accessor last-histogram-value)
   (total-failure
    :initarg :total-failure
    :reader total-failure-p)))

(defmethod print-object ((object histogram-test-results) stream)
  (print-unreadable-object (object stream :type t :identity nil)
    (format stream
            "fails count: ~a fails window: ~a last-value: ~a total failure: ~a"
            (fails-count-p        object)
            (fails-window-p       object)
            (last-histogram-value object)
            (total-failure-p      object))))

(defun make-histogram-test-results (fails-count fails-window last-histogram-value total-failure)
  (make-instance 'histogram-test-results
                 :fails-count          fails-count
                 :fails-window         fails-window
                 :last-histogram-value last-histogram-value
                 :total-failure        total-failure))

(defun test-histogram-failures (histogram
                                &key
                                  (count-threshold (* (length histogram)
                                                      0.2))
                                  (median-window-threshold 0.5))
  (cond
    ((notany #'null histogram)
     (make-histogram-test-results t t t t))
    ((every #'null histogram)
     (make-histogram-test-results nil nil nil nil))
    (t
     (let* ((population     (loop for i from 0 below (length histogram) collect i))
            (values-fails   (loop for i from 0 below (length histogram)
                                  when (elt histogram i)
                                    collect i))
            (fails-count    (test-histogram-failures-count histogram count-threshold))
            (population-max (reduce #'max population))
            (weights-fails  (loop for i in values-fails
                                  collect
                                  (smoothstep population-max i .25 1.0)))
            (median         (weighted-median values-fails weights-fails))
            (fails-window   (>= (/ median population-max)
                                median-window-threshold)))
       (make-histogram-test-results fails-count
                                    fails-window
                                    (a:last-elt histogram)
                                    nil)))))

(defparameter *histogram-pass-symbol* "✅")

(defparameter *histogram-fail-symbol* "❌")

(defun histogram->string (histogram)
  (let* ((labels          (loop for i in histogram
                                for j from 0
                                collect
                                (format nil "t~a" (text-utils:integer->subscript j))))
         (results         (loop for i in histogram collect (if i
                                                               *histogram-fail-symbol*
                                                               *histogram-pass-symbol*)))
         (max-label-width (reduce (lambda (a b) (if (> a
                                                       b)
                                                    a
                                                    b))
                                  labels
                                  :initial-value -1
                                  :key #'length))
         (paddings        (loop for label in labels
                                collect
                                (make-string (1+ (- max-label-width (length label)))
                                             :initial-element #\Space)))
         (table           (loop for label in labels
                                for result in results
                                for padding in paddings
                                collect
                                (format nil "~a~a→ ~a~%" label padding result))))
    (text-utils:strcat* table)))

(defun http-check-failures (count delay failure-function
                            &key
                              (count-threshold          (* count 0.2))
                              (median-window-threshold 0.5)
                              (retry-if-unstable t))
  (let* ((histogram (collect-tests-results count delay failure-function))
         (histogram-analysis (test-histogram-failures histogram
                                                      :count-threshold         count-threshold
                                                      :median-window-threshold median-window-threshold))
         (unstable (not (last-histogram-value histogram-analysis))))
    (log-message :debug "histogram ~a ~a"
                 histogram histogram-analysis)
    (cond
      ((total-failure-p histogram-analysis)
       (values (histogram->string histogram) :total-failure))
      ((and (fails-count-p histogram-analysis)
            (fails-window-p histogram-analysis))
       (if unstable
           (if retry-if-unstable
               (http-check-failures count
                                    delay
                                    failure-function
                                    :count-threshold         count-threshold
                                    :median-window-threshold median-window-threshold
                                    :retry-if-unstable       nil)
               (values (histogram->string histogram) :unstable))
           (values (histogram->string histogram) :over-thresholds))))))

(defun http-check-failures-re-body (count delay uri regex
                                    &key
                                      (count-threshold          (* count 0.2))
                                      (median-window-threshold 0.5))
  (http-check-failures count delay (lambda () (not (http-check-regex uri regex)))
                       :count-threshold          count-threshold
                       :median-window-threshold median-window-threshold))

(defun http-check-failures-code (count delay uri expected-code
                                 &key
                                   (redirect                5)
                                   (count-threshold         (* count 0.2))
                                   (median-window-threshold 0.5))
  (http-check-failures count delay (lambda () (not (http-expected-code-p uri
                                                                         expected-code
                                                                         :redirect redirect)))
                       :count-threshold           count-threshold
                       :median-window-threshold  median-window-threshold))

(defun http-check-failures-timeout (count delay uri deadline
                                    &key
                                      (count-threshold          (* count 0.2))
                                      (median-window-threshold 0.5))
  (http-check-failures count delay
                       (lambda () (eq (http-test-no-timeout uri deadline)
                                      :timeout))
                       :count-threshold           count-threshold
                       :median-window-threshold  median-window-threshold))

(defun http-check-failures-error (count delay uri
                                  &key
                                    (count-threshold          (* count 0.2))
                                    (median-window-threshold 0.5))
  (http-check-failures count delay
                       (lambda () (not (http-test-no-errors uri)))
                       :count-threshold           count-threshold
                       :median-window-threshold  median-window-threshold))

(defun http-check-header-regex (count delay uri header-name expected-header-value-regex
                                &key
                                  (redirect                5)
                                  (count-threshold         (* count 0.2))
                                  (median-window-threshold 0.5))
  (flet ((scan (header)
           (cl-ppcre:scan expected-header-value-regex
                          header)))
    (http-check-failures count delay (lambda ()
                                       (not (http-check-header uri
                                                               header-name
                                                               #'scan
                                                               :redirect redirect)))
                         :count-threshold          count-threshold
                         :median-window-threshold  median-window-threshold)))

(defun trigger-message-template (minutes
                                 times
                                 period
                                 uri
                                 message
                                 failure-ratio
                                 median-window-threshold
                                 histogram)
  (format nil
          (strcat (_ "In the latest ~,1f minutes I tested the URL ~s, ~a times and some failed for this reason:~2%~a~2%")
                  (_ "The number of tests failed is higher than the limit of ~,2f%, ")
                  (_ "with peak of failures in the latest tests (after ~,2f% of the tests was accomplished)~2%")
                  (_ "Results of the performed tests~2%~a~2%")
                  (n_ "Each time step values ~a millisecond.~%"
                      "Each time step values ~a milliseconds.~%"
                      period)
                  (_ "Legend: '~a' Test passed, '~a' test failed"))
          minutes
          uri
          times
          message
          (* failure-ratio 100)
          (* median-window-threshold 100)
          histogram
          period
          *histogram-pass-symbol*
          *histogram-fail-symbol*))

(defun trigger-message-template-total-failures (minutes
                                                times
                                                period
                                                uri
                                                message
                                                histogram)
  (format nil
          (strcat (_ "In the latest ~,1f minutes I tested the URL ~s, ~a times and all failed for this reason:~2%~a~2%")
                  (_ "Results of the performed tests~2%~a~2%")
                  (n_ "Each time step values ~a millisecond.~%"
                      "Each time step values ~a milliseconds.~%"
                      period)
                  (_ "Legend: '~a' Test passed, '~a' test failed"))
          minutes
          uri
          times
          message
          histogram
          period
          *histogram-pass-symbol*
          *histogram-fail-symbol*))

(defun http-trigger-website-timeout (uri count delay deadline
                                     &key
                                       (count-threshold          (* count 0.2))
                                       (median-window-threshold 0.5))
  (multiple-value-bind (results-histogram failure-reasons)
      (http-check-failures-timeout count
                                   delay
                                   uri
                                   deadline
                                   :count-threshold
                                   count-threshold
                                   :median-window-threshold
                                   median-window-threshold)
    (let* ((seconds   (milliseconds->seconds delay))
           (minutes   (seconds->minutes (* count seconds))))
      (when results-histogram
        (if (eq failure-reasons :total-failure)
            (trigger-message-template-total-failures minutes
                                                     count
                                                     delay
                                                     uri
                                                     (format nil
                                                             (n_ "no response after ~a second~%"
                                                                 "no response after ~a seconds~%"
                                                                 (floor deadline))
                                                             deadline)
                                                     results-histogram)
            (format nil
                    (strcat (_ "In the latest ~,1f minutes the URL ~a~%")
                            (n_ "has gone timeout (dead line ~a second) for more than ~,1f% of the times "
                                "has gone timeout (dead line ~a seconds) for more than ~,1f% of the times "
                                deadline)
                            (_ "with peak of failures in the latest tests (after ~,2f% of the tests was accomplished)~%")
                            (_ "Results of the performed tests~2%~a~%"))
                    minutes
                    uri
                    deadline
                    (* count-threshold 100)
                    (* median-window-threshold 100)
                    results-histogram))))))

(defun http-trigger-website-code (uri count delay expected-code
                                  &key
                                    (redirect                5)
                                    (count-threshold         (* count 0.2))
                                    (median-window-threshold 0.5))
  (multiple-value-bind (results-histogram failure-reasons)
      (http-check-failures-code count
                                delay
                                uri
                                expected-code
                                :redirect        redirect
                                :count-threshold count-threshold
                                :median-window-threshold
                                median-window-threshold)
    (let* ((seconds  (milliseconds->seconds delay))
           (minutes  (seconds->minutes (* count seconds)))
           (message  (format nil
                            (_ "returned a code different from the expected value (~a) ")
                            expected-code)))
      (when results-histogram
        (if (eq failure-reasons :total-failure)
            (trigger-message-template-total-failures minutes
                                                     count
                                                     delay
                                                     uri
                                                     message
                                                     results-histogram)
            (trigger-message-template minutes
                                      count
                                      delay
                                      uri
                                      message
                                      (/ count-threshold count)
                                      median-window-threshold
                                      results-histogram))))))

(defun http-trigger-website-body (uri count delay regexp
                                  &key
                                    (count-threshold          (* count 0.2))
                                    (median-window-threshold 0.5))
  (multiple-value-bind (results-histogram failure-reasons)
      (http-check-failures-re-body count
                                   delay
                                   uri
                                   regexp
                                   :count-threshold
                                   count-threshold
                                   :median-window-threshold
                                   median-window-threshold)
    (let* ((seconds (milliseconds->seconds delay))
           (minutes (seconds->minutes (* count seconds)))
           (message (format nil
                            (_ "returned a body that did not matched the regular expression~2%~s~2%")
                              regexp)))
      (when results-histogram
        (if (eq failure-reasons :total-failure)
            (trigger-message-template-total-failures minutes
                                                     count
                                                     delay
                                                     uri
                                                     message
                                                     results-histogram)
            (trigger-message-template minutes
                                      count
                                      delay
                                      uri
                                      message
                                      (/ count-threshold count)
                                      median-window-threshold
                                      results-histogram))))))

(defun http-trigger-website-header (uri count delay header-name header-value-regexp
                                    &key
                                      (redirect                5)
                                      (count-threshold         (* count 0.2))
                                      (median-window-threshold 0.5))
  (multiple-value-bind (results-histogram failure-reasons)
      (http-check-header-regex count
                               delay
                               uri
                               header-name
                               header-value-regexp
                               :redirect
                               redirect
                               :count-threshold
                               count-threshold
                               :median-window-threshold
                               median-window-threshold)
    (let* ((seconds (milliseconds->seconds delay))
           (minutes (seconds->minutes (* count seconds)))
           (message (format nil
                           (_ "returned, for header ~a a value that does not that did not matched the regular expression~2%~s~2%")
                             header-name
                             header-value-regexp)))
      (when results-histogram
        (if (eq failure-reasons :total-failure)
            (trigger-message-template-total-failures minutes
                                                     count
                                                     delay
                                                     uri
                                                     message
                                                     results-histogram)

            (trigger-message-template minutes
                                      count
                                      delay
                                      uri
                                      message
                                      (/ count-threshold count)
                                      median-window-threshold
                                      results-histogram))))))

(defun http-trigger-website-errors (uri count delay
                                    &key
                                      (count-threshold          (* count 0.2))
                                      (median-window-threshold 0.5))
  (multiple-value-bind (results-histogram failure-reasons)
      (http-check-failures-error count
                                 delay
                                 uri
                                 :count-threshold
                                 count-threshold
                                 :median-window-threshold
                                 median-window-threshold)
    (let* ((seconds (milliseconds->seconds delay))
           (minutes (seconds->minutes (* count seconds)))
           (message (_ " failed ")))
      (when results-histogram
        (if (eq failure-reasons :total-failure)
            (trigger-message-template-total-failures minutes
                                                     count
                                                     delay
                                                     uri
                                                     message
                                                     results-histogram)
            (trigger-message-template minutes
                                      count
                                      delay
                                      uri
                                      message
                                      (/ count-threshold count)
                                      median-window-threshold
                                      results-histogram))))))

(defun parse-integer-with-junk (string)
  (parse-integer string :junk-allowed t))

(defparameter *task-debugging* nil)

(defun threshold-value-http-test (uri count delay value-threshold
                                  &key
                                    (count-threshold          (* count 0.2))
                                    (median-window-threshold 0.5)
                                    (extract-value-function #'parse-integer-with-junk))
  (labels ((failure-function (&optional (error-count 10))
             (handler-case
                 (let* ((raw-value (http-utils:get-url-content-body uri))
                        (measure   (funcall extract-value-function raw-value)))
                   (> measure value-threshold))
               (condition (e)
                 (if (< error-count 0)
                     (progn
                       (when *task-debugging*
                         (format t
                                 "An error was signaled for this task ~a~%"
                                 e))
                       (make-notification :all
                                          :warning
                                          (format nil
                                                  (_ "Something wrong happened when getting ~a: ~s")
                                                  uri
                                                  e)
                                          ""
                                          swconf:+admin-smtp-destination-group+)
                       nil)
                     (progn
                       (sleep-milliseconds (seconds->milliseconds 10))
                       (failure-function (1- error-count))))))))
    (multiple-value-bind (results-histogram failure-reasons)
        (http-check-failures count
                             delay
                             #'failure-function
                             :count-threshold
                             count-threshold
                             :median-window-threshold
                             median-window-threshold)
      (let* ((seconds (milliseconds->seconds delay))
             (minutes (seconds->minutes (* count seconds)))
             (message (format nil (_ " returned a value bigger than ~a ") value-threshold)))
        (when results-histogram
          (if (eq failure-reasons :total-failure)
              (trigger-message-template-total-failures minutes
                                                       count
                                                       delay
                                                       uri
                                                       message
                                                       results-histogram)
              (trigger-message-template minutes
                                        count
                                        delay
                                        uri
                                        message
                                        (/ count-threshold count)
                                        median-window-threshold
                                        results-histogram)))))))

(a:define-constant +run-all-task-argument+ "ALL" :test #'string=)

(defun run-all-tasks (name)
  (load-all-tasks :initialize-task nil)
  (loop for task in *all-tasks*
        when (or (string= name +run-all-task-argument+)
                 (cl-ppcre:scan name (id task)))
          do
             (handler-case
                 (let ((*task-debugging* t)
                       (encoded-frequency (funcall (trigger-function task)
                                                   +frequency-argument-trigger+)))
                   (format t
                           (_ "testing task: ~a~%frequency: ~a ~@[(~ams)~] ~%dependencies: ~a~%respects grace time ~a~%init returns ~s~%results: ~s~2%")
                           task
                           encoded-frequency
                           (parse-frequency encoded-frequency)
                           (parse-task-dependencies (funcall (trigger-function task)
                                                             +dependencies-argument-trigger+))
                           (funcall (trigger-function task)
                                    +no-grace-time-argument-trigger+)
                           (funcall (trigger-function task)
                                    +init-argument-trigger+)
                           (multiple-value-list (fire-trigger-function task))))
               (error (e) (format t (_ "error ~a ~a~%") task e)))))

(defun sync-time-waiting-queue->db-tasks ()
  (q:map-queue *waiting-queue*
               (lambda (a)
                 (a:when-let ((old-task-dump (%db-find-task *all-tasks* (id a))))
                   (let ((timestamp (milliseconds-unix-epoch->timestamp (last-time-run-at old-task-dump))))
                     (log-message :debug
                                  (format nil
                                          "setting last-time-run at ~a for task ~a"
                                          timestamp
                                          (id a)))
                     (setf (last-time-run-at a)
                           (last-time-run-at old-task-dump)))))))
