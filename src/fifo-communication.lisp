;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :fifo-communication)

(a:define-constant +quit-command+          "quit"                  :test #'string=)

(a:define-constant +reload-command+        "reload"                :test #'string=)

(a:define-constant +forced-quit-command+   "forced-quit"           :test #'string=)

(a:define-constant +waiting-notifications+ "waiting-notifications" :test #'string=)

(a:define-constant +ok+                    "OK"                    :test #'string=)

(a:define-constant +error+                 "ER"                    :test #'string=)

(a:define-constant +gc-command+            :gc                     :test #'eq)

;; grammar
;; COMMAND               := RELOAD
;;                          | QUIT
;;                          | FORCED-QUIT
;;                          | GC
;;                          | WAITING-NOTIFICATIONS
;;                          | DELETE-COMMAND
;;                          | QUERY
;; DELETE-COMMAND        := DELETE SPACES DELETED-ITEM
;; DELETE                := "DELETE"
;; DELETED-ITEM          := "task" SPACES DELETED-ID
;; FORCED-QUIT           := FORCED SPACES QUIT
;; WAITING-NOTIFICATIONS := WAITING SPACES NOTIFICATIONS
;; WAITING               := "waiting"
;; NOTIFICATIONS         := "notifications"
;; GC                    := "collect" BLANKS "garbage"
;; QUIT                  := "quit"
;; QUIT                  := "forced"
;; RELOAD                := "reload"
;; BLANKS                := (BLANK)*
;; BLANK                 := (or #\space #\Newline #\Tab)
;; QUERY                 := SELECT SPACES TABLE-NAME SPACES (? WHERE-CLAUSE)
;; SELECT                := "select"
;; SPACES                := (BLANK)+
;; OPEN-PARENS           := "("
;; CLOSE-PARENS          := ")"
;; WHERE-CLAUSE          := (and "where" SPACES TERM)
;; TERM                  := (or DELAYED-TERM
;;                              AND-WHERE
;;                              OR-WHERE
;;                              NOT-TERM
;;                              COMPARISON)
;; DELAYED-TERM          := (and OPEN-PARENS BLANKS TERM BLANKS CLOSE-PARENS)
;; COMPARISON            := (or LIKE
;;                              =-TERM
;;                              !=-TERM
;;                              <-TERM
;;                              >-TERM
;;                              <=-TERM
;;                              >=-TERM)
;; NOT-TERM              := (and NOT SPACES (or DELAYED-TERM COMPARISON))
;; LIKE                  := (and COLUMNS SPACES "LIKE" SPACES COLUMN-VALUE)
;; =-TERM                := (and COLUMNS SPACES "=" SPACES COLUMN-VALUE)
;; <-TERM                := (and COLUMNS SPACES "<" SPACES COLUMN-VALUE)
;; >-TERM                := (and COLUMNS SPACES ">" SPACES COLUMN-VALUE)
;; <=-TERM               := (and COLUMNS SPACES "<=" SPACES COLUMN-VALUE)
;; >=-TERM               := (and COLUMNS SPACES ">=" SPACES COLUMN-VALUE)
;; !=-TERM               := (and COLUMNS SPACES "!=" SPACES COLUMN-VALUE)
;; AND-WHERE             := (and TERM SPACES "and" SPACES TERM)
;; OR-WHERE              := (and TERM SPACES "or" SPACES TERM)
;; COLUMN-VALUE          := (and #\" (+ (not #\")) #\")
;; TABLE-NAME            := (+ (not BLANK))
;; COLUMNS               := (+ (not BLANK))
;; DELETED-ID            := (+ (not BLANK))
;; NOT                   := "not"
;; UNRESERVED-CHARS      := (not (or BLANK
;;                                   OPEN-PARENS
;;                                   CLOSE-PARENS
;;                                   #\"))
;; COLUMN-VALUE          := (and #\" (+ UNRESERVED-CHARS) #\")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(p:defrule blank (or #\space #\Newline #\Tab)
  (:constant ""))

(p:defrule blanks (* blank)
  (:constant "-"))

(p:defrule quit "quit"
  (:text t))

(p:defrule gc (and "collect"  blanks "garbage")
  (:constant +gc-command+))

(p:defrule reload "reload"
  (:text t))

(p:defrule forced "forced"
  (:text t))

(p:defrule waiting "waiting"
  (:text t))

(p:defrule notifications "notifications"
  (:text t))

(p:defrule forced-quit (and forced spaces quit)
  (:text t))

(p:defrule waiting-notifications (and waiting spaces notifications)
  (:text t))

(p:defrule open-parens "(")

(p:defrule close-parens ")")

;; query

(p:defrule spaces (p:+ blank)
  (:constant nil))

(p:defrule unreserved-chars (not (or blank
                                     open-parens
                                     close-parens
                                     #\")))

(p:defrule table-name (p:+ unreserved-chars)
  (:text t))

(p:defrule columns (p:+ unreserved-chars)
  (:text t))

(p:defrule column-value (and #\" (p:+ unreserved-chars) #\")
  (:function (lambda (a) (string-trim '(#\") (coerce (second a) 'string)))))

(p:defrule comparison (or like
                          =-term
                          !=-term
                          <-term
                          >-term
                          <=-term
                          >=-term))

(p:defrule delayed-term (and open-parens blanks term blanks close-parens)
  (:function (lambda (a) (third a))))

(p:defrule term (or and-where
                    or-where
                    not-term
                    delayed-term
                    comparison))

(p:defrule not-term (and not
                         spaces
                         (or comparison
                             delayed-term))
  (:function (lambda (a)
               (lambda (row) (not (funcall (third a) row))))))

(p:defrule not "not")

(defun gen-comparision-function (predicate parser-list)
  (lambda (row)
    (let* ((column-name         (a:make-keyword (string-upcase (first parser-list))))
           (column-value        (getf row column-name))
           (column-filter-value (fifth parser-list)))
      (if column-value
          (funcall predicate column-filter-value column-value)
          (error 'conditions:column-not-found
                 :row    row
                 :column column-name)))))

(p:defrule like (and columns spaces "like" spaces column-value)
  (:function (lambda (a)
               (gen-comparision-function #'cl-ppcre:scan a))))

(p:defrule =-term (and columns spaces "=" spaces column-value)
  (:function (lambda (a)
               (gen-comparision-function #'string= a))))

(p:defrule <-term (and columns spaces "<" spaces column-value)
  (:function (lambda (a)
               (gen-comparision-function #'string< a))))

(p:defrule >-term (and columns spaces ">" spaces column-value)
  (:function (lambda (a)
               (gen-comparision-function #'string> a))))

(p:defrule <=-term (and columns spaces "<=" spaces column-value)
  (:function (lambda (a)
               (gen-comparision-function #'string<= a))))

(p:defrule >=-term (and columns spaces ">=" spaces column-value)
  (:function (lambda (a)
               (gen-comparision-function #'string>= a))))

(p:defrule !=-term (and columns spaces "!=" spaces column-value)
  (:function (lambda (a)
               (gen-comparision-function #'string/= a))))

(defmacro gen-logical-operator-function (op parser-list)
  `(lambda (row)
     (let ((lhs (first ,parser-list))
           (rhs (fifth ,parser-list)))
       (,op (funcall lhs row)
            (funcall rhs row)))))

(p:defrule and-where (and term spaces "and" spaces term)
  (:function (lambda (a)
               (gen-logical-operator-function and a))))

(p:defrule or-where (and term spaces "or" spaces term)
  (:function (lambda (a)
               (gen-logical-operator-function or a))))

(p:defrule where-clause (and "where" spaces term)
  (:function (lambda (a)
               (lambda (row)
                 (let ((term (third a)))
                   (funcall term row))))))

(p:defrule query (and "select" spaces table-name (p:? (and spaces where-clause)))
  (:function (lambda (a)
               (let* ((table-name     (third  a))
                      (where-list     (fourth a))
                      (where-function (second where-list)))
                 (list :query table-name where-function)))))

(p:defrule delete "delete"
  (:constant :delete))

(p:defrule deleted-id (p:+ (not blank))
  (:text t))

(p:defrule deleted-item (and "task" spaces deleted-id)
  (:function (lambda (a) (list :task (third a)))))

(p:defrule delete-command (and delete spaces deleted-item)
  (:function a:flatten))

(p:defrule command (or quit forced-quit reload delete-command waiting-notifications gc query))

(defun parsed-query-table (parsed-query)
  (second parsed-query))

(defun parsed-query-filter (parsed-query)
  (third parsed-query))

(defun execute-query (table filter)
  (let* ((current-history-table  (s:history->table))
         (cached-history-files   (statistics:all-history-cached-files)))
    (cond
      ((string= table "task-history")
       (let ((results '()))
         (flet ((filter-history-table (history &key (add-header t))
                  (let* ((plist-data (loop for i in history
                                           when (or (null filter)
                                                    (funcall filter i))
                                             collect i))
                         (data       (loop for raw in plist-data
                                           collect
                                           (remove-if #'keywordp raw)))
                         (header     (remove-if-not #'keywordp (first plist-data))))
                    (setf results
                          (append results
                                  (and add-header (list header))
                                  data)))))
           (filter-history-table current-history-table)
           (loop for history-file in cached-history-files do
             (let ((pathname-history (fs:namestring->pathname history-file)))
               (filter-history-table (s:history-data->table (misc:deserialize pathname-history))
                                     :add-header nil)))
           results)))
      ((string= table "memory-used")
       (mapcar #'list (s:memory-used-histogram))))))

(defparameter *output-lock* (bt:make-lock))

(defun input-fifo-path ()
  (uiop:xdg-runtime-dir (text:strcat +program-name+ "-in")))

(defun output-fifo-path ()
  (uiop:xdg-runtime-dir (text:strcat +program-name+ "-out")))

(defun make-fifos ()
  (os-utils:make-fifo (input-fifo-path))
  (os-utils:make-fifo (output-fifo-path)))

(misc:defun-w-lock print-data (stream data)
    *output-lock*
  (format stream data)
  (finish-output stream))

(defun print-line (stream line)
  (print-data stream (format nil "~a~a~%" line #\Return)))

(defun handle-delete-command (stream parsed-command)
  (print-line stream (_ "pausing program…"))
  (pm:program-pause)
  (print-line stream (_ "done."))
  (handler-case
      (let* ((starting-id (third parsed-command))
             (scanner (ignore-errors (cl-ppcre:create-scanner starting-id))))
        (labels ((recursive-delete (id)
                   (let ((graph (core-tests:all-tasks-graph)))
                     (print-line stream (format nil (_ "deleting ~s…") id))
                     (core-tests:db-task-delete id)
                     (loop for task in core-tests::*all-tasks* do
                       (core-tests:remove-task-dependencies task id))
                     (print-line stream (format nil (_ "done")))
                     (let ((depend-on-ids
                             (loop for node-handle
                                     in (graph:all-arcs-pointing-to graph id)
                                   collect
                                   (graph:value (graph:node-handle->node graph node-handle)))))
                       (loop for depend-id in depend-on-ids do
                         (print-line stream
                                     (format nil
                                             (_ "~a is a dependency for ~s")
                                             id
                                             depend-id))
                         (recursive-delete depend-id))))))
          (if scanner
              (progn
                (loop for task in core-tests::*all-tasks*
                      when (cl-ppcre:scan scanner (core-tests:id task))
                        do (recursive-delete (core-tests:id task)))
                (print-line stream +ok+))
              (progn
                (print-line stream (format nil (_ "invalid regular expression ~s") starting-id))
                (print-line stream +error+)))
          (print-line stream (_ "restarting program…"))
          (pm:program-restart :reload-tasks nil)
          (print-line stream (_ "done."))))
    (error (e)
      (print-line stream (format nil (_ "Fatal error! ~s")  e))
      (print-line stream (_ "Close and restart the programs"))
      (print-line stream +error+))))

(defun perform-gc ()
  (sb-ext:gc :full t))

(defun make-communication-thread ()
  (labels ((quit (stream)
             (print-line stream (_ "quitting…"))
             (pm:quit-program :close-callback (lambda () (print-line stream +ok+))))
           (queryp (parsed-line)
             (and (listp parsed-line)
                  (eq :query (first parsed-line))))
           (delete-command-p (parsed-line)
             (and (listp parsed-line)
                  (eq :delete (first parsed-line))))
           (handle-query (stream parsed-line)
             (handler-case
                 (let* ((table-name   (parsed-query-table parsed-line))
                        (table-filter (parsed-query-filter parsed-line))
                        (table        (execute-query table-name
                                                     table-filter)))
                   (print-table stream table)
                   (print-line stream +ok+))
               (error (e)
                 (print-line stream e)
                 (print-line stream +error+)))))
    (bt:make-thread (lambda ()
                      (with-open-file (istream (input-fifo-path)
                                               :direction :io
                                               :if-does-not-exist :error)
                        (with-open-file (ostream (output-fifo-path)
                                                 :direction :io
                                                 :if-does-not-exist :error)
                          (loop for line = (read-line istream nil nil nil)
                                  then (read-line istream nil nil nil)
                                do
                                   (handler-case
                                       (let ((parsed (ignore-errors (p:parse 'command line))))
                                         (cond
                                           ((null parsed)
                                            (print-line ostream
                                                        (format nil
                                                                (_ "Unable to parse ~s")
                                                                line))
                                            (print-line ostream +error+))
                                           ((eq parsed +gc-command+)
                                            (perform-gc)
                                            (print-line ostream +ok+))
                                           ((delete-command-p parsed)
                                            (handle-delete-command ostream parsed))
                                           ((queryp parsed)
                                            (handle-query ostream parsed))
                                           ((string= parsed +quit-command+)
                                            (quit ostream))
                                           ((string= parsed +forced-quit-command+)
                                            (print-line ostream +ok+)
                                            (tsm:destroy-all-threads)
                                            (croatoan:end-screen)
                                            (os-utils:exit-program))
                                           ((string= parsed +reload-command+)
                                            (pm:reload-tasks)
                                            (print-line ostream +ok+))
                                           ((string= parsed +waiting-notifications+)
                                            (print-table ostream (s:waiting-notifications))
                                            (print-line ostream +ok+))))
                                     (error (e)
                                       (print-table ostream (list (text-utils:to-s e)))
                                       (print-line ostream +ok+)))))))
                    :name "fifo comunication thread")))

(a:define-constant +table-field-separator+ #\Tab :test #'char=)

(defun print-table (stream table)
  (loop for row in table do
    (format stream "~{\"~a\"~^,~}~%" row)))
