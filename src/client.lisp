;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :client)

(defparameter *certificate* nil)

(defparameter *key* nil)

(defparameter *host* nil)

(defparameter *port* nil)

(defun setup ()
  (multiple-value-bind (host port certificate-file key-file)
      (swconf:client-parameters)
    (handler-case
        (let ((cert-path (res:get-data-file certificate-file))
              (key-path  (res:get-data-file key-file)))
          (setf *certificate* cert-path
                *key*         key-path
                *host*        host
                *port*        port))
      (error (e)
        (misc:log-message :warning
                          (_ "error configuring client: ~a")
                          e)
        nil))))

(defun send-test-results (test-results)
  (when (and *certificate*
             *key*
             *host*
             *port*
             (integerp *port*)
             (> *port* 0))
    (handler-case
        (let ((ctx (cl+ssl:make-context :verify-mode cl+ssl:+ssl-verify-none+)))
          (cl+ssl:with-global-context (ctx :auto-free-p t)
            (let* ((socket       (ssl-utils:open-tls-socket *host* *port*))
                   (stream       (usocket:socket-stream socket))
                   (ssl-stream   (cl+ssl:make-ssl-client-stream stream
                                                                :certificate *certificate*
                                                                :key         *key*
                                                                :external-format nil
                                                                :unwrap-stream-p t
                                                                :verify          nil
                                                                :hostname        *host*))
                   (serialized (misc:serialize test-results)))
              (unwind-protect
                   (let ((end-communication-line (format nil
                                                         "~%~a~%"
                                                         server:+end-form-char+)))
                     (write-sequence (babel:string-to-octets serialized) ssl-stream)
                     (write-sequence (babel:string-to-octets end-communication-line)
                                     ssl-stream)
                     (finish-output ssl-stream))
                (ssl-utils:close-tls-socket socket)))))
      (error (e)
        (misc:log-message :warning
                          (_ "error sending data to ~a (~a): ~a")
                          *host*
                          *port*
                          e)))))
