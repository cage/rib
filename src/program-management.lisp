;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :program-management)

(defun quit-program (&key (close-callback (identity t)) (unblock-barrier t))
  (tsm:stop-all-threads)
  (tsm:wait-threads-terminate)
  (misc:log-message :info "All threads terminated")
  (notification:dump-notifications-queue)
  (core-tests:sync-time-waiting-queue->db-tasks)
  (core-tests:dump-db-task)
  (when unblock-barrier
    (when (functionp close-callback)
      (funcall close-callback))
    (tsm:unblock-quitting-barrier)))

(defun stop-all-loops ()
  (quit-program :unblock-barrier nil :close-callback nil))

(defun program-pause ()
  (stop-all-loops)
  (synchronized-queue:flush core-tests:*waiting-queue*)
  (synchronized-queue:flush core-tests:*fulfilled-queue*)
  (setf tsm::*stop-threads* nil))

(defun program-restart (&key (reload-tasks t))
  (cli:manage-opts)
  (misc:log-message :info (_ "Reloading tasks."))
  (statistics:restore-history)
  (notification:restore-notifications-queue)
  (misc:log-message :info (_ "Restarting notification-loop."))
  (notification:start-notification-loop)
  (if reload-tasks
      (progn
        (misc:log-message :info (_ "Reloading tasks."))
        (core-tests:load-all-tasks))
      (loop for task in core-tests::*all-tasks* do
        (misc:log-message :info (_ "Readding tasks ~s") (core-tests:id task))
        (synchronized-queue:push-unblock core-tests:*waiting-queue* task)))
  (misc:log-message :info (_ "Starting testing threads pool."))
  (tsm:initialize-testing-pool #'core-tests:make-testing-thread)
  ;; initialize pool expader always after the pool has been initialized
  (tsm:make-pool-expander-loop core-tests:*waiting-queue*)
  (core-tests:make-recycling-thread))

(defun reload-tasks ()
  (program-pause)
  (program-restart))
