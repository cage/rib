;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

;; derived from tinmop © 2022 cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :http-utils)

(defun user-agent ()
  (let ((configured (swconf:http-parameters)))
    (if (text-utils:string-not-empty-p configured)
        configured
        :drakma)))

(defun get-url-stream (url &key (redirect 5) (timeout 20))
  (multiple-value-bind (stream response-code)
      (drakma:http-request url
                           :user-agent          (user-agent)
                           :redirect            redirect
                           :want-stream         t
                           :verify              :required
                           :connection-timeout  timeout
                           :external-format-out :utf8)
    (values stream response-code)))

(defun %get-url-content-body (url &key
                                    (redirect 5)
                                    (additional-headers nil)
                                    (method :get)
                                    (content nil))
  (multiple-value-bind (body response-code headers)
      (drakma:http-request url
                           :user-agent          (user-agent)
                           :content             content
                           :method              method
                           :redirect            redirect
                           :want-stream         nil
                           :verify              :required
                           :additional-headers  additional-headers
                           :external-format-out :utf8)
       (values body response-code headers)))

(defun get-url-content-body (url
                             &key
                               (method             :get)
                               (redirect            5)
                               (timeout            20)
                               (additional-headers nil)
                               (content            nil))
  (if (and (numberp timeout)
           (> timeout 0))
      (handler-case
          (misc:with-timeout (timeout)
            (multiple-value-bind (body response-code headers)
                (%get-url-content-body url
                                       :content  content
                                       :method   method
                                       :redirect redirect
                                       :additional-headers  additional-headers)
              (values body response-code headers)))
        (sb-sys:deadline-timeout () nil))
      (multiple-value-bind (body response-code headers)
          (%get-url-content-body url
                                 :content  content
                                 :method   method
                                 :redirect redirect
                                 :additional-headers  additional-headers)
        (values body response-code headers))))

(defun get-url-code (url &key (redirect 5))
  (nth-value 1 (get-url-content-body url :redirect redirect)))

(defun get-url-headers (url &key (redirect 5))
  (nth-value 2 (get-url-content-body url :redirect redirect)))

(defun get-url-header (url header-name &key (redirect 5))
  (let ((headers (get-url-headers url :redirect redirect)))
    (cdr (assoc header-name headers))))
