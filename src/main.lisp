;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

;; derived from:

;; tinmop: an humble gemini and pleroma client
;; Copyright (C) 2022  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :main)

(defun read-tui-line (screen)
  (let ((data-input ""))
    (loop for ch = (read-char screen)
          while (not (equal ch #\Newline))
          do
             (setf data-input (text-utils:strcat data-input (text-utils:to-s ch))))
    data-input))

(defun encrypt-loop (password scr)
  (croatoan:clear scr :redraw t)
  (tagbody loop
     (croatoan:clear scr :redraw t)
     (croatoan:move scr 0 0)
     (princ (_ "Insert the data to be encrypted then press enter:") scr)
     (croatoan:refresh scr)
     (let* ((data-clear (read-tui-line scr))
            (data (cryptos:encrypt data-clear password :iv (swconf:initialization-vector))))
       (croatoan:move scr 1 0)
       (princ (format nil "\"~a\"" (cryptos:to-base64 data)) scr)
       (croatoan:refresh scr)
       (croatoan:move scr 2 0)
       (princ (_ "Continue or clear the screen and quit (y/N)?") scr)
       (croatoan:refresh scr)
       (when (char= (read-char scr) (a:first-elt (_ "y")))
         (go loop)))))

(defun pad-password (pass)
  (let* ((padding        (make-string 16 :initial-element #\#))
         (length-pass    (length pass))
         (padding-length (- 32 length-pass)))
    (text-utils:strcat pass (subseq padding 0 padding-length))))

(defun encrypt-data ()
  (croatoan:with-screen (scr :input-echoing nil :input-blocking t)
    (croatoan:clear scr)
    (princ (_ "Type the password: ") scr)
    (croatoan:refresh scr)
    (let ((pass1 (read-tui-line scr)))
      (croatoan:clear scr :redraw t)
      (croatoan:refresh scr)
      (princ (_ "Type the password again to confirm: ") scr)
      (croatoan:refresh scr)
      (let ((pass2 (read-tui-line scr)))
        (cond
          ((string/= pass1 pass2)
           (croatoan:end-screen)
           (format t (_ "The passwords do not match.~%")))
          ((or (< (length pass1) 16)
               (> (length pass1) 32))
           (croatoan:end-screen)
           (format t
                   (_ "The password must be at least 16 characters long and maximum 32.~%")))
          (t
           (encrypt-loop (babel:string-to-octets (pad-password pass1)) scr)))))))

(defun input-master-password ()
  (croatoan:with-screen (scr :input-echoing nil :input-blocking t)
    (croatoan:clear scr)
    (princ (_ "Type the password (between 16 and 32 characters long): ") scr)
    (croatoan:refresh scr)
    (let ((pass (read-tui-line scr)))
      (if (<= 16 (length pass) 32)
          (progn
            (setf swconf:*master-password* (pad-password pass))
            t)
          (progn
            (croatoan:end-screen)
            (format t (_ "The password must be at least 16 characters long.~%"))
            nil)))))

(defun init-i18n ()
  "Initialize i18n machinery"
  (handler-bind ((error
                  (lambda (e)
                    (declare (ignore e))
                    (invoke-restart 'cl-i18n:return-empty-translation-table))))
    (setf cl-i18n:*translation-file-root* +catalog-dir+)
    (cl-i18n:load-language +text-domain+ :locale (cl-i18n:find-locale))))

(defun main ()
  (init-i18n)
  (cli:manage-opts)
  (fifo:make-fifos)
  (cond
    ((cli:run-tasks-p)
     (core-tests:run-all-tasks cli:*run-tasks*))
    (cli:*start-encrypt-loop*
     (encrypt-data))
    (t
     (when (or cli:*skip-asking-master-password*
               (input-master-password))
       (if cli:*send-test-mail*
           (progn
             (notification-channels:send-test-mail))
           (progn
             (misc:start-logging)
             (misc:log-message :info "Program fifo in ~a ." (fifo:input-fifo-path))
             (when (swconf:debug-mode-p)
               (misc:log-message :debug "Running in debug mode."))
             (fs:make-directory (os-utils:user-cache-dir))
             (misc:log-message :info (_ "Starting program."))
             (statistics:restore-history)
             (notification:restore-notifications-queue)
             (misc:log-message :info (_ "Loading-init-file."))
             (load (swconf::init-file-path))
             (misc:log-message :info (_ "Starting notification-loop."))
             (notification:start-notification-loop)
             (misc:log-message :info (_ "Loading tasks."))
             (core-tests:load-all-tasks)
             (misc:log-message :info (_ "Starting testing threads pool."))
             (tsm:initialize-testing-pool #'core-tests:make-testing-thread)
             ;; initialize pool expader always after the pool has been initialized
             (tsm:make-pool-expander-loop core-tests:*waiting-queue*)
             (core-tests:make-recycling-thread)
             (fifo:make-communication-thread)
             (when (not command-line:*no-tui*)
               (bt:make-thread (lambda () (drawing:init-tui)) :name "drawing thread"))
             (server:start-server)
             (client:setup)
             (tsm:quitting-barrier)
             (misc:log-message :info (_ "Quitting, bye!"))
             (misc:stop-logging)
             (croatoan:end-screen)
             (os-utils:exit-program)))))))
