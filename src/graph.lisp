;; rib: a general scheduler.
;; Copyright (C) 2021 Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free  Software Foundation, either  version 3 of the  License, or
;; (at your option) any later version.

;; This program is distributed in the  hope that it will be useful, but
;; WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
;; MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
;; General Public License for more details.

;; You should  have received a copy  of the GNU General  Public License
;; along      with      this       program.       If      not,      see
;; <http://www.gnu.org/licenses/>.

(in-package :graph)

(a:define-constant +unvisited-color+          :white :test #'eq)

(a:define-constant +partially-visited-color+  :gray  :test #'eq)

(a:define-constant +visited-color+            :black  :test #'eq)

(defclass node ()
  ((value
    :initform nil
    :initarg  :value
    :accessor value)
   (color
    :initform +unvisited-color+
    :initarg  :color
    :accessor color)
   (parent
    :initform nil
    :initarg  :parent
    :accessor parent)))

(defmethod print-object ((object node) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "~s" (value object))))

(defclass graph ()
  ((connection-matrix
    :initform nil
    :initarg :connection-matrix
    :accessor connection-matrix)
   (nodes
    :initform '()
    :initarg :nodes
    :accessor nodes)
   (equals-node-predicate
    :initform #'eq
    :initarg :equals-node-predicate
    :accessor equals-node-predicate)))

(define-condition node-not-found (error)
  ((node
    :initarg :node
    :reader  node)
   (graph
    :initarg graph
    :reader graph))
  (:report (lambda (condition stream)
             (format stream "~a" (node condition))))
  (:documentation "Condition signalled when a graph's node was not found."))

(defun make-connection-matrix (dimension)
  (make-array (list dimension dimension)
              :element-type 'fixnum
              :initial-element 0
              :adjustable nil))

(defmethod initialize-instance :after ((object graph) &key (nodes nil) &allow-other-keys)
  (assert (listp nodes))
  (assert (> (length nodes) 0))
  (with-accessors ((connection-matrix connection-matrix)
                   (nodes             nodes)) object
    (setf connection-matrix (make-connection-matrix (length nodes)))
    (setf nodes (loop for node in nodes collect (make-instance 'node :value node)))
    object))

(defun find-node-position (node graph)
  (with-accessors ((nodes nodes)
                   (equals-node-predicate equals-node-predicate)) graph
    (or (position node nodes :test equals-node-predicate :key #'value)
        (error 'node-not-found :node node :graph graph))))

(defgeneric add-arc (object from to &key weight-from->to weight-to->from))

(defgeneric add-directed-arc (object from to &key weight-from->to))

(defgeneric delete-arc (object from to))

(defgeneric all-arcs (object node))

(defgeneric all-arcs-pointing-to (object node))

(defgeneric dfs-single-connected-component (object visited-node &optional mapping-function))

(defgeneric dfs (object &optional mapping-function))

(defgeneric cyclep (object))

(defgeneric cyclep-single-component (object visited-node &optional path))

(defgeneric node-handle->node (object handle))

(defun reset-colors (graph)
  (loop for node in (nodes graph) do (setf (color node) +unvisited-color+)))

(defun reset-parent (graph)
  (loop for node in (nodes graph) do (setf (parent node) nil)))

(defmethod add-arc ((object graph) from to
                    &key (weight-from->to 1) (weight-to->from weight-from->to))
  (with-accessors ((connection-matrix connection-matrix)
                   (nodes             nodes)) object
    (let ((position-from (find-node-position from object))
          (position-to   (find-node-position to object)))
      (setf (aref connection-matrix position-from position-to)
            weight-from->to)
      (setf (aref connection-matrix position-to position-from)
            weight-to->from)))
  object)

(defmethod add-directed-arc ((object graph) from to &key (weight-from->to 1))
  (add-arc object from to :weight-from->to weight-from->to :weight-to->from 0))

(defmethod delete-arc ((object graph) from to)
  (add-arc object from to :weight-from->to 0))

(defmethod all-arcs ((object graph) node)
  (with-accessors ((connection-matrix connection-matrix)) object
    (let ((node-position (find-node-position node object)))
      (loop for i from 0 below (array-dimension connection-matrix 0)
            when (/= (aref connection-matrix node-position i)
                     0)
            collect i))))

(defmethod all-arcs-pointing-to ((object graph) node)
  (with-accessors ((connection-matrix connection-matrix)) object
    (let ((node-position (find-node-position node object)))
      (loop for i from 0 below (array-dimension connection-matrix 0)
            when (/= (aref connection-matrix i node-position)
                     0)
            collect i))))

(defmethod dfs-single-connected-component ((object graph)
                                           (visited-node node)
                                           &optional (mapping-function #'identity))
  (with-accessors ((nodes nodes)) object
    (with-accessors ((color color)) visited-node
      (when (eq color +unvisited-color+)
        (funcall mapping-function visited-node)
        (setf (color visited-node) +partially-visited-color+)
        (loop for arc in (all-arcs object (value visited-node)) do
          (dfs-single-connected-component object (elt nodes arc) mapping-function))
        (setf (color visited-node) +visited-color+)))))

(defmethod dfs ((object graph) &optional (mapping-function #'identity))
  (reset-colors object)
  (loop for node in (nodes object) when (eq (color node) +unvisited-color+) do
    (dfs-single-connected-component object node mapping-function)))

(defmethod cyclep-single-component ((object graph) (visited-node node)
                                    &optional (path '()))
  (with-accessors ((nodes nodes)) object
    (with-accessors ((color color)
                     (parent parent)) visited-node
      (cond
        ((and (eq color +partially-visited-color+)
              (not (eq visited-node parent)))
         (return-from cyclep-single-component (reverse (push visited-node path))))
        ((eq color +unvisited-color+)
         (setf (color visited-node) +partially-visited-color+)
         (loop for arc in (all-arcs object (value visited-node))
               when (not (eq parent (elt nodes arc)))
               do
           (setf (parent (elt nodes arc)) visited-node)
           (let ((cycle-path (cyclep-single-component object
                                                      (elt nodes arc)
                                                      (push visited-node path))))
             (when cycle-path
               (return-from cyclep-single-component cycle-path))))
         (setf (color visited-node) +visited-color+)))))
  nil)

(defmethod cyclep ((object graph))
  (let ((results (loop for node in (nodes object)
                       collect
                       (progn
                         (reset-colors object)
                         (reset-parent object)
                         (cyclep-single-component object node)))))
    (setf results (remove-duplicates results
                                     :test #'a:set-equal))
    (setf results (misc:remove-if-null results))
    results))

(defmethod node-handle->node ((object graph) (handle integer))
  (elt (nodes object) handle))
