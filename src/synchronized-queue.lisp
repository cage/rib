;; rib: a general scheduler.
;; Copyright (C) 2021 Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free  Software Foundation, either  version 3 of the  License, or
;; (at your option) any later version.

;; This program is distributed in the  hope that it will be useful, but
;; WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
;; MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
;; General Public License for more details.

;; You should  have received a copy  of the GNU General  Public License
;; along      with      this       program.       If      not,      see
;; <http://www.gnu.org/licenses/>.

(in-package :synchronized-queue)

(defclass queue ()
  ((container
    :initform '()
    :initarg  :container
    :accessor container)
   (lock
    :initform (bt:make-lock)
    :initarg  :lock
    :accessor lock)
   (condition-lock
    :initform (bt:make-lock)
    :initarg  :condition-lock
    :accessor condition-lock)
   (condition-variable
    :initform (bt:make-condition-variable)
    :initarg  :condition-variable
    :accessor condition-variable)
   (sorting-predicate
    :initform (lambda (a b) (declare (ignore a b)) nil)
    :initarg  :sorting-predicate
    :accessor sorting-predicate)))

(defun-w-lock push-value (queue a)
    (lock queue)
  (with-accessors ((container         container)
                   (sorting-predicate sorting-predicate)) queue
    (push a container)
    (setf container (sort container sorting-predicate))
    queue))

(defun %pop-value (queue)
  (pop (container queue)))

(defun-w-lock pop-value (queue)
    (lock queue)
  (%pop-value queue))

(defun-w-lock peek-value (queue)
    (lock queue)
  (misc:safe-last-elt (container queue)))

(defun-w-lock emptyp (queue)
    (lock queue)
  (null (container queue)))

(defun-w-lock flush (queue)
    (lock queue)
  (setf (container queue) '()))

(defun-w-lock size (queue)
    (lock queue)
  (length (container queue)))

(defun-w-lock map-queue (queue fn)
    (lock queue)
  (mapcar fn (container queue)))

(defun-w-lock find-value-if (queue fn)
    (lock queue)
  (find-if fn (container queue)))

(defun pop-block (queue)
  (with-lock ((condition-lock queue))
    (loop while (emptyp queue)
          do
          (bt:condition-wait (condition-variable queue) (condition-lock queue)))
    (pop-value queue)))

(defun push-unblock (queue value)
  (with-lock ((condition-lock queue))
    (push-value queue value)
    (bt:condition-notify (condition-variable queue))))
