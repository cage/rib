;; rib: a notification system
;; Copyright (C) 2022  Università degli studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :software-configuration)

(a:define-constant +admin-smtp-destination-group+ "admin" :test #'string=)

(defparameter *configuration* nil)

(defparameter *configuration-file* (filesystem-utils:cat-parent-dir +sys-conf-dir+
                                                                    +starting-init-file+))

(defparameter *debug-mode* nil)

(defparameter *master-password* nil)

(defun decrypt (data)
  (if (text-utils:string-empty-p data)
      nil
      (cryptos:decrypt (misc::base64-decode-string data)
                       *master-password*
                       :iv (initialization-vector))))

(defun debug-mode-p ()
  *debug-mode*)

(defun debug-mode ()
  (setf *debug-mode* t))

(defun load-configuration-file (file)
  (let ((data (fs:slurp-file file)))
    (clop:parse data)))

(defun load-main-configuration-file ()
  (setf *configuration*
        (load-configuration-file *configuration-file*)))

(defun search-alist (alist key)
  (cdr (assoc key alist :test #'string=)))

(defun search-table (key &optional (table *configuration*))
  (find-if (lambda (a) (string= (first a) key))
           table))

(defun smtp-parameters ()
  (a:when-let ((smtp-table (rest (search-table "smtp"))))
    (values (search-alist smtp-table "host")
            (search-alist smtp-table "username")
            (decrypt (search-alist smtp-table "password"))
            (search-alist smtp-table "port")
            (search-alist smtp-table "sender")
            (search-alist smtp-table "destination")
            (search-alist smtp-table "use-ssl"))))

(defun smtp-encryption-type ()
  (a:when-let* ((smtp-table (rest (search-table "smtp")))
                (ssl        (search-alist smtp-table "use-ssl")))
    (when (text-utils:string-not-empty-p ssl)
      (a:make-keyword (string-upcase ssl)))))

(defun destinations (table)
  (a:when-let ((smtp-table (rest (search-table table))))
    (rest (search-table "destinations" smtp-table))))

(defun smtp-destinations ()
  (destinations "smtp"))

(defun initialization-vector ()
  (let ((iv (search-alist *configuration* "IV")))
    iv))

(defun threads-parameters ()
  (a:when-let ((thread-table (rest (search-table "threads"))))
     (values (search-alist thread-table "pool-starting-size")
             (search-alist thread-table "pool-max-size")
             (search-alist thread-table "pool-expanding-threshold")
             (search-alist thread-table "pool-expanding-check-frequency"))))

(defun thread-pool-maximum-size ()
  (second (multiple-value-list (threads-parameters))))

(defun thread-pool-expanding-threshold ()
  (third (multiple-value-list (threads-parameters))))

(defun thread-pool-expanding-check-frequency ()
  (fourth (multiple-value-list (threads-parameters))))

(defun telegram-parameters ()
  (a:when-let ((telegram-table (rest (search-table "telegram"))))
    (values (decrypt (search-alist telegram-table "api-key")))))

(defun telegram-destinations ()
  (destinations "telegram"))

(defun queue-sorting-parameters ()
  (a:when-let ((queue-sorting-table (rest (search-table "queue-sorting-parameters"))))
    (values (misc:parse-frequency (search-alist queue-sorting-table
                                                "max-difference-frequency-tolerance"))
            (misc:parse-frequency (search-alist queue-sorting-table
                                                "tolerance-acceleration-ratio")))))

(defun send-consecutive-notifications-p ()
  (search-alist *configuration* "send-consecutive-notifications"))

(defun treshold-deny-consecutive-notifications ()
  (misc:parse-frequency (search-alist *configuration*
                                      "treshold-deny-consecutive-notifications")))

(defun notify-crash-p ()
  (search-alist *configuration* "notify-crash"))

(defun tui-histogram-height ()
  (a:when-let ((table (rest (search-table "user-interface"))))
    (search-alist table "histogram-height")))

(defun task-table ()
  (rest (search-table "task")))

(defun task-json-table ()
  (rest (search-table "http-json" (task-table))))

(defun task-directory ()
  (let ((res (search-alist (task-table) "task-directory")))
    (when (text-utils:string-not-empty-p res)
      res)))

(defun task-file-ignored-regexp ()
  (let ((re (search-alist (task-table) "task-file-ignored-regexp")))
    (when (text-utils:string-not-empty-p re)
      (ignore-errors
       (cl-ppcre:create-scanner re)))))

(defun task-json-parameters ()
  (a:when-let ((table (task-json-table)))
    (values (search-alist table "url")
            (search-alist table "frequency")
            (search-alist table "delay")
            (search-alist table "count")
            (search-alist table "regexp-key")
            (search-alist table "target-url-key")
            (search-alist table "destination")
            (search-alist table "threshold")
            (search-alist table "median-threshold"))))

(defun database-table ()
  (rest (search-table "database")))

(defun database-history-table ()
  (rest (search-table "history" (database-table))))

(defun database-history-parameters ()
  (a:when-let ((table (database-history-table)))
    (values (search-alist table "chunk-size")
            (search-alist table "size"))))

(defun init-file-path ()
  (a:when-let ((filename (search-alist *configuration* "init-file"))
               (config-dir (fs:parent-dir-path swconf:*configuration-file*)))
    (fs:cat-parent-dir config-dir filename)))

(defun server-table ()
  (rest (search-table "server")))

(defun server-parameters ()
  (a:when-let ((table (server-table)))
    (values (search-alist table "hostname")
            (search-alist table "port")
            (search-alist table "certificate-file")
            (search-alist table "key-file")
            (search-alist table "authorized-clients"))))

(defun client-table ()
  (rest (search-table "client")))

(defun client-parameters ()
  (a:when-let ((table (client-table)))
    (values (search-alist table "hostname")
            (search-alist table "port")
            (search-alist table "certificate-file")
            (search-alist table "key-file"))))

(defun http-parameters ()
  (a:when-let ((smtp-table (rest (search-table "http"))))
    (let ((raw-user-agent (search-alist smtp-table "user-agent")))
      (if (text-utils:string-not-empty-p raw-user-agent)
          (format nil "~a/~a" raw-user-agent +program-version+)
          nil))))

(defun max-debug-log-size ()
  (let ((res (search-alist *configuration* "max-debug-file-size")))
    (if (<= res 0)
        (error "\"max-debug-file-size\" directive is not a positive integer, value ~s"
               res))
        res))
