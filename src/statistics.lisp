;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

;; derived from tinmop © 2022 cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :statistics)

(defparameter *statistics-lock*  (bt:make-lock))

(a:define-constant +memory-history-size+ 1000 :test #'=)

(defparameter *memory-used-histogram* (misc:make-fresh-list +memory-history-size+))

(defparameter *history-file-count* -1)

(defparameter *history-single-chunk-size* 10000)

(defparameter *history-limit*              100)

(a:define-constant +history-files-re+ (strcat +history-filename+ ".([0-9]+)$") :test #'string=)

(defparameter *history* '())

(defclass test-results ()
  ((originating-host
    :initform ""
    :initarg  :originating-host
    :accessor originating-host)
   (task
   :initform nil
   :initarg  :task
   :accessor task)
   (notification
    :initform nil
    :initarg  :notification
    :accessor notification)))

(defmethod ms:class-persistent-slots ((object test-results))
  '(originating-host task notification))

(defun history-item-triggered-p (item)
  (notification item))

(defun history-buffer-size-limit-reached-p ()
  (> (length *history*) *history-single-chunk-size*))

(defun max-history-file-number ()
  *history-limit*)

(defun current-history-file-path ()
  (setf *history-file-count*
        (rem (1+ *history-file-count*)
             (max-history-file-number)))
  (os-utils:cached-file-path (format nil "~a.~a" +history-filename+ *history-file-count*)))

(defun next-history-file-path ()
  (setf *history-file-count*
        (rem (1+ *history-file-count*)
             (max-history-file-number)))
  (os-utils:cached-file-path (format nil "~a.~a" +history-filename+ *history-file-count*)))

(defun dump-history (history-data history-file-path)
  (with-open-file (stream
                   history-file-path
                   :direction          :output
                   :if-exists          :supersede
                   :if-does-not-exist  :create)
    (misc:serialize-to-stream history-data stream)))

(defun-w-lock add-history-item (item)
    *statistics-lock*
  (push item *history*)
  (misc:log-message :debug "Update history, size is ~a" (length *history*))
  (when (history-buffer-size-limit-reached-p)
    (let ((new-file (next-history-file-path)))
      (dump-history *history* new-file)
      (setf *history* '())))
  *history*)

(defun-w-lock map-history (function &rest args)
    *statistics-lock*
  (mapcar (lambda (a) (apply function a args))
          *history*))

(defun restore-history ()
  (when (fs:file-exists-p (latest-history-cached-file))
    (setf *history*
          (misc:deserialize (fs:namestring->pathname (latest-history-cached-file))))
    (log-message :info
                 "Restored history from disk file ~a, length: ~a."
                 (latest-history-cached-file)
                 (length *history*))
    *history*))

(defun-w-lock all-test-names ()
    *statistics-lock*
  (let ((results '()))
    (loop for i in *history* do
      (push (ct:id (task i)) results))
    (sort (remove-duplicates results :test #'string=)
          #'string<)))

(defun history-item-hour (item)
  (local-time:timestamp-hour
   (misc:milliseconds-unix-epoch->timestamp (ct:last-time-run-at (task item)))))

(defun filter-name-clsr (name)
  (lambda (a) (string= name (ct:id (task a)))))

(defun make-histogram (history-items bin-size-minutes
                       &optional (filter-fn (lambda (a) (declare (ignore a)) t)))
  (let ((results (make-fresh-list (/ (* 24 60) bin-size-minutes) 0)))
    (loop for threshold from 0 to (* 24 60) by bin-size-minutes
          for i from 0
          do
             (loop for history-item in history-items do
               (let* ((task          (task history-item))
                      (last-time-run (ct:last-time-run-at task))
                      (last-time-universal
                        (misc:milliseconds-unix-epoch->universal last-time-run))
                      (time    (multiple-value-list (decode-universal-time last-time-universal)))
                      (hours   (misc:time-hour-of time))
                      (minutes (+ (* hours 60)
                                  (misc:time-minutes-of time))))
                 (when (and (funcall filter-fn history-item)
                            (< minutes threshold))
                   (setf history-items (remove history-item history-items))
                   (incf (elt results (1- i)))))))
    results))

(defun-w-lock called-histogram (name)
    *statistics-lock*
  (let ((filtered (remove-if-not (filter-name-clsr name) *history*)))
    (make-histogram filtered 30)))

(defun-w-lock triggered-histogram (name)
    *statistics-lock*
  (let ((filtered (remove-if-not (filter-name-clsr name) *history*)))
    (make-histogram filtered 30 (lambda (item) (history-item-triggered-p item)))))

(defun %count-notifications (level &key (from-date nil) (to-date nil))
  (length (remove-if-not (lambda (a)
                           (a:when-let* ((notification (notification a))
                                         (task         (task a))
                                         (date         (misc:milliseconds-unix-epoch->timestamp
                                                        (ct:last-time-run-at task))))
                             (and (or (null from-date)
                                      (local-time:timestamp>= date from-date))
                                  (or (null to-date)
                                      (local-time:timestamp<= date to-date))
                                  (eq (notification:level notification)
                                      level))))
                         *history*)))

(defun-w-lock count-critical-notifications ()
    *statistics-lock*
  (%count-notifications :crit))

(defun-w-lock count-warning-notifications ()
    *statistics-lock*
  (%count-notifications :warning))

(defun-w-lock count-notifications (level &key (from-date nil) (to-date nil))
    *statistics-lock*
  (%count-notifications level :from-date from-date :to-date to-date))

(defun-w-lock waiting-notifications ()
    *statistics-lock*
  (let ((results '()))
    (q:map-queue notification:*waiting-queue*
                 (lambda (a)
                   (with-accessors ((level    notification:level)
                                    (metadata notification:level)
                                    (payload  notification:payload)) a
                       (push (list level metadata payload)
                             results))))
    results))

(defun lisp-boolean->column (a)
  (if a "true" "false"))

(defun history-data->table (history-data)
  (loop for item in history-data collect
    (with-accessors ((notification notification)
                     (task         task)) item
      (let ((level    (and notification (notification:level notification)))
            (metadata (and notification (notification:level notification)))
            (payload  (and notification (notification:payload notification)))
            (sentp    (and notification (notification:send-to-destination-p notification))))
        (with-accessors ((id               ct:id)
                         (frequency        ct:frequency)
                         (last-time-run-at ct:last-time-run-at)) task
          (list :triggered             (lisp-boolean->column (history-item-triggered-p item))
                :sent-to-destination   (lisp-boolean->column sentp)
                :task-name             id
                :message               payload
                :frequency             frequency
                :notification-level    level
                :notification-metadata metadata
                :time-run
                (misc:format-time (misc:milliseconds-unix-epoch->timestamp last-time-run-at))))))))

(defun-w-lock history->table ()
    *statistics-lock*
  (history-data->table *history*))

(defun all-history-cached-files ()
  (let* ((scanner (cl-ppcre:create-scanner +history-files-re+))
         (all-files (remove-if-not (lambda (a) (cl-ppcre:scan scanner a))
                                   (fs:collect-children (os-utils:user-cache-dir)))))
    (sort all-files
          (lambda (a b)
            (multiple-value-bind (x-a count-a)
                (cl-ppcre:scan-to-strings scanner a)
              (declare (ignore x-a))
            (multiple-value-bind (x-b count-b)
                (cl-ppcre:scan-to-strings scanner b)
              (declare (ignore x-b))
              (< (parse-integer (elt count-a 0))
                 (parse-integer (elt count-b 0)))))))))

(defun latest-history-cached-file ()
  (a:when-let ((all-files (all-history-cached-files)))
    (a:last-elt all-files)))

(defun-w-lock update-memory-used ()
    *statistics-lock*
  (let ((rest (misc:all-but-last-elt *memory-used-histogram*)))
    (setf *memory-used-histogram*
          (append (list (os-utils:memory-used))
                  rest))
    *memory-used-histogram*))

(defun-w-lock memory-used-histogram ()
    *statistics-lock*
  *memory-used-histogram*)
