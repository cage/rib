;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

;; derived from:

;; tinmop: an humble gemini and pleroma client
;; Copyright (C) 2022  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :command-line)

(defun print-version ()
  (format t (_ "~a version ~a~%") +program-name+ +program-version+))

(defmacro gen-opts ()
  `(opts:define-opts
     (:name :help
      :description               (_ "print help and exit")
      :short                     #\h
      :long                      "help")
     (:name :version
      :description               (_ "print program information and exit")
      :short                     #\v
      :long                      "version")
     (:name :debug-mode
      :description               (_ "enter debug mode.")
      :short                     #\D
      :long                      "debug-mode")
     (:name :configuration-file
      :description               (_ "use the configuration file provided as CONF-PATH")
      :short                     #\c
      :arg-parser                #'identity
      :meta-var                  (_ "CONF-PATH")
      :long                      "configuration-file")
     (:name :encrypt-data
      :description               (_ "start utility to generate encrypt data suitable for the configuration file")
      :short                     #\E
      :long                      "encryption-utility-mode")
     (:name :generate-iv
      :description               (_ "print an initialization vector suitable for the configuration file and exit")
      :short                     #\V
      :long                      "IV")
     (:name :skip-asking-master-password
      :description               (_ "skip prompt for master password")
      :short                     #\S
      :long                      "skip-asking-master-password")
     (:name :run-tasks
      :description               (_ "run tasks matching a regular expression and print results on standard output, passing \"ALL\" runs all the tasks")
      :short                     #\T
      :arg-parser                #'identity
      :meta-var                  (_ "TASK-NAME")
      :long                      "run-tasks")
     (:name :task-directory
      :description               (_ "load task from an alternative directory")
      :short                     #\d
      :arg-parser                #'identity
      :meta-var                  (_ "DIRECTORY-NAME")
      :long                      "task-directory")
     (:name :no-tui
      :description               (_ "disable terminal user interface")
      :short                     #\N
      :long                      "no-tui")
     (:name :send-test-mail
      :description               (_ "send a test mail message notification to the admin group and exit")
      :long                      "send-test-mail")))

(defun exit-on-error (e)
  (format *error-output* "~a~%" e)
  (os-utils:exit-program 1))

(defmacro set-option-variable (options option-name option-variable)
  (alexandria:with-gensyms (option-value)
    `(let ((,option-value (getf ,options ,option-name)))
       (when ,option-value
         (setf ,option-variable ,option-value)))))

(defparameter *start-encrypt-loop* nil)

(defparameter *skip-asking-master-password* nil)

(defparameter *run-tasks* nil)

(defparameter *no-tui* nil)

(defparameter *send-test-mail* nil)

(defun run-tasks-p ()
  *run-tasks*)

(defun configure-task-directory ()
  (let ((conf (swconf:task-directory)))
    (when (text-utils:string-not-empty-p conf)
      (setf core-tests:*task-directory* conf)))
  (when (not (fs:dirp core-tests:*task-directory*))
    (error (format nil
                   (_ "Task directory ~s not found")
                   core-tests:*task-directory*))))

(defun configure-history-database ()
  (multiple-value-bind (chunk-size chunks-number)
      (swconf:database-history-parameters)
    (setf statistics::*history-single-chunk-size* chunk-size
          statistics::*history-limit*             chunks-number)))

(defun generate-iv ()
  (misc:base64-encode (ironclad:make-random-salt)))

(defun manage-opts ()
  (handler-bind ((opts:unknown-option          #'exit-on-error)
                 (opts:missing-arg             #'exit-on-error)
                 (opts:missing-required-option #'exit-on-error))
    (gen-opts)
    (let ((options (opts:get-opts)))
      (misc:log-message :debug "Command line options ~s" options)
      (when (getf options :generate-iv)
        (format t "\"~a\"~%" (generate-iv))
        (os-utils:exit-program))
      (when (getf options :help)
        (print-version)
        (opts:describe :usage-of                +program-name+
                       :usage-of-label          (_ "Usage")
                       :available-options-label (_ "Available options"))
        (os-utils:exit-program))
      (when (getf options :version)
        (print-version)
        (os-utils:exit-program))
      (set-option-variable options :configuration-file swconf:*configuration-file*)
      (when (not (fs:file-exists-p swconf:*configuration-file*))
        (error (format nil
                       (_ "Configuration file ~s not found")
                       swconf:*configuration-file*)))
      (swconf:load-main-configuration-file)
      (set-option-variable options :debug-mode         swconf:*debug-mode*)
      (set-option-variable options :encrypt-data       *start-encrypt-loop*)
      (set-option-variable options :skip-asking-master-password *skip-asking-master-password*)
      (set-option-variable options :run-tasks *run-tasks*)
      (set-option-variable options :task-directory core-tests:*task-directory*)
      (set-option-variable options :no-tui *no-tui*)
      (set-option-variable options :send-test-mail *send-test-mail*)
      (configure-task-directory)
      (configure-history-database))))
