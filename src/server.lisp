;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :server)

(defparameter *stop-server* nil)

(defstruct client-info
  (name)
  (hash))

(defparameter *authorized-clients-directory* nil)

(defun setup-clients (certs-directory)
  (setf *authorized-clients-directory* certs-directory))

(defun setup-server ()
  (multiple-value-bind (host port certificate-file key-file authorized-client-directory)
      (swconf:server-parameters)
    (handler-case
        (progn
          (setup-clients (res:get-data-dir authorized-client-directory))
          (values host
                  port
                  (res:get-data-file certificate-file)
                  (res:get-data-file key-file)))
      (error (e)
        (setf *stop-server* t)
        (misc:log-message :warning
                          (_ "error configuring server: ~a")
                          e)
        nil))))

(defun open-server (host port cert key)
  (when (not *stop-server*)
    (misc:log-message :info (_ "Starting server on ~a, port ~a") host port)
    (let ((server (usocket:socket-listen host port)))
      (unwind-protect
           (loop while (not *stop-server*) do
             (let* ((client-socket (usocket:socket-accept server)))
               (bt:make-thread (server-client-communication-thread client-socket cert key)
                               :name "server client comunication thread")))
        (ssl-utils:close-tls-socket server)))))

(defun start-server ()
  (setf *stop-server* nil)
  (multiple-value-bind (host port certificate key)
      (setup-server)
    (bt:make-thread (lambda () (open-server host port certificate key))
                    :name "server thread")))

(a:define-constant +end-form-char+ "#" :test #'string=)

(defun read-form (stream)
  (let ((res ""))
    (loop for
          line = (misc:read-line-into-array stream :add-newline-stopper nil)
            then (misc:read-line-into-array stream :add-newline-stopper nil)
          while line
          do
             (let ((string-line (babel:octets-to-string line)))
               (if (string= string-line +end-form-char+)
                   (return-from read-form res)
                   (setf res (text:strcat res string-line)))))
    res))

(defgeneric handle-deserialized-object (object &key &allow-other-keys))

(defmethod handle-deserialized-object ((object statistics:test-results)
                                       &key (hostname nil) &allow-other-keys)
  (with-accessors ((notification statistics:notification)) object
    (setf (statistics:originating-host object) hostname)
    (when notification
      (core-tests:run-notification-hooks (statistics:task object)
                                         (notification:notification->channel-name notification)
                                         (notification:level notification)
                                         (notification:payload notification)
                                         (notification:metadata notification)
                                         (notification:destination notification)
                                         (notification:send-to-destination-p notification))
      (when (notification:send-to-destination-p notification)
        (notification:notify (statistics:notification object))))
    (statistics:add-history-item object)))

(defmethod handle-deserialized-object ((object t) &key &allow-other-keys)
  (misc:log-message :warning (_ "returned unknown type from client ~a") object)
  (when hooks:*server-deserialized-unknown-object-hook*
    (hooks:run-hook hooks:*server-deserialized-unknown-object-hook*
                    object)))

(defun server-client-communication-thread (client-socket cert key)
  (lambda ()
    (unwind-protect
         (let ((ctx (cl+ssl:make-context :verify-mode     cl+ssl:+ssl-verify-peer+
                                         :verify-location *authorized-clients-directory*)))
           (cl+ssl:with-global-context (ctx :auto-free-p t)
             (handler-case
                 (let* ((hostname        (usocket:get-peer-name client-socket))
                        (client-stream   (usocket:socket-stream client-socket))
                        (tls-stream      (cl+ssl:make-ssl-server-stream client-stream
                                                                        :external-format nil
                                                                        :certificate     cert
                                                                        :key             key))
                        (client-hash     (ssl-utils:certificate-fingerprint tls-stream))
                        (serialized-form (read-form tls-stream))
                        (form            (ignore-errors
                                          (misc:deserialize serialized-form))))
                   (ssl-utils:close-tls-socket client-socket)
                   (misc:log-message :debug (_ "server received: ~s") serialized-form)
                   (if (null form)
                       (misc:log-message :crit
                                         (_ "the client ~a (certificate ~a) returned an invalid payload: ~s")
                                         hostname
                                         client-hash
                                         serialized-form)
                       (handle-deserialized-object form :hostname hostname)))
               (error (e)
                 (misc:log-message :warning
                                   (_ "error getting data from client: ~a")
                                   e)))))
      (ssl-utils:close-tls-socket client-socket))))
