;; rib: a general scheduler.
;; Copyright (C) 2021 Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free  Software Foundation, either  version 3 of the  License, or
;; (at your option) any later version.

;; This program is distributed in the  hope that it will be useful, but
;; WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
;; MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
;; General Public License for more details.

;; You should  have received a copy  of the GNU General  Public License
;; along      with      this       program.       If      not,      see
;; <http://www.gnu.org/licenses/>.

(defpackage :config
  (:use :cl)
  (:export
   :+sys-data-dir+
   :+sys-conf-dir+
   :+catalog-dir+
   :+text-domain+
   :+program-name+
   :+program-version+
   :+issue-tracker+
   :+openssl-bin+
   :+xdg-open-bin+
   :+unzip-bin+
   :+man-bin+
   :_
   :n_))

(defpackage :constants
  (:use :cl
   :alexandria
        :config)
  (:export
   :+http-code-ok+
   :+mime-type-jpg+
   :+mime-type-png+
   :+mime-type-html+
   :+starting-init-file+
   :+history-filename+))

(defpackage :conditions
  (:use :cl
   :config)
  (:export
   :text-error
   :text
   :not-implemented-error
   :null-reference
   :out-of-bounds
   :length-error
   :different-length-error
   :command-not-found
   :column-not-found
   :with-default-on-error))

(defpackage :hooks
  (:use
   :cl
   :alexandria)
  (:export
   :*hook*
   :add-hook
   :remove-hook
   :run-hooks
   :run-hook
   :run-hook-compose
   :run-hook-until-failure
   :run-hook-until-success
   :*filter-sending-notification-hook*
   :*before-recycling-hook*
   :*channel-notification-hook*
   :*server-deserialized-unknown-object-hook*))

(defpackage :misc-utils
  (:use :cl
   :constants)
  (:nicknames :misc)
  (:local-nicknames (:a   :alexandria))
  (:export
   :delete@
   :return-whole
   :safe-delete@
   :safe-all-but-last-elt
   :remove-if-null
   :remove-if-not-null
   :not-null-p
   :make-fresh-list
   :seq->list
   :lcat
   :vcat
   :make-array-frame
   :make-fresh-array
   :sequence->list
   :vector-empty-p
   :sequence-empty-p
   :defalias
   :read-line-into-array
   :safe-elt
   :safe-last-elt
   :safe-subseq
   :all-but-last-elt
   :list->array
   :list->simple-array
   :copy-list-into-array
   :format-fn-symbol
   :milliseconds-from-unix-epoch
   :milliseconds-unix-epoch->timestamp
   :milliseconds-unix-epoch->universal
   :time-unix->universal
   :time-second-of
   :time-minutes-of
   :time-hour-of
   :time-date-of
   :time-month-of
   :time-year-of
   :time-day-of
   :time-daylight-p-of
   :time-zone-of
   :year->timestamp
   :current-year
   :extract-year-from-timestamp
   :universal->timestamp
   :timestamp->universal
   :format-time
   :parse-frequency
   :microseconds->milliseconds
   :seconds->milliseconds
   :seconds->minutes
   :seconds->hours
   :seconds->days
   :hours->seconds
   :hours->milliseconds
   :milliseconds->seconds
   :milliseconds->microseconds
   :milliseconds->minutes
   :milliseconds->hours
   :milliseconds->days
   :days->hour
   :days->milliseconds
   :minutes->seconds
   :minutes->milliseconds
   :defun-w-lock
   :with-lock
   :send-email
   :backtrace
   :start-logging
   :stop-logging
   :log-message
   :serialize
   :serialize-to-stream
   :deserialize
   :clone
   :clone-into
   :copy-flat
   :copy-flat-into
   :with-simple-clone
   :with-simple-copy-flat
   :file-shell-script-p
   :file-elf-p
   :with-timeout
   :with-epsilon
   :add-epsilon-rel
   :epsilon<=
   :epsilon>=
   :epsilon=
   :enzyme-kinetics
   :sleep-milliseconds
   :base64-encode
   :base64-decode-octets
   :base64-decode-strings))

(defpackage :filesystem-utils
  (:use
   :cl
   :alexandria)
  (:nicknames :fs)
  (:local-nicknames (:nix :sb-posix))
  (:export
   :+file-path-regex+
   :+s-irwxu+
   :+s-irusr+
   :+s-iwusr+
   :+s-ixusr+
   :+s-irwxg+
   :+s-irgrp+
   :+s-iwgrp+
   :+s-ixgrp+
   :+s-irwxo+
   :+s-iroth+
   :+s-iwoth+
   :+s-ixoth+
   :+s-isuid+
   :+s-isgid+
   :*directory-sep-regexp*
   :*directory-sep*
   :create-a-file
   :copy-a-file
   :rename-a-file
   :file-size
   :slurp-file
   :dump-sequence-to-file
   :create-file
   :cat-parent-dir
   :has-extension
   :get-extension
   :extension-dir-p
   :strip-extension
   :add-extension
   :do-directory
   :collect-children
   :backreference-dir-p
   :loopback-reference-dir-p
   :path-referencing-dir-p
   :collect-files/dirs
   :prepend-pwd
   :search-matching-file
   :regular-file-p
   :temporary-file
   :with-anaphoric-temp-file
   :dirp
   :split-path-elements
   :path-last-element
   :path-first-element
   :path-to-hidden-file-p
   :parent-dir-path
   :append-file-to-path
   :strip-dirs-from-path
   :get-stat-mtime
   :get-stat-ctime
   :get-stat-atime
   :file-outdated-p
   :path-truename
   :file-exists-p
   :directory-exists-p
   :file-length-if-exists
   :delete-file-if-exists
   :home-dir
   :file-can-write-p
   :set-file-permissions
   :directory-files
   :make-directory
   :package-path
   :pathname->namestring
   :namestring->pathname
   :read-single-form
   :octects->units
   :octects->units-string))

(defpackage :text-utils
  (:use
   :cl
   :config)
  (:local-nicknames (:a :alexandria))
  (:export
   :*blanks*
   :to-s
   :strcat
   :strcat*
   :join-with-strings
   :join-with-strings*
   :split-words
   :split-lines
   :strip-prefix
   :strip-withespaces
   :left-padding
   :left-padding-prefix
   :ellipsize
   :string-empty-p
   :string-not-empty-p
   :string-starts-with-p
   :string-ends-with-p
   :trim-blanks
   :percent-encode
   :percent-decode
   :percent-encoded-p
   :maybe-percent-encode
   :histogram-plot
   :integer->subscript))

(defpackage :graph
  (:use
   :cl
   :config
   :constants)
  (:local-nicknames (:a    :alexandria)
                    (:text :text-utils)
                    (:m    :misc-utils))
  (:export
   :node-not-found
   :node
   :value
   :graph
   :equals-node-predicate
   :add-arc
   :add-directed-arc
   :delete-arc
   :all-arcs
   :all-arcs-pointing-to
   :dfs
   :cyclep
   :node-handle->node))

(defpackage :resources-utils
  (:use
   :cl
   :cl-ppcre
   :config
   :constants
   :text-utils)
  (:nicknames :res)
  (:local-nicknames (:a    :alexandria)
                    (:text :text-utils)
                    (:fs   :filesystem-utils)
                    (:m    :misc-utils))
  (:export
   :init
   :home-datadir
   :home-confdir
   :return-home-filename
   :return-system-filename
   :create-empty-in-home
   :get-config-file
   :get-sys-config-file
   :get-data-file
   :get-data-dir))

(defpackage :modules
  (:use
   :cl
   :config
   :constants
   :text-utils
   :resources-utils)
  (:local-nicknames (:a   :alexandria))
  (:shadowing-import-from :resources-utils :init)
  (:export
   :load-sys-module
   :load-module))

(defpackage :os-utils
  (:use
   :cl
   :config
   :constants)
  (:local-nicknames (:a   :alexandria)
                    (:nix :sb-posix))
  (:export
   :getenv
   :default-temp-dir
   :pwd
   :input-fifo-path
   :make-fifo
   :run-external-program
   :slurp-external-program
   :slurp-octets-external-program
   :process-exit-code
   :process-exit-success-p
   :exit-program
   :user-cache-dir
   :cached-file-path
   :memory-used
   :open-ssh-stream))

(defpackage :ssl-utils
  (:use
   :cl
   :config
   :constants)
  (:export
   :open-tls-socket
   :close-tls-socket
   :fetch-certificate
   :certificate-expiry-date
   :fetch-certificate-expiry-date
   :certificate-fingerprint
   :decode-fingerprint))

(defpackage :http-utils
  (:use
   :cl
   :config
   :constants)
  (:export
   :get-url-stream
   :get-url-content-body
   :get-url-code
   :get-url-headers
   :get-url-header))

(defpackage :synchronized-queue
  (:use
   :cl
   :config
   :constants
   :misc)
  (:local-nicknames (:a  :alexandria)
                    (:bt :bordeaux-threads))
  (:export
   :queue
   :container
   :sorting-predicate
   :push-value
   :peek-value
   :pop-value
   :emptyp
   :flush
   :size
   :map-queue
   :find-value-if
   :pop-block
   :push-unblock))

(defpackage :software-configuration
  (:use
   :cl
   :config
   :constants)
  (:local-nicknames (:a :alexandria))
  (:nicknames :swconf)
  (:export
   :+admin-smtp-destination-group+
   :*configuration-file*
   :*debug-mode*
   :*configuration*
   :*master-password*
   :decrypt
   :load-configuration-file
   :load-main-configuration-file
   :smtp-parameters
   :smtp-encryption-type
   :smtp-destinations
   :initialization-vector
   :threads-parameters
   :thread-pool-maximum-size
   :thread-pool-expanding-threshold
   :thread-pool-expanding-check-frequency
   :telegram-parameters
   :telegram-destinations
   :queue-sorting-parameters
   :send-consecutive-notifications-p
   :treshold-deny-consecutive-notifications
   :notify-crash-p
   :tui-histogram-height
   :debug-mode
   :debug-mode-p
   :task-directory
   :task-json-parameters
   :task-file-ignored-regexp
   :database-history-parameters
   :init-file-path
   :server-parameters
   :client-parameters
   :http-parameters
   :max-debug-log-size))

(defpackage :threads-management
  (:use
   :cl
   :config
   :constants
   :misc-utils)
  (:local-nicknames (:a  :alexandria)
                    (:bt :bordeaux-threads)
                    (:q  :synchronized-queue))
  (:export
   :*stop-threads*
   :stop-all-threads
   :destroy-all-threads
   :stop-all-threads-p
   :*testing-pool*
   :*recycling-thread*
   :initialize-testing-pool
   :wait-threads-terminate
   :make-pool-expander-loop
   :threading-pool-length
   :*notification-loop*
   :unblock-quitting-barrier
   :quitting-barrier))

(defpackage :notification-channels
  (:use
   :cl
   :config
   :constants)
  (:local-nicknames (:a    :alexandria)
                    (:text :text-utils))
  (:export
   :send-test-mail
   :notify-email
   :notify-telegram))

(defpackage :core-tests
  (:use
   :cl
   :config
   :constants
   :misc
   :text-utils
   :filesystem-utils)
  (:local-nicknames (:a  :alexandria)
                    (:bt :bordeaux-threads)
                    (:q  :synchronized-queue)
                    (:tsm :threads-management))
  (:export
   :*waiting-queue*
   :*fulfilled-queue*
   :*processing-queue*
   :task
   :id
   :dependencies
   :one-shot-p
   :frequency
   :last-time-run-at
   :trigger-function
   :run-notification-hooks
   :remove-task-dependencies
   :make-testing-thread
   :stop-testing-threads
   :destroy-testing-threads
   :make-recycling-thread
   :arguments-asks-frequency-p
   :arguments-asks-dependencies-p
   :arguments-asks-respects-grace-time-p
   :arguments-init-p
   :*loaded-task-function*
   :dump-db-task
   :revive-db-task
   :db-task-delete
   :load-all-tasks
   ;; useful tests
   :hours->seconds
   :minutes->seconds
   :certificate-expiration-days
   :certificate-near-to-expiration-p
   :certificate-expired-p
   :http-expected-code-p
   :http-ok-code-p
   :http-check-body
   :http-check-regex
   :test-many-times
   :collect-tests-results
   :average
   :median
   :test-histogram-failures-count
   :test-histogram-failures
   :http-check-failures-error
   :http-check-failures-re-body
   :http-check-failures-timeout
   :http-check-failures-code
   :http-trigger-website-body
   :http-trigger-website-code
   :http-trigger-website-errors
   :http-trigger-website-timeout
   :http-trigger-website-header
   :threshold-value-http-test
   :*task-directory*
   :run-all-tasks
   :sync-time-waiting-queue->db-tasks
   :tasks->graph
   :all-tasks-graph))

(defpackage :notification
  (:use
   :cl
   :config
   :constants
   :misc-utils)
  (:local-nicknames (:a   :alexandria)
                    (:bt  :bordeaux-threads)
                    (:q   :synchronized-queue)
                    (:tsm :threads-management))
  (:export
   :*waiting-queue*
   :start-loop
   :stop-loop
   :end-loop
   :notification
   :level
   :metadata
   :payload
   :destination
   :send-to-destination-p
   :send-to-destination
   :notification->channel-name
   :notify
   :enqueue
   :extract
   :enqueue
   :notification-noop
   :notification-mail
   :notification-telegram
   :start-notification-loop
   :dump-notifications-queue
   :restore-notifications-queue
   :stop-notification-loop
   :destroy-notification-loop))

(defpackage :statistics
  (:use
   :cl
   :config
   :constants
   :misc-utils
   :text-utils)
  (:local-nicknames (:a  :alexandria)
                    (:bt :bordeaux-threads)
                    (:q  :synchronized-queue)
                    (:n  :notification)
                    (:ct :core-tests))
  (:export
   :test-results
   :originating-host
   :task
   :notification
   :add-history-item
   :map-history
   :restore-history
   :all-test-names
   :called-histogram
   :triggered-histogram
   :count-critical-notifications
   :count-warning-notifications
   :count-notifications
   :waiting-notifications
   :history-data->table
   :history->table
   :all-history-cached-files
   :update-memory-used
   :memory-used-histogram))

(defpackage :tui-utils
  (:use
   :cl
   :alexandria
   :cl-ppcre
   :local-time
   :croatoan
   :config
   :constants
   :text-utils)
  (:nicknames :tui)
  (:import-from :misc-utils :defalias)
  (:shadowing-import-from :text-utils :split-lines)
  (:export
   :make-win-background
   :make-croatoan-window
   :make-blocking-croatoan-window
   :make-screen
   :make-tui-char
   :make-tui-string
   :tui-format
   :decode-key-event
   :text-length
   :find-max-line-width
   :ncat-complex-string
   :to-tui-string
   :cat-complex-string
   :cat-tui-string
   :tui-char->char
   :tui-string->chars-string
   :tui-string-subseq
   :text-ellipsis
   :right-pad-text
   :text->tui-attribute
   :assemble-attributes
   :attribute-reverse
   :attribute-bold
   :attribute-underline
   :attribute-italic
   :attribute-blink
   :attribute-dim
   :attribute-invisible
   :combine-attributes
   :colorize-line
   :colorized-line->tui-string
   :apply-coloring
   :standard-error-notify-life
   :with-notify-errors
   :with-print-error-message
   :make-tui-char
   :make-tui-string
   :tui-string-apply-colors
   :copy-tui-string
   :apply-attributes))

(defpackage :program-management
  (:use
   :cl
   :config
   :constants)
  (:local-nicknames (:a   :alexandria)
                    (:bt  :bordeaux-threads)
                    (:text :text-utils)
                    (:tsm :threads-management)
                    (:s   :statistics)
                    (:p   :esrap))
  (:export
   :quit-program
   :stop-all-loops
   :program-pause
   :program-restart
   :reload-tasks))

(defpackage :fifo-communication
  (:use
   :cl
   :config
   :constants)
  (:local-nicknames (:a   :alexandria)
                    (:bt  :bordeaux-threads)
                    (:text :text-utils)
                    (:tsm :threads-management)
                    (:s   :statistics)
                    (:p   :esrap)
                    (:pm  :program-management))
  (:export
   :+quit-command+
   :input-fifo-path
   :output-fifo-path
   :make-fifos
   :make-communication-thread))

(defpackage :server
  (:use
   :cl
   :config
   :constants)
  (:local-nicknames (:a    :alexandria)
                    (:bt   :bordeaux-threads)
                    (:text :text-utils)
                    (:ssl  :cl+ssl)
                    (:tsm  :threads-management))
  (:export
   :+end-form-char+
   :start-server))

(defpackage :client
  (:use
   :cl
   :config
   :constants)
  (:local-nicknames (:a    :alexandria)
                    (:bt   :bordeaux-threads)
                    (:text :text-utils)
                    (:ssl  :cl+ssl)
                    (:tsm  :threads-management))
  (:export
   :setup
   :send-test-results))

(defpackage :drawing
  (:use
   :cl
   :config
   :constants
   :tui)
  (:local-nicknames (:a    :alexandria)
                    (:tsm  :threads-management)
                    (:text :text-utils)
                    (:q    :synchronized-queue)
                    (:fifo :fifo-communication))
  (:export
   :*main-window*
   :draw-all
   :init-tui))

(defpackage :command-line
  (:use
   :cl
   :config
   :constants)
  (:nicknames :cli)
  (:local-nicknames (:a    :alexandria)
                    (:bt   :bordeaux-threads)
                    (:q    :synchronized-queue)
                    (:tsm  :threads-management)
                    (:fifo :fifo-communication))
  (:export
   :*start-encrypt-loop*
   :*skip-asking-master-password*
   :*run-tasks*
   :*no-tui*
   :*send-test-mail*
   :run-tasks-p
   :manage-opts))

(defpackage :main
  (:use
   :cl
   :config
   :constants)
  (:local-nicknames (:a    :alexandria)
                    (:bt   :bordeaux-threads)
                    (:q    :synchronized-queue)
                    (:tsm  :threads-management)
                    (:fifo :fifo-communication))
  (:export))
