;; rib: a notification system
;; Copyright (C) 2022  Università degli studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :notification-channels)

(a:define-constant +destinations-field-separator-re+ "[ ,;]" :test #'string=)

(a:define-constant +destinations-field-separator+ ","        :test #'string=)

(defun filter-group-by-hooks (groups)
  (mapcar (lambda (group)
            (if hooks:*filter-sending-notification-hook*
                (hooks:run-hook-compose 'hooks:*filter-sending-notification-hook*
                                        group)
                group))
          groups))

(defun collect-mail-addresses (&optional (group-regex ".*"))
  (let ((results         nil)
        (filtered-groups (filter-group-by-hooks (swconf:smtp-destinations))))
    (loop for group in filtered-groups
          when (cl-ppcre:scan group-regex (car group))
            do
               (setf results
                     (text:join-with-strings (list results (cdr group))
                                             +destinations-field-separator+)))
    (remove-if #'text-utils:string-empty-p
               (mapcar #'text-utils:trim-blanks
                       (cl-ppcre:split +destinations-field-separator-re+
                                       results)))))

(defun send-test-mail ()
  (format t (_ "Sending a test mail, any error appearing here is either a bug or wrong configuration of the SMTP parameters in the configuration file.~%"))
  (let ((destination-list (collect-mail-addresses "admin")))
    (assert destination-list)
    (format t (_ "sending mail to: ~a~%") destination-list)
    (multiple-value-bind (host
                          username
                          password
                          port
                          sender)
        (swconf:smtp-parameters)
      (format t
              (_ "smtp parameters: host: ~a username ~a password [not shown] port ~a sender ~a destination ~a ssl? ~a~%")
              host
              username
              port
              sender
              (first destination-list)
              (swconf:smtp-encryption-type))
      (misc:send-email (format nil
                               (_ "test mail from ~a")
                               +program-name+)
                       sender
                       (first destination-list)
                       (format nil
                               (_ "If you can read this message ~a works!")
                               +program-name+)
                       host
                       port
                       username
                       password
                       (swconf:smtp-encryption-type)
                       nil))))

(defun notify-email (subject message destination attachments)
  #+mock-email
  (let* ((group-re         (if (text:string-not-empty-p destination)
                                 destination
                                 ".*"))
         (destination-list (collect-mail-addresses group-re)))
    (misc:log-message :crit
                      "maildest ~a → ~a → [~a] ~a"
                      destination
                      destination-list
                      subject
                      message))
  #-mock-email
  (handler-case
      (multiple-value-bind (host
                        username
                        password
                        port
                        sender)
          (swconf:smtp-parameters)
        (cond
          ((text-utils:string-empty-p host)
           (misc:log-message :crit
                             (_ "SMTP server non specified in configuration file, notification can not be processed!")))
          ((null port)
           (misc:log-message :crit
                             (_ "SMTP server port non specified in configuration file, notification can not be processed!")))
          (t
           (let* ((group-re         (if (text:string-not-empty-p destination)
                                        destination
                                        ".*"))
                  (destination-list (collect-mail-addresses group-re)))
             (loop for actual-destination in destination-list do
               (handler-case
                   (misc:send-email subject
                                    sender
                                    actual-destination
                                    message
                                    host
                                    port
                                    username
                                    password
                                    (swconf:smtp-encryption-type)
                                    attachments)
                 (error (e) (misc-utils:log-message
                             :warning
                             (format nil
                                     "Unable to send email to ~s ~s ~a"
                                     actual-destination
                                     (type-of e)
                                     e)))))))))
    (error (e) (misc-utils:log-message
                :warning
                (format nil
                        "Unable to send email, error: ~s ~a"
                        (type-of e)
                        e)))))

(a:define-constant +telegram-webapi-template+ "https://api.telegram.org/~a/~a?~a"
  :test #'string=)

(defun expand-telegram-template (key method query)
  (format nil +telegram-webapi-template+ key method query))

(defun collect-telegram-chats (&optional (group-regex ".*"))
  (let ((filtered-groups (filter-group-by-hooks (swconf:telegram-destinations))))
    (loop for group in filtered-groups
          when (and (cl-ppcre:scan group-regex (car group))
                    (text-utils:string-not-empty-p (cdr group)))
            collect
            (cdr group))))

(defun notify-telegram (message destinations)
  (loop for channel-id in (collect-telegram-chats destinations) do
    (handler-case
        (multiple-value-bind (api-key channel-id)
            (swconf:telegram-parameters)
          (cond
            ((text-utils:string-empty-p api-key)
             (misc:log-message :warning
                               (_ "Unable to send telegram message because the api key is empty or invalid")))
            ((text-utils:string-empty-p channel-id)
             (misc:log-message :warning
                               (_ "Unable to send telegram message because the channel-id is empty")))
            ((text-utils:string-empty-p message)
             (misc:log-message :warning
                               (_ "Refused to send an empty telegram message")))
            (t
             (let* ((encoded-message (text-utils:maybe-percent-encode message))
                    (encoded-channel (text-utils:maybe-percent-encode channel-id))
                    (query           (format nil "chat_id=~a&text=~a" encoded-channel encoded-message))
                    (uri             (expand-telegram-template api-key
                                                               "sendMessage"
                                                               query)))
               (http-utils:get-url-content-body uri)))))
      (error (e) (misc-utils:log-message
                  :warning
                  (format nil
                          "Unable to send telegram notification error: ~s ~a"
                          (type-of e)
                          e))))))
