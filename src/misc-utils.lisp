;; tinmop: an humble gemini and pleroma client
;; Copyright (C) 2022  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; derived from:

;; niccolo': a chemicals inventory
;; Copyright (C) 2016  Universita' degli Studi di Palermo

;; This  program is  free  software: you  can  redistribute it  and/or
;; modify it  under the  terms of  the GNU  General Public  License as
;; published  by  the  Free  Software Foundation,  version  3  of  the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :misc-utils)

;; macro utils

(defmacro format-fn-symbol (package format &rest format-args)
  `(alexandria:format-symbol ,package ,(concatenate 'string "~:@(" format "~)")
                             ,@format-args))

;; function utils

(defun unsplice (form)
  (and form
       (list form)))

(defmacro defalias (alias &body (def &optional docstring))
  "Define a value as a top-level function.
     (defalias string-gensym (compose #'gensym #'string))
Like (setf (fdefinition ALIAS) DEF), but with a place to put
documentation and some niceties to placate the compiler.
Name from Emacs Lisp."
  `(progn
     ;; Give the function a temporary definition at compile time so
     ;; the compiler doesn't complain about it's being undefined.
     (eval-when (:compile-toplevel)
       (unless (fboundp ',alias)
         (defun ,alias (&rest args)
           (declare (ignore args)))))
     (eval-when (:load-toplevel :execute)
       (compile ',alias ,def)
       ,@(unsplice
          (when docstring
            `(setf (documentation ',alias 'function) ,docstring))))
     ',alias))

;; i/o

(defun read-line-into-array (stream &key (add-newline-stopper t))
  "Read a line as array of unsigned octets or nil if stream is exausted.
if `add-newline-stopper' is  non nil a newline (ASCII  10) is appended
to the array"
  (let ((first-byte (read-byte stream nil nil)))
    (when first-byte
      (let ((raw (loop
                   for c = first-byte then (read-byte stream nil (char-code #\Newline))
                   while (/= c (char-code #\Newline))
                   collect c)))
        (when add-newline-stopper
          (let ((rev (reverse raw)))
            (push (char-code #\Newline) rev)
            (setf raw (reverse rev))))
        (misc:list->array raw '(unsigned-byte 8))))))


;; sequence utils

(defun safe-elt (sequence index)
  (and (>= index 0)
       (< index (length sequence))
       (elt sequence index)))

(defun safe-last-elt (sequence)
  (safe-elt sequence (1- (length sequence))))

(defun safe-subseq (sequence start &optional (end nil))
  (when sequence
    (when (or (null start)
              (< start 0))
      (setf start 0))
    (when (and (numberp end)
               (> end  (length sequence)))
      (setf end (length sequence)))
    (let* ((actual-start (alexandria:clamp start
                                           0
                                           (length sequence)))
           (actual-end   (and end
                              (max actual-start
                                   (min end
                                        (length sequence))))))
      (subseq sequence actual-start actual-end))))

(defgeneric sequence-empty-p (a))

(defmethod sequence-empty-p ((a vector))
  (vector-empty-p a))

(defmethod sequence-empty-p ((a sequence))
  (alexandria:emptyp a))

(defun vector-empty-p (v)
  (declare (optimize (speed 3) (safety 0) (debug 0)))
  (declare (vector v))
  (= (length v) 0))

(defun make-fresh-list (size &optional (el nil))
  (map-into (make-list size)
            (if (functionp el)
                el
                #'(lambda () el))))

(defun seq->list (sequence)
  (if (listp sequence)
      (copy-list sequence)
      (map-into (make-list (length sequence)) #'identity sequence)))

(defmacro *cat (type-return input)
  `(reduce #'(lambda (a b) (concatenate ',type-return a b)) ,input))

(defun lcat (&rest v)
  (declare (optimize (speed 3) (safety 1) (debug 0)))
  (*cat list v))

(defun vcat (&rest v)
  (declare (optimize (speed 3) (safety 1) (debug 0)))
  (*cat vector v))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun make-array-frame (size &optional (el nil) (type t) (simplep nil))
    "All elements points to the same address/reference!"
    (make-array size
                :fill-pointer (if (not simplep) size nil)
                :adjustable (if (not simplep) t nil)
                :initial-element el
                :element-type type)))

(defun make-fresh-array (size &optional (el nil) (type t) (simplep nil))
  (let ((res (make-array size
                         :fill-pointer (if (not simplep) size nil)
                         :adjustable (if (not simplep) t nil)
                         :initial-element el
                         :element-type type)))
    (map-into res #'(lambda (a) (setf a (cond
                                          ((functionp el)
                                           (funcall el))
                                          ((arrayp el)
                                           (alexandria:copy-array el))
                                          ((listp el)
                                           (copy-list el))
                                          (t
                                           el))))
              res)))

(defun list->array (the-list &optional (element-type t))
  (make-array (length the-list)
              :element-type     element-type
              :fill-pointer     (length the-list)
              :adjustable       t
              :initial-contents (copy-list the-list)))

(defun copy-list-into-array (from to)
  (assert (= (length from) (length to)))
  (loop
     for i in from
     for ct from 0 by 1 do
       (setf (elt to ct) i))
  to)

(defun array-slice (array start &optional (end nil))
  (let* ((new-size         (if end
                               (- end start)
                               (length array)))
         (new-fill-pointer (cond
                             ((array-has-fill-pointer-p array)
                              (if end
                                  new-size
                                  (fill-pointer array)))
                             (t
                              nil)))
         (new-array        (make-array new-size
                                       :element-type    (array-element-type array)
                                       :fill-pointer    new-fill-pointer
                                       :initial-element (alexandria:first-elt array)
                                       :adjustable      (adjustable-array-p array)))
         (end-iteration    (or end
                               (length array))))
    (loop
       for index-from from start below end-iteration
       for index-to   from 0
           do
         (setf (elt new-array index-to)
               (elt array     index-from)))
    new-array))

(defun list->simple-array (the-list start-type type)
  (let ((res (make-array-frame (length the-list) start-type type t)))
    (loop
       for element in the-list
       for i from 0 below (length the-list) do
         (setf (elt res i) element))
    res))

(defgeneric delete@ (sequence position))

(defgeneric safe-delete@ (sequence position)
  (:documentation "Return sequence if position is out of bound"))

(defmacro gen-delete@ ((sequence position) &body body)
  `(if (and (>= ,position 0)
            (< ,position (length ,sequence)))
       ,@body
      (error 'conditions:out-of-bounds :seq sequence :idx position)))

(defmethod delete@ ((sequence list) position)
  (gen-delete@
   (sequence position)
   (append (subseq sequence 0 position)
           (and (/= position (- (length sequence) 1))
                (subseq sequence (1+ position))))))

(defmethod delete@ ((sequence vector) position)
  (gen-delete@
   (sequence position)
    (make-array (1- (length sequence))
                :fill-pointer (1- (length sequence))
                :adjustable t
                :initial-contents (concatenate 'vector (subseq sequence 0 position)
                                               (and (/= position (- (length sequence) 1))
                                                    (subseq sequence (1+ position)))))))

(defmethod safe-delete@ ((sequence sequence) position)
  (restart-case
      (delete@ sequence position)
    (return-nil () nil)
    (return-whole () sequence)
    (new-index (i) (safe-delete@ sequence i))))

(defun safe-all-but-last-elt (sequence)
  (handler-bind ((conditions:out-of-bounds
                  #'(lambda (c)
                      (declare (ignore c))
                      (invoke-restart 'return-nil))))
    (safe-delete@ sequence (1- (length sequence)))))

(defun remove-if-null (a)
  (remove-if #'null a))

(defun remove-if-not-null (a)
  (remove-if #'(lambda (i) (not (null i))) a))

(defun all-but-last-elt (s)
  (if s
      (let ((length (length s)))
        (if (> length 0)
            (subseq s 0 (1- length))
            s))
      s))

(cffi:defcstruct timespec
  (seconds   :long)
  (nseconds  :long))

(a:define-constant +system-clock-monotonic+ 1 :test #'=)

(cffi:defcfun ("clock_gettime" ffi-clock-gettime) :int
  (clockid :int)
  (tp :pointer))

(defun clock-get-monotoninc-time ()
  (cffi:with-foreign-object (clock-time '(:struct timespec) 1)
    (let ((errors (ffi-clock-gettime +system-clock-monotonic+
                                     clock-time)))
      (if (/= errors 0)
          (error "unable to get clock time")
          (let ((decoded-clock (cffi:convert-from-foreign clock-time '(:struct timespec))))
            (values (getf decoded-clock 'seconds)
                    (getf decoded-clock 'nseconds)))))))

;;;; partially derived from local-time library

(defun milliseconds-from-unix-epoch ()
  (multiple-value-bind (seconds nanoseconds)
      (clock-get-monotoninc-time)
    (truncate (+ (seconds->milliseconds seconds)
                 (nanoseconds->milliseconds nanoseconds)))))

(a:define-constant +unix-epoch+ (encode-universal-time 0 0 0 1 1 1970 0)
  :test #'=)

(defun time-unix->universal (unix-timestamp)
  "Return the UNIVERSAL-TIME corresponding to the TIMESTAMP"
  ;; universal time is seconds from 1900-01-01T00:00:00Z
  ;; unix timestamp is seconds from 1970-01-01T00:00:00Z
  (+ unix-timestamp +unix-epoch+))

(defmacro gen-time-access (name pos)
  `(defun ,(format-fn-symbol t "time-~a-of" name) (time-list)
     (elt time-list ,pos)))

(defmacro gen-all-time-access (&rest name-pos)
  `(progn
     ,@(loop for i in name-pos collect
            `(gen-time-access ,(car i) ,(cdr i)))))

(gen-all-time-access (seconds    . 0)
                     (minutes    . 1)
                     (hour       . 2)
                     (date       . 3)
                     (month      . 4)
                     (year       . 5)
                     (day        . 6)
                     (daylight-p . 7)
                     (zone       . 8))

(defun year->timestamp (year)
  (local-time:encode-timestamp 0
                               0
                               0
                               0
                               1
                               1
                               (truncate (max 0
                                              (parse-number:parse-number year)))))

(defun current-year ()
  (local-time:timestamp-year (local-time:now)))

(defun extract-year-from-timestamp (ts)
  (local-time:timestamp-year ts))

(defun command-terminated-no-error-p (command-error-code)
  (= command-error-code 0))

(defun format-time (local-time-object
                    &optional (format-control-list local-time:+iso-8601-format+))
  (with-output-to-string (stream)
    (local-time:format-timestring stream local-time-object :format format-control-list)))

(defun milliseconds-unix-epoch->timestamp (milliseconds)
  (universal->timestamp (time-unix->universal (milliseconds->seconds milliseconds))))

(defun milliseconds-unix-epoch->universal (milliseconds)
  (local-time:timestamp-to-universal (milliseconds-unix-epoch->timestamp milliseconds)))

(defun universal->timestamp (universal)
  (local-time:universal-to-timestamp universal))

(defun timestamp->universal (timestamp)
  (local-time:timestamp-to-universal timestamp))

(a:define-constant +frequency-synonyms+
    (list (cons "half-hour" "30m")
          (cons "hourly"    "1h")
          (cons "daily"     "1d")
          (cons "weekly"    "7d")
          (cons "monthly"   "30d")
          (cons "yearly"    "365d"))
  :test #'equalp)

(defun frequency-synonym (freq)
  (or (cdr (assoc freq +frequency-synonyms+ :test #'string-equal))
      freq))

(defun parse-frequency (value)
  (let ((actual-value (frequency-synonym (text-utils:trim-blanks value))))
    (multiple-value-bind (matchedp matches)
        (cl-ppcre:scan-to-strings "([0-9]+) *(ms|[dhms])" actual-value)
      (when matchedp
        (let ((frequency (parse-integer (elt matches 0)))
              (units     (elt matches 1)))
          (cond
            ((string= units "d")
             (days->milliseconds frequency))
            ((string= units "h")
             (hours->milliseconds frequency))
            ((string= units "m")
             (minutes->milliseconds frequency))
            ((string= units "s")
             (seconds->milliseconds frequency))
            ((string= units "ms")
             frequency)))))))

(defun nanoseconds->milliseconds (nanoseconds)
  (truncate (/ nanoseconds 1000000)))

(defun nanoseconds->seconds (nanoseconds)
  (truncate (/ nanoseconds 1000000000)))

(defun microseconds->milliseconds (microseconds)
  (truncate (/ microseconds 1000)))

(defun seconds->milliseconds (seconds)
  (truncate (* 1000 seconds)))

(defun seconds->nanoseconds (seconds)
  (truncate (* 1000000000 seconds)))

(defun seconds->minutes (seconds)
  (truncate (/ seconds 60)))

(defun seconds->hours (seconds)
  (truncate (/ (seconds->minutes seconds) 60)))

(defun seconds->days (seconds)
  (truncate (/ (seconds->hours seconds) 24)))

(defun hours->seconds (hours)
  (truncate (* hours 3600)))

(defun hours->milliseconds (hours)
  (seconds->milliseconds (hours->seconds hours)))

(defun milliseconds->seconds (milliseconds)
  (truncate (/ milliseconds 1000)))

(defun milliseconds->microseconds (milliseconds)
  (truncate (* milliseconds 1000)))

(defun milliseconds->nanoseconds (milliseconds)
  (truncate (* milliseconds 1000000)))

(defun milliseconds->minutes (milliseconds)
  (seconds->minutes (milliseconds->seconds milliseconds)))

(defun milliseconds->hours (milliseconds)
  (seconds->hours (milliseconds->seconds milliseconds)))

(defun milliseconds->days (milliseconds)
  (seconds->days (milliseconds->seconds milliseconds)))

(defun days->hours (days)
  (truncate (* 24 days)))

(defun days->milliseconds (days)
  (seconds->milliseconds (hours->seconds (days->hours days))))

(defun minutes->seconds (minutes)
  (truncate (* minutes 60)))

(defun minutes->milliseconds (minutes)
  (seconds->milliseconds (minutes->seconds minutes)))

;; threads

(defmacro with-lock ((lock) &body body)
  `(bt:with-lock-held (,lock)
     ,@body))

(defmacro defun-w-lock (name parameters lock &body body)
  (multiple-value-bind (remaining-forms declarations doc-string)
      (alexandria:parse-body body :documentation t)
    `(defun ,name ,parameters
       ,doc-string
       ,declarations
       (with-lock (,lock)
         ,@remaining-forms))))

;; mail

(defparameter *base36-alphabet* "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")

(defun encode-base-36 (number &optional (accum '()))
  (if (<= number 0)
      (coerce (reverse accum) 'string)
      (let ((new-number (truncate (/ number 36)))
            (remainder  (rem number 36)))
        (encode-base-36 new-number (push (elt *base36-alphabet* remainder)
                                         accum)))))

(defun generate-mail-message-id ()
  (format nil
          "<~a~a.~a@~a>"
          (encode-base-36 (misc:milliseconds-from-unix-epoch))
          (random 2048)
          (random 2048)
          (usocket::get-host-name)))

(defun send-email (subject from to message server-name port username password ssl attachments)
  "Note that `ATTACHMENTS' must be a list of pathnames"
  (let ((credential (if (or (text-utils:string-empty-p username)
                            (text-utils:string-empty-p password))
                        nil
                        (list username password))))
    (log-message :debug "mail attachments ~a" attachments)
    (cl-smtp:send-email server-name
                        from
                        to
                        subject
                        message
                        :extra-headers  (list (list "Message-ID"
                                                    (misc::generate-mail-message-id)))
                        :ssl            ssl
                        :port           port
                        :authentication credential
                        :attachments    attachments)))

;; debug

(defun backtrace (condition)
  (with-output-to-string (stream)
    (uiop:print-condition-backtrace condition :stream stream)))

;; logging

(defparameter *logging-file*   nil)

(defparameter *logging-stream* nil)

(defun setf-logging-file ()
  (setf *logging-file* (fs:cat-parent-dir (os-utils:pwd) "rib.log")))

(defun log-time-format ()
  (let* ((now     (multiple-value-list (get-decoded-time)))
         (year    (time-year-of    now))
         (month   (time-month-of   now))
         (day     (time-date-of    now))
         (hour    (time-hour-of    now))
         (minute  (time-minutes-of now))
         (seconds (time-seconds-of now)))
    (format nil
            "~d-~2,'0d-~2,'0d ~2,'0d:~2,'0d:~2,'0d"
            year month day hour minute seconds)))

(defun open-debug-log-file (&key (reopen nil))
  (when reopen
    (close vom:*log-stream*)
    (fs:delete-file-if-exists *logging-file*))
  (setf vom:*log-stream*
        (open *logging-file*
              :direction :output
              :if-exists :append
              :if-does-not-exist :create)))

(cffi:defcfun ("openlog" ffi-openlog) :void
  (ident :pointer)
  (option :int)
  (facility :int))

(cffi:defcfun ("closelog" close-log) :void)

(cffi:defcfun ("syslog" ffi-sys-log) :void
  (level :int)
  (message :pointer))

(a:define-constant +legal-log-levels+ '(:emerg :alert :crit :err :warning :notice :info :debug) :test #'equalp)

(defun convert-log-level (level-as-keyword)
  (or (position level-as-keyword +legal-log-levels+)
      (error "~a is not a legal log level identifier" level-as-keyword)))

(defun sys-log (level control &rest arguments)
  (let ((message (apply #'format nil control arguments)))
    (cffi:with-foreign-string (foreign-message message)
      (ffi-sys-log (convert-log-level level)
                   foreign-message))))

(a:define-constant +log-pid+       #x01 :test #'=)

(a:define-constant +log-perror+    #x20 :test #'=)

(a:define-constant +log-user+         8 :test #'=)

(defun open-log (options)
  (cffi:with-foreign-string (program-name config:+program-name+)
    (ffi-openlog program-name +log-user+ options)))

(defun start-logging (&optional on-console-p)
  (cond
    ((swconf:debug-mode-p)
     (setf-logging-file)
     (setf vom:*time-formatter* #'log-time-format)
     (open-debug-log-file)
     (vom:config t :notice))
    (on-console-p
     (open-log (logior +log-pid+
                       +log-perror+)))
    (t
     (open-log +log-pid+))))

(defun stop-logging ()
  (if (swconf:debug-mode-p)
      (clear-output *logging-stream*)
      (close-log)))

(defparameter *logging-lock* (bt:make-lock))

(defun log-message (priority format &rest args)
  (when (not (cli:run-tasks-p))
    (if (swconf:debug-mode-p)
        (with-lock (*logging-lock*)
          (when (> (fs:file-size *logging-file*)
                   (swconf:max-debug-log-size))
            (open-debug-log-file :reopen t)
            (vom:notice "file truncated due to size limits"))
          (let ((message (apply #'format nil format args)))
            (vom:notice "~s ~a" priority message)))
        (when (not (eq priority :debug))
          (apply #'sys-log priority format args)))))

;; serialization

(defgeneric serialize (object))

(defgeneric serialize-to-stream (object stream))

(defgeneric deserialize (object))

(defmethod serialize (object)
  (format nil "~s" (marshal:marshal object)))

(defmethod serialize-to-stream (object stream)
  (prin1 (marshal:marshal object) stream))

(defmethod deserialize ((object pathname))
  (deserialize (filesystem-utils:slurp-file object)))

(defmethod deserialize ((object string))
  (let ((err nil)
        (count-parens 0)
        (in-string nil))
    (loop for c across object do
      (cond
        ((char= c #\")
         (setf in-string (not in-string)))
        ((get-macro-character c)
         (cond
           ((char= c #\()
            (incf count-parens))
           ((char= c #\))
            (decf count-parens))
           (t
            (when (not in-string)
              (setf err t)))))))
    (when (/= count-parens 0)
      (setf err t))
    (when (not err)
      (marshal:unmarshal (read-from-string object)))))

;; cloning

(defgeneric clone (object))

(defmethod clone (object))

(defgeneric clone-into (from to))

(defmethod  clone-into (from to))

(defgeneric copy-flat (object))

(defmethod  copy-flat (object))

(defgeneric copy-flat-into (from to))

(defmethod  copy-flat-into (from to))

(defmacro with-simple-clone ((object type))
  (alexandria:with-gensyms (res)
    `(let ((,res (make-instance ,type)))
       (clone-into ,object ,res)
       ,res)))

(defmacro with-simple-copy-flat ((object type))
  (alexandria:with-gensyms (res)
    `(let ((,res (make-instance ,type)))
       (copy-flat-into ,object ,res)
       ,res)))

;; file types

(defun check-mime-file (file expected-mime)
  (magicffi:with-open-magic (magic '(:mime-type :symlink))
    (magicffi:magic-load magic)
    (let ((mime-type (magicffi:magic-file magic file)))
      (cl-ppcre:scan expected-mime mime-type))))

(defun file-shell-script-p (file)
  (check-mime-file file "text/x-shellscript"))

(defun file-elf-p (file)
  (check-mime-file file "application/x-pie-executable"))

;; timeout

(defmacro with-timeout ((seconds) &body body)
  `(sb-sys:with-deadline (:seconds ,seconds)
     ,@body))

;; some numeric utils

(defparameter *default-epsilon* 1e-7)

(defmacro with-epsilon ((epsilon) &body body)
  `(let ((*default-epsilon* ,epsilon))
     ,@body))

(defun add-epsilon-rel (v &optional (epsilon *default-epsilon*))
  (+ v (* epsilon v)))

(defun epsilon<= (a b &optional (epsilon *default-epsilon*))
  (or (<= a b)
      (epsilon= a b epsilon)))

(defun epsilon>= (a b &optional (epsilon *default-epsilon*))
  (or (>= a b)
      (epsilon= a b epsilon)))

(defun epsilon= (a b &optional (epsilon *default-epsilon*))
  (and (<= (- b epsilon) a (+ b epsilon))))

(defun enzyme-kinetics (max k x)
  (/ (* max x) (+ k x)))

;; sleep process

(cffi:defcfun (ffi-nanosleep "nanosleep")
  :int
  (delay :pointer)
  (rem   :pointer))

(defun nanosleep (seconds nanoseconds residual-time)
  (cffi:with-foreign-object (times '(:struct timespec))
    (setf (cffi:foreign-slot-value times '(:struct timespec) 'seconds) seconds)
    (setf (cffi:foreign-slot-value times '(:struct timespec) 'nseconds) nanoseconds)
    (ffi-nanosleep times residual-time)))

(defun sleep-milliseconds (milliseconds)
  (cffi:with-foreign-object (residual-time '(:struct timespec))
    (handler-case
        (let* ((nanoseconds           (milliseconds->nanoseconds milliseconds))
               (seconds               (nanoseconds->seconds nanoseconds))
               (remaining-nanoseconds (- nanoseconds (seconds->nanoseconds seconds))))
          (nanosleep seconds remaining-nanoseconds residual-time))
      (error (e)
        (ffi-nanosleep residual-time (cffi:null-pointer))
        (log-message :crit "sleep error ~a." e)))))

;; base64

(defun base64-encode (data)
  (cryptos:to-base64 data))

(defun base64-decode-octets (data)
  (cryptos:from-base64 data :octets))

(defun base64-decode-string (data)
  (cryptos:from-base64 data :string))
