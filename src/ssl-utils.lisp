;; rib: a notification system
;; Copyright (C) 2022  Università degli studi di Palermo

;; derived from tinmop © 2022 cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :ssl-utils)

(defun open-tls-socket (hostname port)
  (usocket:socket-connect hostname
                          port
                          :element-type '(unsigned-byte 8)))

(defun close-tls-socket (socket)
  (usocket:socket-close socket))

(defun fetch-certificate (host &optional (port 443))
  (let ((ctx (cl+ssl:make-context :verify-mode cl+ssl:+ssl-verify-none+)))
    (cl+ssl:with-global-context (ctx :auto-free-p t)
      (let* ((socket       (open-tls-socket host port))
             (stream       (usocket:socket-stream socket))
             (ssl-stream   (cl+ssl:make-ssl-client-stream stream
                                                          :unwrap-stream-p t
                                                          :verify          nil
                                                          :hostname        host)))
        (unwind-protect
             (cl+ssl:ssl-stream-x509-certificate ssl-stream)
          (close ssl-stream)
          (close stream)
          (close-tls-socket socket))))))

(defun certificate-expiry-date (certificate)
  (local-time:universal-to-timestamp (cl+ssl:certificate-not-after-time certificate)))

(defun fetch-certificate-expiry-date (host &optional (port 443))
  (let ((x509-cert (fetch-certificate host port)))
    (unwind-protect
         (certificate-expiry-date x509-cert)
      (cl+ssl::x509-free x509-cert))))

(defun pem->der (pem-file)
  (handler-case
      (let* ((raw     (fs:slurp-file pem-file))
             (encoded (cl-ppcre:regex-replace-all "-----(BEGIN|END) CERTIFICATE-----" raw ""))
             (decoded (misc:base64-decode-octets encoded)))
        (fs:with-anaphoric-temp-file (stream)
          (write-sequence decoded stream)
          filesystem-utils::temp-file))
   (error () pem-file)))

(defgeneric certificate-fingerprint (object &key hash-algorithm))

(defmacro decode-fingerprint (cert hash-algorithm)
  (alexandria:with-gensyms (hash hash-string algo-string)
    `(unwind-protect
          (let* ((,hash        (cl+ssl:certificate-fingerprint ,cert ,hash-algorithm))
                 (,hash-string (format nil "~{~2,'0x~}" (map 'list #'identity ,hash)))
                 (,algo-string (format nil "~:@(~a~)" ,hash-algorithm)))
            (text-utils:strcat ,algo-string ":" (string-downcase ,hash-string)))
       (cl+ssl:x509-free ,cert))))

(defmethod certificate-fingerprint ((object cl+ssl::ssl-stream) &key (hash-algorithm :sha256))
  (let* ((cert (cl+ssl:ssl-stream-x509-certificate object)))
    (decode-fingerprint cert hash-algorithm)))

(defmethod certificate-fingerprint ((object string) &key (hash-algorithm :sha256))
  (let* ((cert (cl+ssl:decode-certificate-from-file (pem->der object) :format :der)))
    (decode-fingerprint cert hash-algorithm)))
