;; rib: a general scheduler.
;; Copyright (C) 2021 Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free  Software Foundation, either  version 3 of the  License, or
;; (at your option) any later version.

;; This program is distributed in the  hope that it will be useful, but
;; WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
;; MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
;; General Public License for more details.

;; You should  have received a copy  of the GNU General  Public License
;; along      with      this       program.       If      not,      see
;; <http://www.gnu.org/licenses/>.

(in-package :notification)

(defparameter *waiting-queue* (make-instance 'q:queue))

(defclass notification ()
  ((level
    :initform :info
    :initarg  :level
    :reader level)
   (metadata
    :initform ""
    :initarg  :metadata
    :reader metadata)
   (payload
    :initform nil
    :initarg  :payload
    :accessor payload)
   (destination
    :initform nil
    :initarg  :destination
    :accessor destination)
   (attachments
    :initform '()
    :initarg  :attachments
    :accessor attachments)
   (send-to-destination
    :initform t
    :initarg  :send-to-destination
    :reader   send-to-destination-p
    :writer   (setf send-to-destination))))

(defmethod (setf level) (new-level (object notification))
  (assert (member new-level '(:info :warning :error :debug :crit)))
  (setf (slot-value object 'level) new-level))

(defmethod ms:class-persistent-slots ((object notification))
  '(level payload metadata destination attachments send-to-destination))

(defgeneric notify (object))

(defmethod notify (object))

(defgeneric enqueue (object))

(defgeneric notification->channel-name (object))

(defmethod notification->channel-name ((object notification))
  nil)

(defmethod enqueue (object)
  object)

(defun extract ()
  (q:pop-block *waiting-queue*))

(defmethod enqueue :after ((object notification))
  (let ((syslog-needed-p (or swconf:*debug-mode*
                             (not (eq (level object) :debug)))))
    (when syslog-needed-p
      (misc:log-message (level object)
                        "Notifiyng ~s to ~s"
                        (payload object)
                        (destination object)))
    (q:push-unblock *waiting-queue* object)
    (when syslog-needed-p
      (misc:log-message (level object) "Notified pushed to stack"))
    object))

(defclass notification-noop (notification) ())

(defmethod notify ((object notification-noop))
  (log-message :debug
               "Noop notified: ~s ~s to ~s"
               (level object)
               (payload object)
               (destination object)
               (attachments object)))

(defmethod enqueue ((object notification-noop))
  object)

(defclass notification-mail (notification) ())

(defmethod notification->channel-name ((object notification-mail))
  :mail)

(defmethod notify ((object notification-mail))
  (let ((subject            (format nil "[~a] ~a ~a"
                                    +program-name+
                                    (level object)
                                    (metadata object)))
        (actual-attachments (remove-if-not #'fs:file-exists-p
                                           (mapcar #'fs:path-truename
                                                   (attachments object)))))
    (notification-channels:notify-email subject
                                        (payload     object)
                                        (destination object)
                                        actual-attachments)))

(defmethod enqueue ((object notification-mail))
  object)

(defclass notification-telegram (notification) ())

(defmethod notification->channel-name ((object notification-telegram))
  :telegram)

(defmethod notify ((object notification-telegram))
  (notification-channels:notify-telegram (format nil
                                                 "~a ~a"
                                                 (level object)
                                                 (payload object))
                                         (destination object)))

(defmethod enqueue ((object notification-telegram))
  object)

(defun start-notification-loop ()
  (let ((thread (bt:make-thread (lambda ()
                                  (loop while (not (tsm:stop-all-threads-p))
                                        do
                                           (let ((notification (extract)))
                                             (handler-case
                                                 (notify notification)
                                               (error (e)
                                                 (let ((message (format nil
                                                                        "notification thread chrashed ~a"
                                                                        e)))
                                                   (log-message :crit message))))))
                                  (log-message :info "notification thread terminating"))
                                :name "notification thread")))
    (setf tsm:*notification-loop* thread)))

(a:define-constant +notification-dump-file+ "notifications.sexp" :test #'string=)

(defun dump-notifications-queue ()
  (let ((filename (os-utils:cached-file-path +notification-dump-file+)))
    (log-message :info (n_ "Dumping ~a notification in ~a"
                           "Dumping ~a notifications in ~a"
                           (length (q:container *waiting-queue*)))
                 (length (q:container *waiting-queue*))
                 filename)
    (with-open-file (stream
                     filename
                     :direction          :output
                     :if-exists          :supersede
                     :if-does-not-exist  :create)
      (misc:serialize-to-stream (q:container *waiting-queue*) stream))))

(defun restore-notifications-queue ()
  (let* ((filename (os-utils:cached-file-path +notification-dump-file+)))
    (when (fs:file-exists-p filename)
      (let ((notifications (misc:deserialize (fs:namestring->pathname filename))))
        (log-message :info
                     (n_ "Restored ~a notification from disk"
                         "Restored ~a notifications from disk"
                         (length notifications))
                     (length notifications))
        (setf *waiting-queue*
              (make-instance 'q:queue
                             :container notifications))))))

(defun stop-notification-loop ()
  (enqueue (make-instance 'notification:notification-noop)))

(defun destroy-notification-loop ()
  (bt:destroy-thread tsm:*notification-loop*))
