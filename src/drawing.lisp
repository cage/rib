;; rib: a notification system
;; Copyright (C) 2022  Università degli studi di Palermo

;; derived from tinmop © 2022 cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

;; Derived from:

;; tinmop: an humble gemini and pleroma client
;; Copyright (C) 2022  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :drawing)

(defparameter *main-window* nil)

(defclass wrapper-window ()
  ((croatoan-window
    :initform nil
    :initarg  :croatoan-window
    :accessor croatoan-window
    :documentation "The lowlevel (ncurses) window")))

(defmethod initialize-instance :after ((object wrapper-window) &key &allow-other-keys))

(defmethod print-object ((object wrapper-window) stream)
  (print-unreadable-object (object stream :type t :identity nil)))

(defgeneric draw (object))

(defmethod draw ((object t)))

(defmethod draw :after ((object wrapper-window))
  (win-clear object))

(defclass border-window ()
  ((uses-border-p
    :initform t
    :initarg  :uses-border-p
    :reader   uses-border-p))
  (:documentation "This is a window that has a border."))

(defun window-uses-border-p (window)
  (and window
       (typep window 'border-window)
       (uses-border-p window)))

(defgeneric usable-window-width (object))

(defmethod  usable-window-width ((object border-window))
  (if (uses-border-p object)
      (win-width-no-border object)
      (win-width           object)))

(defmethod draw :after ((object border-window))
  (when (uses-border-p object)
    (win-box object)))

(defclass title-window ()
  ((title
    :initform ""
    :initarg  :title
    :accessor title
    :documentation "The actual title")
   (title-padding-left
    :initform 3
    :initarg  :title-padding-left
    :accessor title-padding-left
    :documentation "left padding text for title")
   (left-stopper
    :initform "|"
    :initarg  :left-stopper
    :accessor left-stopper
    :documentation "The text before the actual title")
   (right-stopper
    :initform "|"
    :initarg  :right-stopper
    :accessor right-stopper
    :documentation "The text after the actual title"))
  (:documentation "This is a window that dplays a title

 ---border--- left-stopper title-padding-left right-stopper ---border---"))

(defmethod (setf title) ((new-title string) (object title-window))
  (with-slots (title) object
    (with-accessors ((left-stopper       left-stopper)
                     (right-stopper      right-stopper)
                     (title-padding-left title-padding-left)) object
      (let ((clean-title (text-utils:ellipsize (text-utils:trim-blanks new-title)
                                               (truncate (/ (- (win-width object)
                                                               (length left-stopper)
                                                               (length right-stopper)
                                                               title-padding-left)
                                                            2)))))
        (setf title clean-title))))
  object)

(defmethod draw :after ((object title-window))
  (with-accessors ((left-stopper       left-stopper)
                   (right-stopper      right-stopper)
                   (title-padding-left title-padding-left)
                   (title              title)) object
    (print-text object left-stopper        title-padding-left 0)
    (print-text object title               nil                nil)
    (print-text object right-stopper       nil                nil)))

(defmacro with-croatoan-window ((slot window) &body body)
  `(with-accessors ((,slot croatoan-window)) ,window
     ,@body))

(defun win-clear (window &key (redraw t))
  "Clear window content"
  (croatoan:clear (croatoan-window window) :target :window :redraw redraw))

(defmacro gen-simple-win->croatoan-specialized-wrapper (fn-name &optional (prefix nil))
  "Generate micro  wrapper for simple curses  library function (window
height, position and so on)"
  (a:with-gensyms (window inner)
    `(defun ,(misc:format-fn-symbol t "~@[~a-~]~a" prefix fn-name) (,window)
       (with-croatoan-window (,inner ,window)
         (,(misc:format-fn-symbol :croatoan "~a" fn-name) ,inner)))))

(gen-simple-win->croatoan-specialized-wrapper width   win)

(gen-simple-win->croatoan-specialized-wrapper height  win)

(gen-simple-win->croatoan-specialized-wrapper box     win)

(gen-simple-win->croatoan-specialized-wrapper bgcolor win)

(gen-simple-win->croatoan-specialized-wrapper fgcolor win)

(gen-simple-win->croatoan-specialized-wrapper refresh win)

(gen-simple-win->croatoan-specialized-wrapper touch  win)

(defun win-close (window)
  (with-croatoan-window (croatoan-window window)
    (close croatoan-window)))

(defun win-width-no-border (win)
  (- (win-width win)
     2))

(defun win-height-no-border (win)
  (- (win-height win)
     2))

(defun win-x (win)
  (with-croatoan-window (inner-window win)
    (second (croatoan:widget-position inner-window))))

(defun win-y (win)
  (with-croatoan-window (inner-window win)
    (first (croatoan:widget-position inner-window))))

(defmacro with-window-width ((win w) &body body)
  `(let ((,w (win-width  ,win)))
     ,@body))

(defmacro with-window-height ((win h) &body body)
  `(let ((,h (win-height  ,win)))
     ,@body))

(defmacro with-window-sizes ((win w h) &body body)
  `(with-window-width (,win ,w)
     (with-window-height (,win ,h)
     ,@body)))

(defun win-move-cursor (window x y &key relative)
  "Wrapper of croatoan:move-window"
  (with-croatoan-window (inner window)
    (croatoan:move inner y x :relative relative)))

(defun win-move-cursor-direction (window direction &optional (n 1))
  "Wrapper for croatoan:move-direction"
  (with-croatoan-window (inner window)
    (croatoan:move-direction inner direction n)))

(defun win-move (window x y &key relative)
  "Wrapper for croatoan:move-window"
  (with-croatoan-window (inner window)
    (croatoan:move-window inner y x :relative relative)))

(defun win-resize (window width height)
  "Wrapper for croatoan:resize"
  (with-croatoan-window (inner window)
    (croatoan:resize inner height width)))

(defun win-show (window)
  "Show a window (must be stacked, see croatoan)"
  (with-croatoan-window (inner window)
    (setf (croatoan:visiblep inner) t)))

(defun win-hide (window)
  "Hide a window (must be stacked, see croatoan)"
  (with-croatoan-window (inner window)
    (setf (croatoan:visiblep inner) nil)))

(defun win-shown-p (window)
  "Show a window (must be stacked, see croatoan)"
  (with-croatoan-window (inner window)
    (croatoan:visiblep inner)))

(defun win-set-background (window bg)
  "Set window background
   - window an instance of 'wrapper-window';
   - bg the returns value of 'tui-utils:make-win-background'"
  (with-croatoan-window (inner window)
    (setf (croatoan:background inner) bg)))

(defmethod print-text ((object wrapper-window) (text string) x y
                       &key
                         (attributes nil)
                         (fgcolor    nil)
                         (bgcolor    nil)
                       &allow-other-keys)
  (print-text object
              (make-tui-string text
                               :attributes attributes
                               :fgcolor    fgcolor
                               :bgcolor    bgcolor)
              x y))

(defmethod print-text ((object wrapper-window) (text croatoan:complex-string) x y
                       &key &allow-other-keys)
  (croatoan:add (croatoan-window object) text :x x :y y))

(defmethod print-text ((object wrapper-window) (text character) x y
                       &key
                         (attributes nil)
                         (fgcolor    nil)
                         (bgcolor    nil)
                       &allow-other-keys)
  (croatoan:add (croatoan-window object)
                (string text)
                :x          x
                :y          y
                :attributes attributes
                :bgcolor    bgcolor
                :fgcolor    fgcolor))

(defmethod print-text ((object wrapper-window) (text list) x y &key &allow-other-keys)
  (loop
    for block in text
    with current-x = x do
      (croatoan:add (croatoan-window object)
                    block
                    :x current-x
                    :y y)
      (incf current-x (text-length block)))
  object)

(defmethod print-text ((object wrapper-window) text x y &key &allow-other-keys)
  (print-text object (text-utils:to-s text) x y))

(defun cursor-show ()
  (setf (croatoan:cursor-visible-p (croatoan-window *main-window*)) t))

(defun cursor-hide ()
  (setf (croatoan:cursor-visible-p (croatoan-window *main-window*)) nil))

(defclass info-window (title-window border-window wrapper-window) ())

(defgeneric reposition (object))

(defmethod reposition ((object info-window))
  (with-accessors ((croatoan-window croatoan-window)) object
    (let ((width  (truncate (/ (win-width *main-window*)
                               2)))
          (height (- (win-height *main-window*)
                     (help-line-height *main-window*))))
      (win-resize object width height)
      (win-move object
                (- (win-width *main-window*)
                   width)
                0)
      object)))

(defmethod initialize-instance :after ((object info-window) &key &allow-other-keys)
  (with-accessors ((croatoan-window croatoan-window)) object
    (setf croatoan-window (make-croatoan-window))
    (reposition object)
    (setf (title object) (_ "Information"))
    object))

(defmethod draw :after ((object info-window))
  (let* ((width              (win-width-no-border object))
         (critical-label     (make-tui-string (_ "Number of critical notifications")
                                              :fgcolor :cyan
                                              :attributes (attribute-bold)))
         (critical-count     (make-tui-string (text:to-s (statistics:count-critical-notifications))))
         (pool-size-label    (make-tui-string (_ "Pool size")
                                              :fgcolor :cyan
                                              :attributes (attribute-bold)))
         (pool-size          (make-tui-string (text:to-s (tsm:threading-pool-length))))
         (memory-label       (make-tui-string (_ "Memory used")
                                              :fgcolor :cyan
                                              :attributes (attribute-bold)))
         (memory             (os-utils:memory-used))
         (notification-label (make-tui-string (_ "Notification waiting count")
                                              :fgcolor :cyan
                                              :attributes (attribute-bold)))
         (input-fifo-label   (make-tui-string (_ "Input FIFO path")
                                              :fgcolor :cyan
                                              :attributes (attribute-bold)))
         (input-fifo-path    (text-utils:to-s (fifo:input-fifo-path)))
         (output-fifo-label  (make-tui-string (_ "Output FIFO path")
                                              :fgcolor :cyan
                                              :attributes (attribute-bold)))
         (output-fifo-path   (text-utils:to-s (fifo:output-fifo-path)))
         (waiting-queue-size (text-utils:to-s (q:size notification:*waiting-queue*))))
    (flet ((print-row (text y)
             (print-text object (text-ellipsis text width) 1 y)))
      (reposition object)
      (print-row input-fifo-label    1)
      (print-row input-fifo-path     2)
      (print-row output-fifo-label   3)
      (print-row output-fifo-path    4)
      (print-row critical-label      5)
      (print-row critical-count      6)
      (print-row pool-size-label     7)
      (print-row pool-size           8)
      (print-row memory-label        9)
      (print-row memory             10)
      (print-row notification-label 11)
      (print-row waiting-queue-size 12))))

(defclass histogram-window (wrapper-window)
  ((histogram-index
    :initform 0
    :initarg  :histogram-index
    :accessor histogram-index)
   (histogram-height
    :initform (swconf:tui-histogram-height)
    :initarg  :histogram-height
    :accessor histogram-height)
   (histogram-type
    :initform :called
    :initarg  :histogram-type
    :accessor histogram-type)))

(defun histogram-per-page (histogram-window)
  (with-accessors ((histogram-height histogram-height)) histogram-window
    (let* ((help-line-height (help-line-height *main-window*))
           (effective-height (- (win-height histogram-window)
                                help-line-height)))
      (handler-case
          (truncate (/ effective-height histogram-height))
        (error () 0)))))

(defun histogram-page-number (histogram-window)
  (handler-case
      (let* ((histogram-per-page (histogram-per-page histogram-window))
             (all-names          (statistics:all-test-names)))
        (truncate (/ (length all-names)
                     histogram-per-page)))
    (error () 0)))

(defun increase-index (histogram-window)
  (when (> (length (statistics:all-test-names))
           0)
    (let ((new-index (+ (histogram-index histogram-window)
                        (histogram-per-page histogram-window))))
      (if (>= new-index
              (length (statistics:all-test-names)))
          (setf (histogram-index histogram-window) 0)
          (setf (histogram-index histogram-window) new-index)))))

(defun print-histograms (histogram-window)
  (with-accessors ((histogram-type   histogram-type)
                   (histogram-height histogram-height)
                   (histogram-index  histogram-index)) histogram-window
    (let ((help-line-height   (help-line-height *main-window*)))
      (when (> (win-height histogram-window)
               (+ histogram-height
                  help-line-height))
        (flet ((normalize-plot (raw-data max-value drawing-height)
                 (loop for i in raw-data
                       collect
                       (handler-case
                           (truncate (* (/ i max-value)
                                        drawing-height))
                         (error () 0)))))
          (a:when-let* ((all-names          (statistics:all-test-names))
                        (histogram-number   (histogram-per-page histogram-window))
                        (histogram-fn       (if (eq histogram-type :called)
                                                #'statistics:called-histogram
                                                #'statistics:triggered-histogram))
                        (all-histograms-raw (loop for name in all-names
                                                  collect
                                                  (funcall histogram-fn name)))
                        (actual-histogram-height (- histogram-height 2))
                        (all-histograms     (loop for i from 0 below (length all-histograms-raw)
                                                  collect
                                                  (let* ((raw-data   (elt all-histograms-raw i))
                                                         (max-value  (reduce #'max raw-data))
                                                         (normalized
                                                           (normalize-plot raw-data
                                                                           max-value
                                                                           actual-histogram-height)))
                                                    (text-utils:histogram-plot normalized
                                                                               actual-histogram-height))))
                        (histograms         (misc:safe-subseq all-histograms
                                                              histogram-index
                                                              (+ histogram-index
                                                                 histogram-number)))
                        (names              (misc:safe-subseq all-names
                                                              histogram-index
                                                              (+ histogram-index
                                                                 histogram-number))))
            (loop for histogram in histograms
                  for name in names
                  for y from 0 by histogram-height
                  for index from histogram-index
                  do
                     (print-text histogram-window histogram 0 y :fgcolor :green)
                     (loop for i from 0 below 24 by 2 do
                       (print-text histogram-window
                                   (text-utils:to-s i)
                                   (* i 2)
                                   (+ y actual-histogram-height)
                                   :fgcolor :white))
                     (let* ((caption-max  (format nil
                                                  (_ "(maximum value: ~a)")
                                                  (reduce #'max
                                                          (elt all-histograms-raw index)
                                                          :initial-value 0)))
                            (caption-name (text-utils:ellipsize name
                                                                (- (win-width-no-border histogram-window)
                                                                   (length caption-max)))))

                       (print-text histogram-window
                                   caption-name
                                   0
                                   (+ y actual-histogram-height 1)
                                   :attributes (tui:attribute-bold))
                       (print-text histogram-window
                                   caption-max
                                   (1+ (length caption-name))
                                   (+ y actual-histogram-height 1))))))))))

(defun next-page-key ()
    (_ "n"))

(defun toggle-type-key ()
    (_ "t"))

(defun quit-key ()
    (_ "q"))

(a:define-constant +toggle-type-key+ #\t :test #'char=)

(defun print-keys (dashboard)
  (when (> (win-height dashboard)
           (help-line-height dashboard))
    (with-croatoan-window (inner dashboard)
      (let* ((y                     (1- (win-height dashboard)))
             (next-page-label-key   (make-tui-string  (format nil "[~a]" (next-page-key))
                                                      :attributes (attribute-bold)
                                                      :fgcolor :green))
             (change-type-label-key (make-tui-string  (format nil "[~a]" (toggle-type-key))
                                                      :attributes (attribute-bold)
                                                      :fgcolor :green))
             (quit-label-key        (make-tui-string  (format nil
                                                              "[~a]"
                                                              (quit-key))
                                                      :attributes (attribute-bold)
                                                      :fgcolor :green))
             (next-page-label       (make-tui-string  (_ "ext page ")))
             (change-type-label     (make-tui-string  (_ "oggle histogram type ")))
             (quit-type-label       (make-tui-string  (_ "uit")))
             (histogram-window      (histogram-window dashboard))
             (current-page          (1+ (truncate (/ (histogram-index histogram-window)
                                                     (max 1
                                                          (histogram-per-page histogram-window)))))))
        (croatoan:add inner
                      (format nil
                              (_ "Pages: ~a/~a Type: ~a ")
                              current-page
                              (1+ (histogram-page-number (histogram-window dashboard)))
                              (histogram-type (histogram-window dashboard)))
                      :x 0
                      :y y)
        (croatoan:add inner (_ " Commands: ") :fgcolor :green :attributes '(:bold))
        (croatoan:add inner (cat-tui-string next-page-label-key next-page-label))
        (croatoan:add inner (cat-tui-string change-type-label-key change-type-label))
        (croatoan:add inner (cat-tui-string quit-label-key quit-type-label))))))

(defmethod initialize-instance :after ((object histogram-window) &key &allow-other-keys)
  (with-accessors ((croatoan-window croatoan-window)) object
    (reposition object)
    object))

(defmethod reposition ((object histogram-window))
  (with-accessors ((croatoan-window croatoan-window)) object
    (let ((width  (truncate (/ (win-width *main-window*)
                               2)))
          (height (- (win-height *main-window*)
                     (help-line-height *main-window*))))
      (win-resize object width height)
      (win-move object 0 0)
      object)))

(defmethod draw :after ((object histogram-window))
  (reposition object)
  (print-histograms object))

(defclass dashboard (wrapper-window)
  ((info-window
    :initform :info-window
    :initarg  :info-window
    :accessor info-window)
   (histogram-window
    :initform :histogram-window
    :initarg  :histogram-window
    :accessor histogram-window)
   (help-line-height
    :initform 2
    :initarg  :help-line-height
    :accessor help-line-height)))

(defmethod initialize-instance :after ((object dashboard) &key &allow-other-keys))

(defmethod draw :after ((object dashboard))
  (print-keys object)
  object)

(defun setup-bindings ()
  (with-croatoan-window (low-level-window *main-window*)
    (croatoan:bind low-level-window
                   :resize
                   (lambda (w event)
                     (declare (ignore w event))
                     (drawing:draw-all)))
    (croatoan:bind low-level-window
                   t
                   (lambda (w event)
                     (declare (ignore w))
                     (a:when-let ((key (and (characterp (croatoan:event-key event))
                                            (croatoan:event-key event))))
                       (cond
                         ((string= (text-utils:to-s (croatoan:event-key event))
                                   (quit-key))
                          (with-open-file (stream (fifo-communication:input-fifo-path)
                                                  :direction :io
                                                  :if-does-not-exist :error)
                            (format stream "~a~%" fifo-communication:+quit-command+)
                            (finish-output stream)))
                         ((string= (text-utils:to-s (croatoan:event-key event))
                                   (next-page-key))
                          (increase-index (histogram-window *main-window*)))
                         ((string= (text-utils:to-s (croatoan:event-key event))
                                   (toggle-type-key))
                          (if (eq (histogram-type (histogram-window *main-window*))
                                  :called)
                              (setf (histogram-type (histogram-window *main-window*)) :triggered)
                              (setf (histogram-type (histogram-window *main-window*)) :called))))
                       (drawing:draw-all))))
    ;; this is the main thread
    (croatoan:bind low-level-window
                   nil
                   (lambda (w e)
                     (declare (ignore w e))
                     (drawing:draw-all)))
    (croatoan:run-event-loop low-level-window)))

(defun draw-all ()
  (draw *main-window*)
  (draw (info-window *main-window*))
  (draw (histogram-window *main-window*))
  (croatoan:mark-for-refresh (croatoan-window *main-window*))
  (croatoan:mark-for-refresh (croatoan-window (info-window *main-window*)))
  (croatoan:mark-for-refresh (croatoan-window (histogram-window *main-window*)))
  (croatoan:refresh-marked))

(defun init-tui ()
  (let ((screen (make-screen)))
    (setf *main-window* (make-instance 'dashboard :croatoan-window screen))
    (setf (histogram-window *main-window*)
          (make-instance 'histogram-window
                         :croatoan-window (make-croatoan-window)))
    (setf (info-window *main-window*) (make-instance 'info-window))
    (setf (croatoan:frame-rate (croatoan-window *main-window*)) 1)
    (win-show *main-window*)
    (win-show (info-window *main-window*))
    (win-show (histogram-window *main-window*))
    (let ((croatoan::*debugger-hook* #'(lambda (c h)
                                         (declare (ignore h))
                                         (croatoan:end-screen)
                                         (print c))))
      (setup-bindings))))
