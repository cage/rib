;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

;; derived from:

;; tinmop: an humble gemini and pleroma client
;; Copyright (C) 2022  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :os-utils)

(alexandria:define-constant +ssl-cert-name+ "cert.pem" :test #'string=)

(alexandria:define-constant +ssl-key-name+  "key" :test #'string=)

(defun getenv (name)
  (uiop:getenv name))

(defun default-temp-dir ()
  (or (os-utils:getenv "TMPDIR")
      "/tmp/"))

(defun pwd ()
  (os-utils:getenv "PWD"))

(defun make-fifo (path)
  (when (not (fs:file-exists-p path))
    (nix:mkfifo path #o600)))

(defun run-external-program (program args
                             &key
                               (wait t)
                               search
                               #-win32 pty
                               input
                               output
                               (error :output))
  #-win32 (sb-ext:run-program program
                      args
                      :wait   wait
                      :search search
                      :pty    pty
                      :input  input
                      :output output
                      :error  error)
  #+win32 (sb-ext:run-program program
                      args
                      :wait   wait
                      :search search
                      :input  input
                      :output output
                      :error  error))

(defun process-exit-code (process)
  (sb-ext:process-exit-code process))

(defun process-exit-success-p (process)
  (= (process-exit-code process) 0))

(defun slurp-external-program (program-file &optional (args nil))
  (with-output-to-string (stream)
    (sb-ext:run-program program-file
                        args
                        :wait   t
                        :search nil
                        :pty    nil
                        :input  nil
                        :output stream
                        :error  nil)))

(defun slurp-octets-external-program (program-file &optional (args nil))
  (flexi-streams:with-output-to-sequence (stream)
    (sb-ext:run-program program-file
                        args
                        :wait   t
                        :search nil
                        :pty    nil
                        :input  nil
                        :output stream
                        :error  nil)))

(defun exit-program (&optional (exit-code 0))
  (uiop:quit exit-code))

(defun user-cache-dir (&rest more)
  (fs:pathname->namestring (apply #'uiop:xdg-cache-home
                                  (append (list +program-name+) more))))

(defun cached-file-path (filename)
  (fs:cat-parent-dir (user-cache-dir) filename))

(defun open-pid-stat (&optional pid)
  (a:when-let* ((actual-pid (if pid
                                (text-utils:to-s pid)
                                "self"))
                (file       (fs:cat-parent-dir (fs:cat-parent-dir "/proc/" actual-pid)
                                               "stat"))
                (data       (with-open-file (stream file :if-does-not-exist nil)
                              (read-line stream)))
                (fields     (text-utils:split-words data)))
    fields))

(cffi:defcfun (get-memory-page-size "getpagesize") :int)

(defun memory-used (&optional (pid nil))
  (let* ((fields  (open-pid-stat pid))
         (pages   (parse-integer (elt fields 23)))
         (octects (* pages (get-memory-page-size))))
    (fs:octects->units-string octects)))

(defun open-ssh-stream (host command username private-key-path)
  (ssh:with-connection (stream host (ssh:key username private-key-path))
    (ssh:with-command (stream iostream command)
      stream)))
