;; rib: a notification system
;; Copyright (C) 2022  Università degli studi di Palermo

;; derived from tinmop  © 2022 cage and © 2014  Paul M. Rodriguez (see
;; LICENSES.org).

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :hooks)

(defvar *hook* nil
  "The hook currently being run.")

(defgeneric add-hook (hook fn &key append)
  (:documentation "Add FN to the value of HOOK.")
  (:method ((hook symbol) fn &key (append t))
    (declare (type (or function symbol) fn))
    (if (not append)
        (pushnew fn (symbol-value hook))
        (unless (member fn (symbol-value hook))
          (appendf (symbol-value hook) (list fn))))))

(defgeneric remove-hook (hook fn)
  (:documentation "Remove FN from the symbol value of HOOK.")
  (:method ((hook symbol) fn)
    (removef (symbol-value hook) fn)))

(defmacro with-hook-restart (&body body)
  `(with-simple-restart (continue "Call next function in hook ~s" *hook*)
     ,@body))

(defun run-hooks (&rest hooks)
  "Run all the hooks in HOOKS, without arguments.
The variable `*hook*' is bound to the name of each hook as it is being
run."
  (dolist (*hook* hooks)
    (run-hook *hook*)))

(defgeneric run-hook (hook &rest args)
  (:documentation "Apply each function in HOOK to ARGS.")
  (:method ((*hook* symbol) &rest args)
    (dolist (fn (symbol-value *hook*))
      (with-hook-restart
        (apply fn args)))))

(defgeneric run-hook-compose (hook args)
  (:documentation "Apply first function in HOOK to ARGS, second hook
to the results of first function applied and so on,
returns the results of the last hook.")
  (:method ((*hook* symbol) args)
    (let ((results args))
      (dolist (fn (symbol-value *hook*))
        (with-hook-restart
          (setf results (funcall fn results))))
      results)))

(defgeneric run-hook-until-failure (hook &rest args)
  (:documentation "Like `run-hook-with-args', but quit once a function returns nil.")
  (:method ((*hook* symbol) &rest args)
    (loop
       for fn in (symbol-value *hook*)
       always (apply fn args))))

(defgeneric run-hook-until-success (hook &rest args)
  (:documentation "Like `run-hook-with-args', but quit once a function returns
non-nil.")
  (:method ((*hook* symbol) &rest args)
    (loop
       for fn in (symbol-value *hook*)
       thereis (apply fn args))))

(defparameter *filter-sending-notification-hook* ()
  "Run this hooks to decide if send a notification: argument is the destination group")

(defparameter *before-recycling-hook* ()
  "Run this hooks to transform a task before pushing again in the waiting queue: argument is a task instance")

(defparameter *channel-notification-hook* ()
  "Run this hooks before sending the notification; parameters are:

   - channel
   - level
   - payload
   - metadata
   - destination
   - the notification was actually sent or not (boolean: non nil means 'was sent')")

(defparameter *server-deserialized-unknown-object-hook* ()
  "Run this hooks after an unknown-object has been received from the server, the argument is the object itself")
