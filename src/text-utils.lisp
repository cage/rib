;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

;; derived from:

;; tinmop: an humble gemini and pleroma client
;; Copyright (C) 2022  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; and derived from:

;; niccolo': a chemicals inventory
;; Copyright (C) 2016  Universita' degli Studi di Palermo

;; This  program is  free  software: you  can  redistribute it  and/or
;; modify it  under the  terms of  the GNU  General Public  License as
;; published  by  the  Free  Software Foundation,  version  3  of  the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :text-utils)

(defgeneric to-s (object))

(defmethod to-s ((object string))
  object)

(defmethod to-s ((object vector))
  (handler-bind ((sb-int:character-decoding-error
                   (lambda (e)
                     (declare (ignore e))
                     (invoke-restart 'use-value #\�))))
    (sb-ext:octets-to-string object)))

(defmethod to-s ((object character))
  (string object))

(defmethod to-s (object)
  (format nil "~a" object))

(defun strcat (&rest chunks)
  (declare (optimize (debug 0) (safety 0) (speed 3)))
  (strcat* chunks))

(defun strcat* (chunks)
  (declare (optimize (debug 0) (safety 0) (speed 3)))
  (reduce (lambda (a b) (concatenate 'string a b)) chunks))

(defun strip-prefix (string prefix)
  (let ((re (strcat "^" prefix)))
    (cl-ppcre:regex-replace re string "")))

(defun strip-withespaces (string)
  (let ((re "\\s"))
    (cl-ppcre:regex-replace re string "")))

(defgeneric join-with-strings (object junction))

(defmethod join-with-strings ((object sequence) (junction string))
  (reduce #'(lambda (a b) (text-utils:strcat a junction b)) object))

(defmethod join-with-strings ((object sequence) (junction character))
  (join-with-strings object (string junction)))

(defmethod join-with-strings ((object string) junction)
  (declare (ignore junction))
  object)

(defun join-with-strings* (junction &rest strings)
  (apply #'join-with-strings strings (list junction)))

(defvar *blanks* '(#\Space #\Newline #\Backspace #\Tab
                   #\Linefeed #\Page #\Return #\Rubout))

(defgeneric trim-blanks (s))

(defmethod trim-blanks ((s string))
  (string-trim *blanks* s))

(defmethod trim-blanks ((s null))
  s)

(defun split-words (text)
  (cl-ppcre:split "\\p{White_Space}" text))

(defun split-lines (text)
  (let ((res ()))
    (flex:with-input-from-sequence (stream (babel:string-to-octets text))
      (loop for line-as-array = (misc:read-line-into-array stream)
            while line-as-array do
              (push (babel:octets-to-string line-as-array) res)))
    (let ((*blanks* '(#\Newline)))
      (reverse (mapcar #'trim-blanks res)))))

(defun left-padding (str total-size &key (padding-char #\Space))
  (strcat (make-string (max 0 (- total-size (length str)))
                       :initial-element padding-char)
          str))

(defun left-padding-prefix (str total-size &key (padding-char #\Space))
  (make-string (max 0 (- total-size (length str)))
               :initial-element padding-char))

(defun ellipsize (string len &key (truncate-string "…"))
  "If 'string''s length is bigger  than 'len', cut the last characters
  out.  Also replaces the last n  characters (where n is the length of
  'truncate-string')     of     the      shortened     string     with
  'truncate-string'. It  defaults to  \"…\", but can  be nil  or the
  empty string."
  (let ((string-len (length string)))
    (cond
      ((<= string-len len)
       string)
      ((< len
          (length truncate-string))
       (subseq string 0 len))
      (t
       (strcat (subseq string 0 (- len (length truncate-string)))
               truncate-string)))))

(defgeneric string-empty-p (s))

(defmethod string-empty-p (s)
  (error 'type-error
         :datum         s
         :expected-type 'string))

(defmethod string-empty-p ((s null))
  (declare (ignore s))
  t)

(defmethod string-empty-p ((s string))
  (string= s ""))

(defun string-not-empty-p (s)
  (not (string-empty-p s)))

(defun string-starts-with-p (start s &key (test #'string=))
  "Returns non nil if `s' starts with the substring `start'.
Uses `test' to match strings (default #'string=)"
  (when (>= (length s)
            (length start))
    (funcall test s start :start1 0 :end1 (length start))))

(defun string-ends-with-p (end s &key (test #'string=))
  "Returns t if s ends with the substring 'end', nil otherwise.
Uses `test' to match strings (default #'string=)"
  (when (>= (length s)
            (length end))
    (funcall test s end :start1 (- (length s) (length end)))))

(defun percent-encode (string)
  (percent-encoding:encode string :encoding :utf-8))

(defun percent-decode (string)
  (percent-encoding:decode string :encoding :utf-8))

(defun percent-decode-allow-null (data)
  (when data
    (percent-decode data)))

(defun percent-encoded-p (string)
  (if (string-empty-p string)
      nil
      (progn
        (loop for i in (coerce string 'list)
              for ct from 0 do
                (cond
                  ((char= i #\%)
                   (when (not (cl-ppcre:scan "(?i)^%[0123456789abcdef]{2}" string :start ct))
                     (return-from percent-encoded-p nil)))
                  ((or (percent:reservedp i)
                       (char= i #\Space)
                       (not (or (percent:alphap      (char-code i))
                                (percent:digitp      (char-code i))
                                (percent:unreservedp (char-code i)))))
                   (return-from percent-encoded-p nil))))
        t)))

(defun percent-encode-allow-null (data)
  (when data
    (percent-encode data)))

(defun maybe-percent-encode (data)
  "Note that when data is null this function returns nil"
  (if (percent-encoded-p data)
      data
      (percent-encode-allow-null data)))

(defun histogram-plot (data &optional (height -1) (tick #\█) (base #\▁))
  (let* ((max-height (max height (reduce #'max data)))
         (pillars    (loop for datum in data
                           collect
                           (reverse (if (zerop datum)
                                        (append (list base)
                                                (make-list (1- max-height)
                                                           :initial-element #\Space))
                                        (append (make-list datum
                                                           :initial-element tick)
                                                (make-list (- max-height datum)
                                                           :initial-element #\Space)))))))
    (with-output-to-string (stream)
      (loop for y from 0 below max-height do
        (loop for x from 0 below (length data) do
          (write-char (elt (elt pillars x) y) stream))
        (format stream "~%")))))

(a:define-constant +digit-subscript+ "₀₁₂₃₄₅₆₇₈₉₋" :test #'string=)

(defun digit->subscript (digit)
  (elt +digit-subscript+
       (rem digit 10)))

(defun integer->subscript (number)
  (assert (integerp number))
  (let* ((minus          (if (< number 0)
                             (string (a:last-elt +digit-subscript+))
                             ""))
         (absolute-value (abs number))
         (big-endian     (if (zerop number)
                             (list (digit->subscript 0))
                             (loop while (> absolute-value 0)
                                   collect
                                   (let ((digit (rem absolute-value 10)))
                                     (setf absolute-value (truncate (/ absolute-value 10)))
                                     (digit->subscript digit))))))
    (strcat minus
            (coerce (reverse big-endian)
                    'string))))
