;; rib: a general scheduler.
;; derived from:
;; tinmop: an humble gemini and pleroma client
;; Copyright (C) 2022  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :threads-management)

(defparameter *stop-threads*      nil)

(defparameter *stop-threads-lock* (bt:make-lock))

(defparameter *testing-pool* nil)

(defparameter *recycling-thread* nil)

(defparameter *expanding-pool-thread* nil)

(defparameter *thread-testing-making-function* nil)

(defparameter *expanding-testing-pool-lock* (bt:make-lock))

(misc:defun-w-lock stop-all-threads ()
    *stop-threads-lock*
  (misc:log-message :info "Stopping all threads")
  (notification:stop-notification-loop)
  (core-tests:stop-testing-threads)
  (setf *stop-threads* t))

(misc:defun-w-lock stop-all-threads-p ()
    *stop-threads-lock*
  *stop-threads*)

(defun destroy-all-threads ()
  (misc:log-message :info "Destroiyng all threads")
  (notification:destroy-notification-loop)
  (core-tests:destroy-testing-threads)
  (bt:destroy-thread *expanding-pool-thread*)
  (setf *stop-threads* t))

(defun initialize-testing-pool (make-thread-fn)
  (setf *thread-testing-making-function* make-thread-fn)
  (multiple-value-bind (thread-number)
      (swconf:threads-parameters)
    (log-message :info "Initializing a pool of ~a testing threads" thread-number)
    (let ((threads (loop for i from 0 below thread-number
                         collect
                         (bt:make-thread (funcall make-thread-fn i)
                                         :name (text-utils:to-s i)))))
      (setf *testing-pool* threads))))

(let ((count 0))
  (misc:defun-w-lock expand-threading-pool ()
      *expanding-testing-pool-lock*
    (decf count)
    (push (bt:make-thread (funcall *thread-testing-making-function* count)
                          :name (text-utils:to-s count))
          *testing-pool*)))

(misc:defun-w-lock threading-pool-length ()
      *expanding-testing-pool-lock*
  (length *testing-pool*))

(defun make-pool-expander-loop (queue)
  (let ((thread (bt:make-thread
                 (lambda ()
                   (let* ((max-queue-size  (swconf:thread-pool-expanding-threshold))
                          (check-frequency (swconf:thread-pool-expanding-check-frequency))
                          (max-pool-size   (swconf:thread-pool-maximum-size))
                          (delay           (seconds->milliseconds check-frequency)))
                     (log-message :info "Initializing pool expander thread.")
                     (loop while (not (stop-all-threads-p))
                           do
                              (sleep-milliseconds delay)
                              (statistics:update-memory-used)
                              (when (and (< (threading-pool-length)
                                            max-pool-size)
                                         (> (q:size queue)
                                            max-queue-size))
                                (log-message :info
                                             "expanding testing pool...")
                                (expand-threading-pool)))
                     (log-message :info "Stopping pool expander thread.")))
                                :name "Expander thread")))
    (setf *expanding-pool-thread* thread)))

(defparameter *notification-loop* nil)

(defun wait-threads-terminate ()
  (bt:join-thread *recycling-thread*)
  (bt:join-thread *notification-loop*)
  (loop for thread in *testing-pool* do
    (bt:join-thread thread))
  (bt:join-thread *expanding-pool-thread*)
  (misc:log-message :info "Main loop terminated. Quitting."))

(defparameter *quitting-queue* (make-instance 'q:queue))

(defun unblock-quitting-barrier ()
  (q:push-unblock *quitting-queue* :quit))

(defun quitting-barrier ()
  (bt:join-thread (bt:make-thread (lambda () (q:pop-block *quitting-queue*))
                                  :name "quitting barrier")))
