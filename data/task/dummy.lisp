;;;;;;; mandatory code ;;;;;;;;;

(defpackage :task-dummy ;; always use an unique name across all task
  (:use
   :cl
   :core-tests)
  (:local-nicknames (:a :alexandria)))

(in-package :task-dummy) ; the same name as after :defpackage

;;;;;;; end of mandatory code ;;;;;;;;;

(let ((state 1))

  (defun dummy-task (&rest args)
    "A function that run the task.  Must returns `nil' to indicate that no
notification is needed or one, three  or four values, depending of the
values stored in `ARG'.

If called with no arguments, returns ~nil~ if the task passed (i.e. no
notification is  needed), four or five values -instead- if  the task
needs to send a notification, the five values are:

 - the level of the notification (:info, :error, :warning,
   :debug);
 - the text of the notification;
 - the notification channel (just :mail, telegram or
   :all, any other value means that no notification will be sent);
 - an optional string that specify a regular expression (see regex(7))
   matching the the destination group or groups (see
   [[Configuration]]);
 - a, possibly  empty, list that  specify absolute file paths  of additional
   data to be attached to notification.


If the function is called with argument: \"frequency\", the frequency of the
task is returned instead, see [[Task frequency format]].

If the  function is  called with argument:  \"respects-grace-time\", a
generalized  boolean is  returned  that  , if  not  nil, instructs  the
program  to   *not*  respects  grace  time   (i.e.  send  consecutive
notifications)

The function is  called with argument: \"init\" only  once, just after
have been loaded, this way  the task's code can perform initialization
duties.

Finally if the function is called with argument: \"dependencies\", a
list of dependecies (as task paths or url) the frequency of the  is
returned instead.

 Frequency format
   FREQUENCY         := LITERAL-FREQUENCY | NUMBER SPACE* UNITS
   LITERAL-FREQUENCY := \"once\" | \"half-hour\" | \"hourly\" | \"daily\" | \"weekly\" | \"monthly\" | \"yearly\"
   ONCE              := \"once\" | \"boot\"
   NUMBER            := [0-9]+
   UNITS             := 'h' 'm' 's' 'd' 'ms'
   SPACE             := ASCII #x20

 frequency example:

 \"100ms\" → one hundred milliseconds
 \"10s\"   → ten second
 \"2h\"    → two hours
 \"1d\"    → one day
 \"once\"  → run and discard the task"
    (cond
      ((arguments-asks-frequency-p args)
       "10s")
      ((arguments-asks-dependencies-p args)
       '())
      ((arguments-asks-respects-grace-time-p args)
       t)
      ((arguments-init-p args)
       "init called")
      (t
       (sleep 1)
       (incf state)
       (when (< (rem state 5)
                3)
         (values :info
                 "notification from lisp code"
                 :all
                 "developers"
                 '("foo.png" "bar.jpg")))))))

;; set the function to be called, this step is mandatory.
(setf *loaded-task-function* #'dummy-task)
