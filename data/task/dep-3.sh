#!/bin/bash

# see the manpage rib(1) for help writing tasks

case $1 in
    frequency)
        echo "10s";;
    dependencies)
        echo -e "";;
    respects-grace-time)
        echo  "" ;;
    init)
        echo -e "initialization from shell script";;
    *)
        sleep 1s
        echo -e "info\tnotification from bash script dep-3\tmail\tdeveloper\tfoo.png\tbar.jpg";;
esac
