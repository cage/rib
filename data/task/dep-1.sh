#!/bin/bash

# see the manpage rib(1) for help writing tasks

case $1 in
    frequency)
        echo "10s";;
    dependencies)
        echo -e "";;
    respects-grace-time)
        echo "yes";;
    init)
        echo -e "initialization\tphase I\tphase II\tphase III\tphase IV";;
    *)
        sleep 1s
        echo -e "";;
esac
