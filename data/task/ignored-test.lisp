;;;;;;; mandatory code ;;;;;;;;;

(defpackage :task-dummy ;; always use an unique name across all task
  (:use
   :cl
   :core-tests)
  (:local-nicknames (:a :alexandria)))

(in-package :task-dummy) ; the same name as after :defpackage

;;;;;;; end of mandatory code ;;;;;;;;;

(defun dummy-task (&rest args))

;; set the function to be called , this step is mandatory.
(setf *loaded-task-function* #'dummy-task)
