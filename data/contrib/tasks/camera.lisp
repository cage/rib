;;;; This task will fetch frames from a camera checking for modification
;;;; in the images captured; if the test fails (i.e. the captured frames
;;;; are all different  from the benchmark, the frame  captured when the
;;;; task has been  initialized) will send a  notification with attached
;;;; the encrypted frames captured.   The frame are encrypted (symmetric
;;;; encryption) using gpg with the master password of the software.
;;
;;;; Please use  this code  responsibly, on some  jurisdictions pointing
;;;; the  camera  on  public   places  requires  permission  from  local
;;;; authorities.

;;; this task  require nodgui and  cl-gpio to works as it is used  on a
;;; rasberrypi board; both libraries are available on quicklisp

(asdf:make "nodgui")

(asdf:make "cl-gpio")

(defpackage :task-camera
  (:use
   :cl
   :core-tests
   :os-utils)
  (:local-nicknames (:a :alexandria)))

(in-package :task-camera)

;;;; configure from here

;; delay before initialize the task

(a:define-constant +seconds-before-init+ 30 :test #'=)

;; expected an active buzzer on this pin

(a:define-constant +pin-buzzer+ 4 :test #'=)

;; I am  using a magnetic sensor  to act as  a switch but any  kind of
;; digital will do (maybe a RFID tag would works too?)

(a:define-constant +pin-switch+ 17 :test #'=)

;; The path of the program to capture  a frame from the camera; if you
;; change this constant  very likely also the  code in 'capture-frame'
;; would need to be modified as well.

(a:define-constant +capture-frame-program-path+ "/usr/bin/fswebcam" :test #'string=)

;; change accordingly to your setup

(a:define-constant +capturing-device+ "/dev/video0" :test #'string=)

;; increasing these  two values  will increase the  camera detection's
;; sensibility, but the  chances of false positives  will increase too,
;; so a balance must be found.
;; These values depends on your camera hardware

(a:define-constant +scaling-x+ 20 :test #'=)

(a:define-constant +scaling-y+ 19 :test #'=)

;; the  higher this  value  the less  the camera  will  be reactive  to
;; changes on the captured frames

(a:define-constant +camera-alarm-treshold+ 39 :test #'=)

;; capture a frame in *gray scale*, it is OK to have true color format
;; (RGB  and optional  alpha)  until the  color  channels (except  for
;; alpha) have the same value

(defun capture-frame (&optional (skip-frames-count 10))
  (handler-case
      (let ((octets (os-utils:slurp-octets-external-program +capture-frame-program-path+
                                                            (list "-q"
                                                                  "--png"
                                                                  "9"
                                                                  "--no-banner"
                                                                  "--greyscale"
                                                                  "-S"
                                                                  (format nil
                                                                          "~a"
                                                                          skip-frames-count)
                                                                  "-d"
                                                                  +capturing-device+
                                                                  "-")))
            (pixmap (make-instance 'nodgui.pixmap:png)))
        (nodgui.pixmap:load-from-vector pixmap octets))
    (error (e)
      (error "unrecoverable error accessing the camera: ~a" e))))

;;; end configuration

(defun pause-detection-p ()
  (gpio:value +pin-switch+))

(defun short-beep ()
  (setf (gpio:value +pin-buzzer+) t)
  (misc:sleep-milliseconds 1000)
  (setf (gpio:value +pin-buzzer+) nil))

(defun long-beep ()
  (setf (gpio:value +pin-buzzer+) t)
  (misc:sleep-milliseconds 2000)
  (setf (gpio:value +pin-buzzer+) nil))

(defun very-short-beep ()
  (setf (gpio:value +pin-buzzer+) t)
  (misc:sleep-milliseconds 500)
  (setf (gpio:value +pin-buzzer+) nil))

(defun double-beep ()
  (very-short-beep)
  (misc:sleep-milliseconds 500)
  (very-short-beep))

(defun make-pause ()
  (misc:sleep-milliseconds (misc:seconds->milliseconds 180)))

(defun make-short-pause ()
  (misc:sleep-milliseconds (misc:seconds->milliseconds 5)))

(defparameter *temporary-file-paths* '())

(defun make-temporary-file (extension)
  (let ((name (fs:temporary-file :extension extension)))
    (push name *temporary-file-paths*)
    name))

(defun temporary-file-stale-p (path)
  (let ((now           (get-universal-time))
	(modification-time (fs:get-stat-mtime path)))
    (> (- now modification-time)
       3600)))

(defun remove-temporary-files ()
  (let ((deleted-temporary-files '()))
    (loop for file in *temporary-file-paths*
	  when (and (fs:file-exists-p file)
		    (temporary-file-stale-p file))
	  do
	  (progn
	    (push file deleted-temporary-files)
            (fs:delete-file-if-exists file)))
    (misc:log-message :info "[camera] deleted temporary files ~a" deleted-temporary-files)
    (setf *temporary-file-paths*
	  (set-difference *temporary-file-paths*
			  deleted-temporary-files
			  :test #'string=))
    (misc:log-message :info "[camera] remaining temporary files ~a" *temporary-file-paths*)))

(defun save-pixmap (pixmap where)
  (nodgui.pixmap:save-pixmap pixmap where))

(defun encrypt-frame (frame)
  (let* ((path                (make-temporary-file ".png"))
         (path-encrypted-file (format nil "~a.gpg" path)))
    (save-pixmap frame path)
    (os-utils:run-external-program "gpg"
                                   (list "--batch"
                                         "--passphrase"
                                         swconf:*master-password*
                                         "--symmetric"
                                         path)
                                   :wait t
                                   :search t)
    (push path-encrypted-file *temporary-file-paths*)
    path-encrypted-file))

(defun encrypt-frames (frames)
  (loop for frame in frames collect (encrypt-frame frame)))

(defun calculate-fingerprint (pixmap)
  (let* ((scale-factor-x    (/ +scaling-x+
                               (nodgui.pixmap:width pixmap)))
         (scale-factor-y    (/ +scaling-y+
                               (nodgui.pixmap:height pixmap)))
         (scaled            (nodgui.pixmap:scale-bilinear pixmap
                                                          scale-factor-x
                                                          scale-factor-y))
         (fingerprint       #x0000000000000000)
         (fingerprint-index 0))
    (loop for y from 0 below (nodgui.pixmap:height scaled) do
      (loop for x from 0 below (1- (nodgui.pixmap:width scaled)) do
        (let* ((value-a  (a:first-elt (nodgui.pixmap:pixel@ scaled     x  y)))
               (value-b  (a:first-elt (nodgui.pixmap:pixel@ scaled (1+ x) y)))
               (gradient (if (< value-a value-b)
                             0
                             1)))
          (setf fingerprint (logior (ash gradient
                                         fingerprint-index)
                                    fingerprint))
          (incf fingerprint-index))))
    fingerprint))

(defun calculate-hash-difference (hash1 hash2)
  (let ((difference-index 0))
    (loop
          for count = 0 then (incf count)
          while (< count
                   (* (1- +scaling-x+)
                      +scaling-y+))
          for probe = #x1 then (ash probe 1)
          do
          (when (= (logand probe
                           (logxor hash1
                                   hash2))
                   probe)
            (incf difference-index)))
    difference-index))

(defun init-gpio ()
  (misc:log-message :info "[camera] init GPIO")
  (loop for i from 0 to 26 do (gpio:export i)))

(defun init ()
  (misc:sleep-milliseconds (misc:seconds->milliseconds +seconds-before-init+))
  (misc:log-message :info "[camera] init")
  (init-gpio)
  (very-short-beep))

(defun capture-frames (frames-number delay)
  (loop repeat frames-number
        collect
        (progn
          (misc:sleep-milliseconds delay)
          (capture-frame))))

(defun frames-shows-changes-p (new-fingerprint old-fingerprint)
  (let* ((diffs (mapcar (lambda (a b) (abs (calculate-hash-difference a b)))
                        old-fingerprint
                        new-fingerprint))
         (average (/ (reduce #'+ diffs)
                     (length new-fingerprint))))
    (>= average +camera-alarm-treshold+)))

(let ((start-pause       nil)
      (previous-fingerprint '()))
  (defun camera-task (&rest args)
    (cond
      ((arguments-asks-frequency-p args)
       "500ms")
      ((arguments-asks-dependencies-p args)
       '())
      ((arguments-asks-respects-grace-time-p args)
       t)
      ((arguments-init-p args)
       (init))
      (t
       (cond
         ((pause-detection-p)
          (setf start-pause t)
          (short-beep)
          (make-short-pause)
          (values :info
                  (format nil "Alarm disarmed.")
                  :all
                  "admin"
                  nil))
         (start-pause
          (misc:log-message :info "[camera] start pause")
          (double-beep)
          (setf start-pause nil)
          (make-pause)
          (misc:log-message :info "[camera] end pause")
          (values :info
                  (format nil "Alarm rearmed.")
                  :all
                  "admin"
                  nil))
         (t
          (remove-temporary-files)
          (handler-case
              (let* ((frames       (capture-frames 4 1))
                     (fingerprints (mapcar #'calculate-fingerprint frames)))
                (cond
                  ((null previous-fingerprint)
		   (misc:log-message :info "[camera] setting first time fingerprint")
                   (setf previous-fingerprint fingerprints)
                   nil)
                  ((frames-shows-changes-p fingerprints previous-fingerprint)
                   (let ((encrypted-frame-files (encrypt-frames frames)))
                     (setf previous-fingerprint fingerprints)
                     (very-short-beep)
                     (misc:log-message :info "[camera] diff detected!")
                     (values :info
                             "Camera detected changes!"
                             :all
                             "admin"
                             encrypted-frame-files)))
                  (t
                   (setf previous-fingerprint fingerprints)
                   nil)))
                (error (e)
                  (values :info
                               (format nil "Communication error with camera: ~a" e)
                               :all
                               "admin"
                               nil)))))))))

;; set the function to be called, this step is mandatory.

(setf *loaded-task-function* #'camera-task)
