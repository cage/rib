;; rib: a general scheduler.
;; Copyright (C) 2022  Università degli Studi di Palermo

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

;; Notes:

;; this is an example for a module to integrate rib with glpi:
;; https://glpi-project.org/

;; using this module  (see the file init.lisp)  each notification will
;; open a new ticket to your glpi instance.

(defpackage :glip
  (:use
   :cl
   :config
   :constants
   :misc-utils
   :text-utils
   :http-utils)
  (:local-nicknames (:a    :alexandria)
                    (:text :text-utils)
                    (:json :yason)))

(in-package :glip)

(a:define-constant +api-base-url+ "https://authority/apirest.php/" :test #'string=)

;; these two values below must be crypted using "rib -E" See rib(1).

(a:define-constant +app-token+ "crypted: use rib -E" :test #'string=)

(a:define-constant +user-token+ "crypted: use rib -E"  :test #'string=)

(a:define-constant +app-token-header-name+ "App-Token" :test #'string=)

(a:define-constant +user-token-header-name+ "Authorization" :test #'string=)

(a:define-constant +session-header-name+ "Session-Token" :test #'string=)

(a:define-constant +get-session-endpoint+ (text:strcat +api-base-url+ "initSession/")
  :test #'string=)

(a:define-constant +json-session-token-key+ "session_token" :test #'string=)

(a:define-constant +post-ticket-endpoint+ (text:strcat +api-base-url+ "Ticket/")
  :test #'string=)

(defun get-session-token ()
  (handler-case
      (let* ((headers        (list (cons +app-token-header-name+  (swconf::decrypt +app-token+))
                                   (cons +user-token-header-name+ (swconf::decrypt +user-token+))))
             (body           (http-utils:get-url-content-body +get-session-endpoint+
                                                              :additional-headers headers))
             (body-as-string (babel:octets-to-string body))
             (json           (yason:parse body-as-string)))
        (gethash +json-session-token-key+ json))
    (error (e)
      (misc:log-message :debug "[glpi] error getting session token: ~a" e)
      nil)))

(a:define-constant +post-ticket-endpoint+ (text:strcat +api-base-url+ "Ticket/")
  :test #'string=)

(defun encode-ticket (name content comments &optional (status 1) (urgency 2))
  (with-output-to-string (stream)
    (yason:encode (a:plist-hash-table (list "input"
                                            (a:plist-hash-table (list "name"     name
                                                                      "content"  content
                                                                      "comments" comments
                                                                      "status"   status
                                                                      "urgency"  urgency))))
                  stream)))

(defun make-post-ticket-headers (session-token)
  (list (cons +app-token-header-name+ (swconf::decrypt +app-token+))
        (cons +session-header-name+  session-token)
        (cons "Content-Type" "application/json")))

(defun post-ticket (name content comments &optional (status 1) (urgency 2))
  (handler-case
   (a:when-let* ((json-content  (encode-ticket name content comments status urgency))
                 (session-token (get-session-token))
                 (headers       (make-post-ticket-headers session-token)))
     (multiple-value-bind (x response-code y)
         (http-utils:get-url-content-body +post-ticket-endpoint+
                                          :method             :post
                                          :additional-headers headers
                                          :content            json-content)
       (declare (ignore y))
       (misc:log-message :debug
                         "[glpi] response: ~a ~a"
                         response-code (babel:octets-to-string x))
       response-code))
    (error (e)
      (misc:log-message :debug "[glpi] error sending ticket: ~a" e))))

(hooks:add-hook 'hooks:*channel-notification-hook*
                (lambda (task
                         channel
                         level
                         payload
                         metadata
                         destination
                         notification-sent)
                  (declare (ignore task channel metadata destination))
                  (when notification-sent
                    (post-ticket metadata payload (string level)))))
